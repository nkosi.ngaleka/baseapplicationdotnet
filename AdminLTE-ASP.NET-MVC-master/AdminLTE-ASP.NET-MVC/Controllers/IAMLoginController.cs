﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Configuration;

namespace WebApplication.Controllers
{
    public class IAMLoginController : Controller
    {


        public static string CapturerService, UserServiceNo;
        public static Boolean privateValue = true;
        public static int ApplicationId, PrivateWorkId;

        public static string reference = "";

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 

        [AllowAnonymous]
        public ActionResult OpenIDconnect()
        {
            var d = DateTime.Now.ToString("yyyyMMdd");
            var n = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

            var url = "";

            var AUTHZ_EP = ConfigurationManager.AppSettings["AUTHZ_EP"];
            var CLIENT_ID = ConfigurationManager.AppSettings["CLIENT_ID"];
            var CALLBACK_EP = ConfigurationManager.AppSettings["CALLBACK_EP"];

            url = AUTHZ_EP + "?client_id=" + CLIENT_ID + "&response_type=id_token token&scope=openid&nonce=" + n + "&redirect_uri=" + CALLBACK_EP;


            return Redirect(url);
        }



        #region Redirect
        [AllowAnonymous]
        public ActionResult Redirect()
        {
            return View();
        }
        #endregion


        #region RedirectSuccess
        [AllowAnonymous]
        [HttpPost]
        public ActionResult RedirectSuccess(string id_token, string raw_token)
        {

            Session["raw_token"] = raw_token;

            var normalString = DecodeBase64(id_token);
            //var normalString = WindowsDecrypted(id_token);

            JObject response = JObject.Parse(normalString);
            var emailAddress = response["email"];
            var lastname = response["family_name"];
            var given_name = response["given_name"];
            var name = response["name"];
            var sub = response["sub"];

            Session["UserName"] = given_name + " " + lastname;
            Session["Role"] = "Employee";
            Session["Email"] = response["email"].ToString();

            return View("Home");
        }

        #endregion


        private static string DecodeBase64(string value)
        {
            var valueBytes = System.Convert.FromBase64String(value.Replace(" ", "+"));
            return Encoding.UTF8.GetString(valueBytes);
        }

        public static string WindowsDecrypted(string text)
        {
            string base64 = text.Replace("-", "+").Replace("_", "/").Replace(".", "=");
            byte[] encrypted = Convert.FromBase64String(base64);
            byte[] plainBinary = ProtectedData.Unprotect(encrypted, null, DataProtectionScope.LocalMachine);
            return Encoding.Unicode.GetString(plainBinary);
        }

        public ActionResult LoginFail()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
    }
}
