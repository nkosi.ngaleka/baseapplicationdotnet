﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class PaymentScheduleDetailsController : Controller
    {
        private GrantsManagementDBEntities db = new GrantsManagementDBEntities();

        // GET: PaymentScheduleDetails
        public ActionResult Index()
        {
            return View(db.GrantApplications.ToList());
        }

        // GET: PaymentScheduleDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentScheduleDetail paymentScheduleDetail = db.PaymentScheduleDetails.Find(id);
            if (paymentScheduleDetail == null)
            {
                return HttpNotFound();
            }
            return View(paymentScheduleDetail);
        }

        // GET: PaymentScheduleDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PaymentScheduleDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ApplicationID,PaymentDate1,PaymentDate2,PaymentDate3,DateCreated,IsActive,IsDeleted")] PaymentScheduleDetail paymentScheduleDetail)
        {
            if (ModelState.IsValid)
            {
                db.PaymentScheduleDetails.Add(paymentScheduleDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(paymentScheduleDetail);
        }

        // GET: PaymentScheduleDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrantApplication grantApplication = db.GrantApplications.Find(id);
            if (grantApplication == null)
            {
                return HttpNotFound();
            }
            return View(grantApplication);
        }

        // POST: PaymentScheduleDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ApplicationID,PaymentDate1,PaymentDate2,PaymentDate3,DateCreated,IsActive,IsDeleted")] PaymentScheduleDetail paymentScheduleDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paymentScheduleDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paymentScheduleDetail);
        }

        // GET: PaymentScheduleDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentScheduleDetail paymentScheduleDetail = db.PaymentScheduleDetails.Find(id);
            if (paymentScheduleDetail == null)
            {
                return HttpNotFound();
            }
            return View(paymentScheduleDetail);
        }

        // POST: PaymentScheduleDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PaymentScheduleDetail paymentScheduleDetail = db.PaymentScheduleDetails.Find(id);
            db.PaymentScheduleDetails.Remove(paymentScheduleDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
