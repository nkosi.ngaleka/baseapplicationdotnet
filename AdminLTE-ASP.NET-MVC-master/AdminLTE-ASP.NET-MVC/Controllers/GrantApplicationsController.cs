﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class GrantApplicationsController : Controller
    {
        private GrantsManagementDBEntities db = new GrantsManagementDBEntities();

        // GET: GrantApplications
        public ActionResult Index()
        {
            return View();
        }

        // GET: GrantApplications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrantApplication grantApplication = db.GrantApplications.Find(id);
            if (grantApplication == null)
            {
                return HttpNotFound();
            }
            return View(grantApplication);
        }

        // GET: GrantApplications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GrantApplications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GrantScheduleType,GrantType,GrantName,GrantPurpose,ApplicationRefNo,ApplicationSubmissionDate,ApplicationStatus,ReceivingOfficer,ReceivingDepartment,ReceivedDate,EmaillAddress,GrantorOfficer,Grantor,GrantorEmailAddress,GrantAmount,RejectionReason,AdditionalDescription,ReasonsForNonExpenditure,DateCreated,IsActive,IsDeleted")] GrantApplication grantApplication)
        {
            if (ModelState.IsValid)
            {
                db.GrantApplications.Add(grantApplication);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(grantApplication);
        }

        // GET: GrantApplications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrantApplication grantApplication = db.GrantApplications.Find(id);
            if (grantApplication == null)
            {
                return HttpNotFound();
            }
            return View(grantApplication);
        }

        // POST: GrantApplications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GrantScheduleType,GrantType,GrantName,GrantPurpose,ApplicationRefNo,ApplicationSubmissionDate,ApplicationStatus,ReceivingOfficer,ReceivingDepartment,ReceivedDate,EmaillAddress,GrantorOfficer,Grantor,GrantorEmailAddress,GrantAmount,RejectionReason,AdditionalDescription,ReasonsForNonExpenditure,DateCreated,IsActive,IsDeleted")] GrantApplication grantApplication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grantApplication).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(grantApplication);
        }

        // GET: GrantApplications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrantApplication grantApplication = db.GrantApplications.Find(id);
            if (grantApplication == null)
            {
                return HttpNotFound();
            }
            return View(grantApplication);
        }

        // POST: GrantApplications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GrantApplication grantApplication = db.GrantApplications.Find(id);
            db.GrantApplications.Remove(grantApplication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
