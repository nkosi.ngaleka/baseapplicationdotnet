﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class GrantRegistersController : Controller
    {
        private GrantsManagementDBEntities db = new GrantsManagementDBEntities();

        // GET: GrantRegisters
        public ActionResult Index()
        {
            return View(db.GrantApplications.ToList());
        }

        // GET: GrantRegisters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrantRegister grantRegister = db.GrantRegisters.Find(id);
            if (grantRegister == null)
            {
                return HttpNotFound();
            }
            return View(grantRegister);
        }

        // GET: GrantRegisters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GrantRegisters/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ApplicationID,StartDate,EndDate,BudgetFinYear,BudgetExpenditure,Cashflow,UnspentGrant,CommitedToProject,CommunicationFromNT,RolloverApplication,COfAccounts,FundArea,FundSource,FundGUID,DateCreated,IsActive,IsDeleted")] GrantRegister grantRegister)
        {
            if (ModelState.IsValid)
            {
                db.GrantRegisters.Add(grantRegister);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(grantRegister);
        }

        // GET: GrantRegisters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrantApplication grantRegister = db.GrantApplications.Find(id);
            if (grantRegister == null)
            {
                return HttpNotFound();
            }
            return View(grantRegister);
        }

        // POST: GrantRegisters/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ApplicationID,StartDate,EndDate,BudgetFinYear,BudgetExpenditure,Cashflow,UnspentGrant,CommitedToProject,CommunicationFromNT,RolloverApplication,COfAccounts,FundArea,FundSource,FundGUID,DateCreated,IsActive,IsDeleted")] GrantRegister grantRegister)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grantRegister).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(grantRegister);
        }

        // GET: GrantRegisters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrantRegister grantRegister = db.GrantRegisters.Find(id);
            if (grantRegister == null)
            {
                return HttpNotFound();
            }
            return View(grantRegister);
        }

        // POST: GrantRegisters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GrantRegister grantRegister = db.GrantRegisters.Find(id);
            db.GrantRegisters.Remove(grantRegister);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
