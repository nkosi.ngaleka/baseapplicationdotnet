﻿app.controller('ChangePasswordController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;
    $scope.imgLoader_Save = false;
    $scope.SpinnerSearchWip = false;

    $scope.changePassword = function () {

        $scope.imgLoader_Save = true;
        $scope.SpinnerSearchWip = true;
        $scope.showMsgs = false;
        if ($scope.NewPassword == $scope.ConfirmPassword) {
            $http.post('../Master/ChangePassword', {
                OldPassword: $scope.OldPassword,
                NewPassword: $scope.NewPassword
            })
                .then(function (res) {

                    $scope.imgLoader_Save = false;
                    $scope.SpinnerSearchWip = false;

                    if (res.data.success) {
                        alertify.alert('Success', res.data.success, function () {
                            window.location.href = '../Home/Dashboard';
                        });
                    }
                    else if (res.data.error) {
                        alertify.alert('Error', res.data.error, function () {
                        });
                    } else if (res.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                });
        } else {
            alertify.alert('Error', "new password and confirm password didn't match ", function () {
            });
        }
        
    }


});

