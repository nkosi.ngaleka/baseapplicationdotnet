﻿
app.controller('LiveUsersMapCntrl', function (config, $scope, $filter, $rootScope, $timeout, $interval, $http) {
    $scope.BasicLatitude = -26.164726;
    $scope.BasicLongitude = 28.369397;
    var refreshInterval = 1 * 60 * 1000;
    $scope.TotalUsers = "0";
    $scope.userId = "0";
    var markerArr = [];

    var endTime = new Date().getTime() + 1 * 60 * 1000; // 1 minutes in milliseconds

    $scope.DBUsers = function () {
        $scope.UserList = [];
        $scope.TotalUsers = "0";
        DeleteMarkers();
        $scope.isRepeate = true;
        $http.post('../Master/GetDBLiveUsers').then(function (reasons) {
           // console.log(reasons)
            if (reasons.data.success) {
                $scope.UserList = reasons.data.LiveUsers;
                $scope.TotalUsers = $scope.UserList.length;
              //  console.log("OnlineUsers", $scope.UserList);

                if ($scope.UserList.length > 0) {
                    $scope.LeaftletLiveUserMap($scope.UserList);

                }
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
            $scope.isRepeate = false;
        });

    }

    $interval(function () {
        endTime = new Date().getTime() + 1 * 60 * 1000;
        $scope.DBUsers();
    }, refreshInterval);


    $interval(function () {
        var now = new Date().getTime();
        var remainingTime = endTime - now;
        $scope.remainingSeconds = Math.floor(remainingTime / 1000);
    }, 1000); // update every 1 second
  
    /* leaflet map for live user */

    function DeleteMarker(id) {
        //
        //Find and remove the marker from the Array
        for (var i = 0; i < markerArr.length; i++) {
            // alert(markerArr[i].userID)
            if (markerArr[i].userID === id) {
                //Remove the marker from Map                  
                // markerArr[i].setMap(null);

                RemoveMarker(markerArr[i].marker);

                //Remove the marker from array.
                markerArr.splice(i, 1);


                return;
            }
        }
    };

    function DeleteMarkers() {
        //Loop through all the markers and remove
        // alert(markerArr.length)

        for (var i = 0; i < markerArr.length; i++) {
            RemoveMarker(markerArr[i].marker);
        }
        markerArr = [];
    };

    $scope.RemoveSlectedMarker = function () {
        RemoveMarker(markerArr[0].marker);
    }

    function RemoveMarker(marker) {
        console.log(marker)
        if (map.hasLayer(marker)) {
            //alert('removed')
            map.removeLayer(marker); // remove
        }
    }

    var i = 0;

    var map = L.map('map').setView([-26.164726, 28.369397], 11);

    /*L.esri.Vector.vectorBasemapLayer(basemapEnum, {
        apiKey: apiKey
      }).addTo(map);
      */

    //var osm = new L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
    //    attribution: ''
    //}).addTo(map);

    var osm = new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: ''
    }).addTo(map);

    const markerIcon = L.icon({
        iconSize: [25, 41],
        iconAnchor: [10, 41],
        popupAnchor: [2, -40],
        // specify the path here
        iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
        shadowUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-shadow.png"
    });

    $scope.LeaftletLiveUserMap = function (firebaseData) {
       // console.log("BeforeMarkers", markerArr);
        for (var i = 0; i < firebaseData.length; i++) {
           // console.log("Users", firebaseData[i]);
            //var title = 'Latitude: ' + firebaseData.lat + '<br />Longitude: ' + firebaseData.lng;
            //title += "<br />loggedTime: " + firebaseData.loggedTime;
            var title = "Name: " + firebaseData[i].FirstName + " " + firebaseData[i].LastName;
            title += "<br/>UserID: " + firebaseData[i].UserID;
            title += "<br/>LiveDateTime: " + firebaseData[i].LiveDate;

            var marker = L.marker(L.latLng(parseFloat(firebaseData[i].LATITUDE), parseFloat(firebaseData[i].LONGITUDE)), {
                title: title, opacity: 1, icon: new L.DivIcon({
                    className: 'my-div-icon',
                    html: '<img class="my-div-image" src="../Assets/img/map-marker-2-32.png"/>' +
                        '<span class="" style="color:green;font-weight:500">' + firebaseData[i].FirstName + '</span>'
                })
            });
            //marker.bindLabel("My Label", { noHide: true, className: "my-label", offset: [0, 0] });

            markerArr.push({ userID: firebaseData[i].UserID, marker: marker });
            //var marker = L.marker([parseFloat(locations[i].LATT), parseFloat(locations[i].LONG)], { icon: greenIcon });
            marker.addTo(map).bindPopup(title);
        }
        
      //  console.log("AfterMarkers", markerArr);

    }


    // Set style function that sets fill color property
    function style(feature) {
        return {
            fillColor: '#72bcd4',
            fillOpacity: 0.5,
            weight: 1,
            opacity: 1,
            color: '#000',
            dashArray: '2'
        };
    }
    var highlight = {
        'fillColor': 'yellow',
        'weight': 2,
        'opacity': 1
    };

    function forEachFeature(feature, layer) {

        var popupContent = "<p><b>Ward No: </b>" + feature.properties.Name + '</p>';
        //"</br>Municipality Name: "+ feature.properties.MunicName +
        //"</br>GAPA_NAPA: "+ feature.properties.MunicName +
        //"</br>PGN_TYPE: "+ feature.properties.MapCode +
        //"</br>PROVINCE: "+ feature.properties.Province +'</p>';

        //alert("here!!");

        layer.bindPopup(popupContent);

        layer.on("click", function (e) {
            theLayer.setStyle(style); //resets layer colors
            layer.setStyle(highlight);  //highlights selected.
            //alert("here!!");
        });
    }

    // Null variable that will hold layer
    var theLayer = L.geoJson(null, { onEachFeature: forEachFeature, style: style });

    $.getJSON(config.eku_Url, function (data) {
        theLayer.addData(data);
    });

    theLayer.addTo(map);

    // for Layer Control    
    var baseMaps = {
        "Open Street Map": osm
    };

    var overlayMaps = {
        "My Data": theLayer
    };

    //Add layer control
    L.control.layers(baseMaps, overlayMaps).addTo(map);


    /* leaflet end */


    /* google map live user functionality*/

    //var map;
    //var markerArr = [];
    //// Overlay view allows you to organize your markers in the DOM
    //// https://developers.google.com/maps/documentation/javascript/reference#OverlayView
    //var myoverlay = new google.maps.OverlayView();

    //var i = 0;


    //function DeleteMarkers() {
    //    //Loop through all the markers and remove
    //    for (var i = 0; i < markerArr.length; i++) {
    //        markerArr[i].setMap(null);
    //    }
    //    markerArr = [];
    //};


    //console.log($scope.cat);
    //$scope.GetLiveLocation = function (val) {
    //    console.log(val)
    //    console.log(leadsRef)
    //    leadsRef.on('value', (snapshot) => {
    //        console.log('snapshot.val()');
    //        console.log(snapshot.val());
    //        if (i === 0) {
    //            var key = '';
    //            if(snapshot.val() !== null) {
    //                var childData1 = snapshot.val();
    //                 key = Object.keys(childData1)[0];    //this will return 1st key.         
    //                console.log(childData1[key].lat);
    //            }


    //            var mapOptions = {
    //                zoom: 10,
    //                mapTypeControl: false,
    //                center: snapshot.val() !== null ? new google.maps.LatLng(childData1[key].lat, childData1[key].lng) : new google.maps.LatLng(-25.9612731, 28.6504824),
    //                streetViewControl: true,
    //                styles: [
    //                    {
    //                        "featureType": "administrative",
    //                        "elementType": "geometry",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    },
    //                    {
    //                        "featureType": "administrative.land_parcel",
    //                        "elementType": "labels",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    },
    //                    {
    //                        "featureType": "poi",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    },
    //                    {
    //                        "featureType": "poi",
    //                        "elementType": "labels.text",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    },
    //                    {
    //                        "featureType": "road",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    },
    //                    {
    //                        "featureType": "road",
    //                        "elementType": "labels.icon",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    },
    //                    {
    //                        "featureType": "road.local",
    //                        "elementType": "labels",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    },
    //                    {
    //                        "featureType": "transit",
    //                        "stylers": [
    //                            {
    //                                "visibility": "off"
    //                            }
    //                        ]
    //                    }
    //                ]
    //            };
    //            map = new google.maps.Map(document.getElementById('map'), mapOptions);
    //            map.setTilt(45);
    //            // image from external URL
    //            var myIcon = 'https://1001freedownloads.s3.amazonaws.com/vector/thumb/74908/color_icons_green_home.png';

    //            //preparing the image so it can be used as a marker
    //            //https://developers.google.com/maps/documentation/javascript/reference#Icon
    //            //includes hacky fix "size" to allow for wobble
    //            var catIcon = {
    //                url: myIcon,
    //                size: new google.maps.Size(100, 60),
    //                scaledSize: new google.maps.Size(70, 60),
    //                origin: new google.maps.Point(-15, 0)
    //            }
    //        }



    //        // alert(markerArr.length)
    //        DeleteMarkers();


    //        // alert(markerArr.length)
    //        snapshot.forEach(function (childSnapshot) {
    //            //markerArr = [];
    //            var childData = childSnapshot.val();
    //            //console.log(childData);
    //            DeleteMarker(childData.userID);
    //            console.log(childData.userID + '--------->' + val);
    //            console.log(parseInt(val) === parseInt(childData.userID))
    //            if (val === 'all') {
    //               initialize(childData);

    //            } else if (parseInt(val) === parseInt(childData.userID)) {
    //                console.log(val);
    //                initialize(childData);
    //            }
    //            i++;
    //        });



    //    });
    //    //=====================
    //    map.data.loadGeoJson('../AngularJS/Home/eku.json');
    //    //=====================

    //}


    // $scope.GetLiveLocation($scope.cat);



    //function DeleteMarker(id) {
    //    //Find and remove the marker from the Array
    //    for (var i = 0; i < markerArr.length; i++) {
    //        if (markerArr[i].id === id) {
    //            //Remove the marker from Map                  
    //            markerArr[i].setMap(null);

    //            //Remove the marker from array.
    //            markerArr.splice(i, 1);


    //            return;
    //        }
    //    }
    //};

    //function initialize(firebaseData) {

    //    /*
    //    //If you want to do this without wobble animation no need for hacky fix
    //    var catIcon = {
    //      url: myIcon,
    //      size: new google.maps.Size(70, 60),
    //      scaledSize: new google.maps.Size(70, 60),
    //      origin: new google.maps.Point(0,0)
    //    }*/


    //    // Firebase using for map
    //    var latLng = new google.maps.LatLng(parseFloat(firebaseData.lat), parseFloat(firebaseData.lng));
    //    var marker = new google.maps.Marker({
    //        position: latLng,
    //        map: map,
    //        // set the icon as catIcon declared above
    //        // icon: catIcon,
    //        // must use optimized false for CSS
    //        optimized: false,
    //        title: firebaseData.name
    //    });

    //    var d = firebaseData.name.split(' ');


    //    var label = { color: '#333', fontWeight: 'bold', fontSize: '16px', text: d[0].slice(0, 1) + d[1].slice(0, 1) };
    //    marker.setLabel(label);

    //    markerArr.push(marker);

    //    //add a click handler that does nothing at the moment
    //    // google.maps.event.addListener(marker, 'click', function () {
    //    var content = 'Latitude: ' + firebaseData.lat + '<br />Longitude: ' + firebaseData.lng;
    //    content += "<br />loggedTime: " + firebaseData.loggedTime;
    //    content += "<br />name: " + firebaseData.name;
    //    content += "<br />userID: " + firebaseData.userID;
    //    var infoWindow = new google.maps.InfoWindow({
    //        content: content
    //    });
    //    infoWindow.open(map, marker);
    //    // })



    //    // Overlay view allows you to organize your markers in the DOM
    //    //// https://developers.google.com/maps/documentation/javascript/reference#OverlayView
    //    var myoverlay = new google.maps.OverlayView();
    //    myoverlay.draw = function () {                  
    //        // add an id to the layer that includes all the markers so you can use it in CSS
    //        this.getPanes().markerLayer.id = 'markerLayer';
    //        //alert(this.getPanes().markerLayer.id)
    //    };
    //    myoverlay.setMap(map);


    //    //=====================
    //    map.data.loadGeoJson('../AngularJS/Home/eku.json');
    //    //=====================


    //}

    //initialize();


    // use jQuery to change the markers animation based on toggle button
    $('.btn').click(function () {
        var type = $(this).data('anim');
        $('#markerLayer img').css('animation', type + ' 1s infinite alternate');
        $('#markerLayer img').css('-webkit-animation', type + ' 1s infinite alternate')
    })


});