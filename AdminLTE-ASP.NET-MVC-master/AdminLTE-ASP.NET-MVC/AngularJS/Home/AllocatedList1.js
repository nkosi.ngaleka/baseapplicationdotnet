﻿app.controller('MasterController', function ($scope, $http, $filter, $compile) {

    var url = '../Master/GetAllocatedList';

    $http.get(url).then(function (reasons) {
        if (reasons.data.success) {
            $scope.Screens = reasons.data.Screens;
        } else if (reasons.data.error) {
            alertify.alert("Error", reasons.data.error, function () { });
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        } else { window.location.href = "../Account/Login"; }
    });

});