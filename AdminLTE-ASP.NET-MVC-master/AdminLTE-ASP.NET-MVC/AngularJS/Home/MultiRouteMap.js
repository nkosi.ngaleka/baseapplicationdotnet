﻿app.controller('MultiRouteMap', function ($scope, $filter, $rootScope, $timeout, $interval, $http) {


    $scope.travelMode = "DRIVING";
    $scope.isMapReady = false;
    $scope.Service = "ALL"

    $http.post('../Master/GetFormConfig').then(function (reasons) {
        //alert()
        // console.log(reasons)
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;
            // console.log($scope.UserList);
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });

    

    $scope.isNodata = true;
    $scope.GetAllallocatedMeters = function (val) {
        if (val != undefined) {
            $scope.IsSearchCandidates = true;
            $http.get('../Master/GetAllallocatedMeters?selectedUserID=' + val + '&Service=' + $scope.Service)
                .then(function (res) {
                    $scope.frmMap = false;
                    console.log('---------getting online user from database for Map by Procedure--------------');
                    if (res.data.success === 'true') {
                        console.log(res.data)

                        //console.log(res.data.RoutePoints)
                        $scope.kiranOnlineUsers = res.data.RoutePoints;
                        /*initMap(res.data.RoutePoints);*/
                        if (res.data.AllocatedMetersForUsers.length>0) {
                            if ($scope.kiranOnlineUsers.length > 0) {
                               // alert()
                                $scope.isNodata = false;
                                let points = [];
                                let waypoints1A = [];
                                let waypoints4A = [];



                                angular.forEach($scope.kiranOnlineUsers, function (val, i) {
                                    console.log(val)
                                    angular.forEach(val, function (val1, k) {

                                        console.log('shhhhhhhhh')
                                        console.log(k)
                                        console.log(val1)

                                        if (k <= 5 && k != (val.length - 1)) {
                                            waypoints1A.push({ location: new google.maps.LatLng(val1.lat, val1.lng), stopover: true })
                                        }

                                        points.push({
                                            origin: new google.maps.LatLng(val[0].lat, val[0].lng),
                                            destination: new google.maps.LatLng(val[val.length - 1].lat, val[val.length - 1].lng),
                                            waypoints: waypoints1A,
                                            optimizeWaypoints: false,
                                            provideRouteAlternatives: true,
                                            travelMode: google.maps.DirectionsTravelMode.DRIVING,
                                            transitOptions: {
                                                departureTime: new Date(1337675679473),
                                                modes: ['BUS'],
                                                routingPreference: 'FEWER_TRANSFERS'
                                            },
                                            unitSystem: google.maps.UnitSystem.IMPERIAL



                                        })

                                    });




                                })

                                initMap(points);

                            } else {
                               // alert('else')
                                alertify.alert("Warning", "No data found.", function () { });
                                $scope.isNodata = true;
                                let points = [];
                                initMap(points);
                            }

                        } else {
                           // alert('else')
                            alertify.alert("Warning", "No data found.", function () { });
                            $scope.isNodata = true;
                            let points = [];
                            initMap(points);
                        }
                        




                    } else if (res.data.error) {
                        console.log(res.data)
                        alertify.alert("Error", res.data.error, function () { });
                    } else if (res.data.expire) {
                        window.location.href = "../Account/Login";
                    } else { window.location.href = "../Account/Login"; }

                });
            $scope.isMapReady = true;
            $scope.IsSearchCandidates = false;

        } else {
            alertify.alert("ERROR", "Please choose user", function () { });
        }
        
    }

    var directionsRenderer;

    var colors = [];

    function initMap(points) {
        console.log(points)

        const map = new google.maps.Map(document.getElementById("map_canvas"), {
            zoom: 6,
            scrollwheel: true,
            scaleControl: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_TOP
            },
            panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            center: {
                lat: 19.8762,
                lng: 75.3433
            }
        });
        const directionsService = new google.maps.DirectionsService();


        $(".dd").text('');

        for (var i = 0; i < points.length; i++) {
            calculateAndDisplayRoute(directionsService, points[i], i)

        }
        var points11 = [];
        function calculateAndDisplayRoute(directionsService, points, i) {

            colors.push(getRandomColor());
           // $(".dd").append(' <b style="background-color:' + colors[i] + ';padding:5px">test</b><br />')

            directionsService.route(
                points,
                (response, status) => {
                    console.log("directionsService");
                    console.log(response.request.transitOptions.departureTime);
                    if (status === "OK") {
                        directionsRenderer = new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: colors[i] } });
                        directionsRenderer.setDirections(response);
                        directionsRenderer.setMap(map);
                        //directionsRenderer.setOptions({ suppressMarkers: false });
                        //directionsRenderer.setOptions({
                        //    draggable: false,
                        //    // infoWindow: "sdsd sdsvds d",
                        //    suppressInfoWindows: false,
                        //    suppressMarkers: false,
                        //});


                       // $scope.colorsArray = colors;
                        //var myRoute = response.routes[0].legs[0];

                        //for (var i = 0; i < myRoute.steps.length; i++) {
                        //    for (var j = 0; j < myRoute.steps[i].lat_lngs.length; j++) {
                        //        points11.push(myRoute.steps[i].lat_lngs[j]);
                        //    }
                        //}
                        ////         alert("points "+points);
                        //var eventLine = new google.maps.Polyline({
                        //    path: points11,
                        //    //visible: true,
                        //    strokeColor:'red',
                        //    strokeOpacity: 0,
                        //    zIndex: 1000
                        //});
                        //eventLine.setMap(map);
                        //google.maps.event.addListener(eventLine, "mouseover", function () {
                        //    alert("hi")
                        //});

                    } else {
                        window.alert("Directions request failed due to " + status);
                    }
                }
            );
        }
        function setRouteTitle(dirsDispl, newTitle) {
            var ddObjKeys = Object.keys(dirsDispl);
            for (var i = 0; i < ddObjKeys.length; i++) {
                var obj = dirsDispl[Object(ddObjKeys)[i]];
                var ooObjKeys = Object.keys(obj);
                for (var j = 0; j < ooObjKeys.length; j++) {
                    var ooObj = obj[Object(ooObjKeys)[j]];
                    if ((ooObj) && (ooObj.hasOwnProperty('title')) && (ooObj.hasOwnProperty('shape')) && (ooObj.shape.type == 'circle')) {
                        ooObj.setTitle(newTitle);
                    }
                }
            }
        };
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
    }




});

