﻿


app.controller('MetersAllocatedMapCntrl', function ($scope, $filter, $rootScope, $timeout, $interval, $http) {

    // Proxy created on the fly
    var Hub = $.connection.kMHub;




    // Declare a function on the job hub so the server can invoke it
    Hub.client.displayMetersStatus = function () {

        //Getting  allocated meters by user Id  with condition of 'modified date is not null'
        $scope.GetAllocatedMetersByUser($scope.cat);
    };

    // Start the connection
   // $.connection.hub.start();

    $scope.cat = "89";
    $scope.SearchCandidates = function () {
        markers = [];
        $scope.GetAllocatedMetersByUser($scope.cat);
    }



  

    //Hub.client.getAllCounts = function (users) {
    //    console.log("SignalR getAllCounts");
    //    console.log(users);

    //    angular.forEach(users, function (user, i) {
    //        loadMarkers(user);
    //    })

    //    //$scope.usersList = users;

    //};



    $scope.GetAllocatedMetersByUser = function (Id) {
        //alert(val)
        markers = [];
        $http.get('../Master/GetAllallocatedMetersByUserId?id=' + Id)
            .then(function (res) {
                $scope.frmMap = false;
                console.log('---------getting online user from database for Map by Procedure--------------');
                if (res.data.success === 'true') {
                    console.log(res.data.AllocatedMetersForUsers)
                    console.log(res.data.RoutePoints)
                    $scope.kiranOnlineUsers = res.data.RoutePoints;
                    angular.forEach(res.data.AllocatedMetersForUsers, function (user, i) {
                        loadMarkers(user);
                    });



                } else if (res.data.error) {
                    console.log(res.data)
                    alertify.alert("Error", res.data.error, function () { });
                } else if (res.data.expire) {
                    window.location.href = "../Account/Login";
                } else { window.location.href = "../Account/Login"; }

            });
    }


    $scope.IsForgot = false;


    var markers = [];
    var uniqueId = 1;
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;

    let map;
    var infowindow = new google.maps.InfoWindow();
    //$rootScope.$on("CallParentMethodOfRemoveORAddOnlineUersForMap", function (event, res) {
    //    console.log('---------getting online user or not from signalR--------------')
    //    $scope.$apply(function () {
    //        console.log(res.onlineMembers);
    //        var onlineUsers = res.onlineMembers;
    //        //alert(onlineUsers.is_Online)
    //        //if (onlineUsers.is_Online === 'Y') {
    //        //    loadMarkers(onlineUsers);
    //        //} else {
    //        //    $.fn.DeleteMarker(onlineUsers.userID);
    //        //}
    //        loadMarkers(onlineUsers);
    //    });

    //});


    function initMap() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                console.log(position)
                //var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //you can use any location as center on map startup
                var latlng = new google.maps.LatLng(-29.8582455, 30.9694673); //you can use any location as center on map startup
                Globallatlng = latlng;
                var google_maps_geocoder = new google.maps.Geocoder();
                google_maps_geocoder.geocode(
                    { 'latLng': latlng },
                    function (results, status) {
                        console.log(results);
                    }
                );


                var myOptions = {
                    zoom: 5,
                    center: latlng,
                    styles: [{ "stylers": [{ "saturation": -100 }] }],
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    scaleControl: true,
                    streetViewControl: true,
                    streetViewControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    fullscreenControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };


                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                // Create the DIV to hold the control and call the makeInfoBox() constructor
                // passing in this DIV.
                var infoBoxDiv = document.createElement('div');
                makeInfoBox(infoBoxDiv, map);
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(infoBoxDiv);


               
                if ($scope.Online_Offline === 'Y') {
                    $("#checkBoxOnline").prop("checked", true);
                }


                

                $.connection.hub.start()
                    .done(function () {
                        console.log("hub connected!-------------");
                        //Getting all allocated meters by user Id
                        $scope.GetAllocatedMetersByUser($scope.cat);
                        console.log("end hub connected!-------------");
                    })
                    .fail(function () {
                        console.log("could not connect!");
                        $.connection.hub.stop();
                    });


                directionsDisplay.setMap(map);

               

            });
        } else {
            console.log("Browser doesn't support geolocation!");
        }


    }
    var waypts = [];
    function calculateAndDisplayRoute(directionsService, directionsDisplay,user) {           
        //if (parseFloat($scope.kiranOnlineUsers[0].lat) !== parseFloat(user.lat) && parseFloat($scope.kiranOnlineUsers[$scope.kiranOnlineUsers.length - 1].lat) !== parseFloat(user.lat)) {
        //    waypts.push({
        //        location: new google.maps.LatLng(parseFloat(user.lat), parseFloat(user.lng)),
        //        stopover: true
        //    })
        //}

        $scope.kiranOnlineUsers.forEach(function (c) {
            if (parseFloat($scope.kiranOnlineUsers[0].lat) !== parseFloat(user.lat) && parseFloat($scope.kiranOnlineUsers[$scope.kiranOnlineUsers.length - 1].lat) !== parseFloat(user.lat)) {
                waypts.push({
                    location: new google.maps.LatLng(parseFloat(c.lat), parseFloat(c.lng)),
                    stopover: true
                })
            }
        });


        var distancecal;
        directionsService.route({
            origin: new google.maps.LatLng(parseFloat($scope.kiranOnlineUsers[0].lat), parseFloat($scope.kiranOnlineUsers[0].lng)),
            destination: new google.maps.LatLng(parseFloat($scope.kiranOnlineUsers[$scope.kiranOnlineUsers.length - 1].lat), parseFloat($scope.kiranOnlineUsers[$scope.kiranOnlineUsers.length - 1].lng)),
            waypoints: waypts,
            optimizeWaypoints: false,
            travelMode: 'WALKING'
        }
        , function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
                //var summaryPanel = document.getElementById('distance');
                //summaryPanel.innerHTML = '';
                ////For each route, display summary information.
                //for (var i = 0; i < route.legs.length; i++) {
                //    var routeSegment = i + 1;
                //    summaryPanel.innerHTML += '<b>Route: ' + routeSegment +
                //        '</b><br>';
                //    summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                //    summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                //    summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                //    distancecal += route.legs[i].distance.text;
                //}
                //var summaryPanel1 = document.getElementById('distance1');
                //summaryPanel1.innerHTML = '';
                //var dd = distancecal.split('undefined')[1].split("km");
                ////alert(dd.length);
                //var xx = 0;
                //for (var i = 0; i < dd.length; i++) {
                //    //alert(dd[i])
                //    if (dd[i] != '') {
                //        xx = xx + parseInt(dd[i]);
                //    }



                //}
                ////alert(xx)
                //summaryPanel1.innerHTML = "<a style='color:blue'>Distance: </a>" + xx + " km";
            } else {
                //window.alert('Directions request failed due to ' + status);
            }
        });
    }


    initMap();
   
    function makeInfoBox(controlDiv, map) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.boxShadow = 'rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px';
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '2px';
        controlUI.style.marginBottom = '22px';
        controlUI.style.marginTop = '10px';
        controlUI.style.textAlign = 'center';
        //controlUI.style.position = 'absolute';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        //controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '100%';
        controlText.style.width = '200px';
        controlText.style.padding = '6px';
        controlText.onclick = function () {
            //alert($("#checkBoxOnline").is(":checked"))

            if ($("#checkBoxOnline").is(":checked")) {
                $scope.Online_Offline = 'Y';
            }
            else {
                $scope.Online_Offline = 'N';
            }

           // $scope.getCandidates();
        };
        controlText.innerHTML =
            '<div> <label  style="word-wrap:break-word;color:#1979a9!important"> <input style="vertical-align:middle;color:#1979a9!important"  id = "checkBoxOnline"  type = "checkbox"  /> Online Users </label>';
        controlUI.appendChild(controlText);
    }
    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }


    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }
    function initMapWhenSearch(filterCandidatesList) {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                console.log(position)
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //you can use any location as center on map startup
                Globallatlng = latlng;
                var google_maps_geocoder = new google.maps.Geocoder();
                google_maps_geocoder.geocode(
                    { 'latLng': latlng },
                    function (results, status) {
                        console.log(results);
                    }
                );




                var myOptions = {
                    zoom: 15,
                    center: latlng,
                    styles: [{ "stylers": [{ "saturation": -100 }] }],
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    scaleControl: true,
                    streetViewControl: true,
                    streetViewControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    fullscreenControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                // Create the DIV to hold the control and call the makeInfoBox() constructor
                // passing in this DIV.
                var infoBoxDiv = document.createElement('div');
                makeInfoBox(infoBoxDiv, map);
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(infoBoxDiv);



                angular.forEach(filterCandidatesList, function (user, i) {
                    loadMarkers(user);
                });




            });
        } else {
            console.log("Browser doesn't support geolocation!");
        }


    }

    function loadMarkers(user) {

        var url = "http://maps.google.com/mapfiles/ms/icons/";
        // alert(user.is_Online)
        if (user.Online_Status === "Y") {
            url += "green-dot.png";
        } else {
            url += "blue-dot.png";
        }
        //alert(url);
        //Determine the location where the user has clicked.
        var location = new google.maps.LatLng(parseFloat(user.lat), parseFloat(user.lng));
        const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let labelIndex = 0;
        //Create a marker and placed it on the map.
        var marker = new google.maps.Marker({
            position: location,
            icon: {
                url: url
            },

            map: map,
            //label: labels[labelIndex++ % labels.length],
            gestureHandling: "greedy",
            //animation: google.maps.Animation.DROP,
        });



        //Set unique id
        marker.id = parseInt(user.mId);
        uniqueId++;







        //Attach click event handler to the marker.
        google.maps.event.addListener(marker, "click", function (e) {
            if (infowindow) {
                infowindow.setMap(null);
                infowindow = null;
            }

            var content = 'Meter No: ' + user.meter + '<br />Latitude: ' + user.lat + '<br />Longitude: ' + user.lng + '<br />Online_Status: ' + user.Online_Status;
            content += "<br /><input type = 'button' va;ue = 'Delete' onclick = '$.fn.DeleteMarker(" + marker.id + ");' value = 'Delete' />";
            var infoWindow = new google.maps.InfoWindow({
                content: content
            });
            infoWindow.open(map, marker);
        });

        //Add marker to the array.
        markers.push(marker);

        // Overlay view allows you to organize your markers in the DOM
        // https://developers.google.com/maps/documentation/javascript/reference#OverlayView


        var myoverlay = new google.maps.OverlayView();

        myoverlay.draw = function () {
            // add an id to the layer that includes all the markers so you can use it in CSS
            this.getPanes().markerLayer.id = 'markerLayer';
        };
        myoverlay.setMap(map);


        $timeout(function () {
            if ($scope.Online_Offline === 'Y') {
                // alert($scope.Online_Offline)
                $("#checkBoxOnline").prop("checked", true);
            }
        }, 1000)







        $.fn.DeleteMarker = function (id) {
            //
            console.log("$.fn.DeleteMarker");
            console.log(markers.length);
            console.log(markers);
            //Find and remove the marker from the Array
            for (var i = 0; i < markers.length; i++) {

                console.log(markers[i]);
                alert(markers[i].id);
                if (markers[i].id === id) {
                    //Remove the marker from Map   
                    markers[i].setMap(null);
                    $("#SPViewModal").modal('hide');
                    //Remove the marker from array.
                    markers.splice(i, 1);
                    return;
                }
            }
        };


        if (user.Online_Status === "Y") {
            calculateAndDisplayRoute(directionsService, directionsDisplay, user);
        }
    }







});