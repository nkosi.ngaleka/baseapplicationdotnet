﻿
app.controller('RouteMapController', function ($scope, $filter, $rootScope, $timeout, $interval, $http) {

    $scope.BasicLatitude = -26.164726;
    $scope.BasicLongitude = 28.369397;

    // Proxy created on the fly
    var Hub = $.connection.kMHub;

    // Declare a function on the job hub so the server can invoke it
    Hub.client.displayMetersStatus = function () {
        //Getting  allocated meters by user Id  with condition of 'modified date is not null'
        $scope.GetAllocatedMetersByUser($scope.cat);
    };

    // Start the connection
    // $.connection.hub.start();

    $scope.travelMode = "DRIVING";

    $scope.cat = "74";
    $scope.SearchCandidates = function () {
        $scope.GetAllocatedMetersByUser($scope.cat);
    }



    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });


    $scope.isNodata = true;
    $scope.GetAllocatedMetersByUser = function (Id) {
        $scope.IsSearchCandidates = true;
        if (routingControl != null) {
            map.removeControl(routingControl);
            routingControl = null;
        }
        $scope.isNodata = true;
        if (Id && $scope.userRoute) {
            $http.get('../Master/GetAllallocatedMetersByUserId?id=' + Id + '&userRoute=' + $scope.userRoute)
                .then(function (res) {
                    $scope.frmMap = false;
                    console.log('---------getting online user from database for Map by Procedure--------------');
                    if (res.data.success === 'true') {
                        console.log(res.data)
                        //console.log(res.data.RoutePoints)
                        $scope.kiranOnlineUsers = res.data.RoutePoints;
                        //$scope.initialize($scope.kiranOnlineUsers);
                        $scope.LeafletMapiwthNavigation($scope.kiranOnlineUsers);
                        if ($scope.kiranOnlineUsers.length > 0) {
                            $scope.isNodata = false;
                           

                        } else {
                            alertify.alert("Warning", "No data found.", function () { });
                            $scope.isNodata = true;
                        }





                    } else if (res.data.error) {
                        console.log(res.data)
                        alertify.alert("Error", res.data.error, function () { });
                    } else if (res.data.expire) {
                        window.location.href = "../Account/Login";
                    } else { window.location.href = "../Account/Login"; }

                });
            $scope.IsSearchCandidates = false;

        } else {
            $scope.IsSearchCandidates = false;

            alertify.alert("Required", "User and route are required!.", function () { });
        }

    }



    $scope.GetLiveLocation = function (userId) {
        // alert(userId);
        $http.get('../Master/GetRouteCode?UserID=' + userId).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success === 'true') {
                $scope.UserRoutes = reasons.data.routeCode;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });

    }


    // LeafletMapiwthNavigation
    var map = L.map('map_canvas1', { preferCanvas: true }).setView([$scope.BasicLatitude, $scope.BasicLongitude], 7);
    mapLink = "<a href='http://openstreetmap.org'>OpenStreetMap</a>";
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: 'Leaflet &copy; ' + mapLink + ', contribution', maxZoom: 18 }).addTo(map);
    //L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
    //    maxZoom: 20,
    //    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    //});
    //var osm = L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"),
    //    mqi = L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png", { subdomains: ['otile1', 'otile2', 'otile3', 'otile4'] });

    //var baseMaps = {
    //    "OpenStreetMap": osm,
    //    "MapQuestImagery": mqi
    //};
    //var overlays = {//add any overlays here

    //};
    //L.control.layers(baseMaps, { position: 'bottomleft', attribution: 'Leaflet &copy; ' + mapLink + ', contribution', maxZoom: 18 }).addTo(map);

    var waypointsList = [];
    var routingControl = null;
    $scope.LeafletMapiwthNavigation = function (userData) {
        waypointsList = [];
        var message = [];
        map.eachLayer((layer) => {
            layer.remove();
        });
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: 'Leaflet &copy; ' + mapLink + ', contribution', maxZoom: 18 }).addTo(map);

        //alert(userData.length)
        if (userData.length > 0) {

            angular.forEach(userData, function (user, i) {
                try {
                    if (parseInt(user.lat) !== 0) {
                        waypointsList.push(L.latLng(parseInt(user.lat), parseInt(user.lng)));
                        message.push(user.meter_no);
                    }
                } catch (ex) {

                }
                
            });
            //console.log('message',message)
            var taxiIcon = L.icon({
                iconUrl: 'https://cdn1.iconfinder.com/data/icons/business-seo-elements-2/128/a_human_resources_location_male_man_marker_pin_user_-512.png',
                iconSize: [32, 32],
            })

            var marker = L.marker(waypointsList[0], { icon: taxiIcon }).addTo(map);



            //var popupcontent = `<h1>${message[i]}</h1>`;

             routingControl = L.Routing.control({
                plan: L.Routing.plan(waypointsList, {
                    createMarker: function (i, wp) {
                        //if (waypointsList[0]) {
                        return L.marker(wp.latLng, {
                            draggable: false
                        }).bindPopup(`<h3>Meter No : ${message[i]}</h3>`, { autoClose: false });//.openPopup();

                        //}

                    },
                    routeWhileDragging: true
                })
            }).on('routesfound', function (e) {
                var routes = e.routes;
                console.log(routes);
                
                e.routes[0].coordinates.forEach(function (coord, index) {

                    setTimeout(function () {
                        marker.setLatLng([coord.lat, coord.lng]);
                        
                    }, 10 * index)

                })


            }).addTo(map), waypointsList = [];


        } else {

           
        }
        
    }

   // $scope.LeafletMapiwthNavigation();



    //function animatePath(map, route, marker, pathCoords) {
    //    var index = 0;
    //    route.setPath([]);
    //    for (var index = 0; index < pathCoords.length; index++)
    //        setTimeout(function (offset) {
    //            route.getPath().push(pathCoords.getAt(offset));
    //            marker.setPosition(pathCoords.getAt(offset));
    //            map.panTo(pathCoords.getAt(offset));
    //            map.setZoom(18)
    //        }, index * 1000, index);

    //}



    //var mapOptions = {


    //    center: { lat: -25.9612731, lng: 28.6504824 },
    //    zoom: 10,
    //    streetViewControl: true,
    //    styles: [
    //        {
    //            "featureType": "administrative",
    //            "elementType": "geometry",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        },
    //        {
    //            "featureType": "administrative.land_parcel",
    //            "elementType": "labels",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        },
    //        {
    //            "featureType": "poi",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        },
    //        {
    //            "featureType": "poi",
    //            "elementType": "labels.text",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        },
    //        {
    //            "featureType": "road",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        },
    //        {
    //            "featureType": "road",
    //            "elementType": "labels.icon",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        },
    //        {
    //            "featureType": "road.local",
    //            "elementType": "labels",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        },
    //        {
    //            "featureType": "transit",
    //            "stylers": [
    //                {
    //                    "visibility": "off"
    //                }
    //            ]
    //        }
    //    ]
    //    //mapTypeId: google.maps.MapTypeId.ROADMAP
    //};

    //var map1 = new google.maps.Map(document.getElementById("map_canvas2"), mapOptions);


    //$scope.initialize = function (markers) {
    //    var map = new google.maps.Map(document.getElementById("map_canvas1"), mapOptions);
    //    //var map1 = new google.maps.Map(document.getElementById("map_canvas2"), mapOptions);
    //    var infoWindow = new google.maps.InfoWindow();
    //    var latlngbounds = new google.maps.LatLngBounds();
    //    var image = {
    //        url: 'https://cdn0.iconfinder.com/data/icons/user-icons-4/100/user-17-512.png',
    //        scaledSize: new google.maps.Size(40, 40), // scaled size
    //        //origin: new google.maps.Point(0, 0), // origin
    //        //anchor: new google.maps.Point(0, 0) // anchor
    //    };

    //    //Initialize the path
    //    var poly = new google.maps.Polyline({
    //        map: map,
    //        strokeColor: '#4986E7'
    //    });
    //    var traceMarker = new google.maps.Marker({
    //        map: map,
    //        icon: image
    //    });
    //    //Initialize the Direction Service
    //    var service = new google.maps.DirectionsService();
    //    var directionsDisplay = new google.maps.DirectionsRenderer();
    //    //Initialize the Path Array
    //    var path = new google.maps.MVCArray();


    //    if (markers.length > 0) {
    //        for (i = 0; i < markers.length; i++) {

    //            var data = markers[i];
    //            markers[i].latLng = new google.maps.LatLng(data.lat, data.lng);
    //            var markerMapIcon = "../Attachments/" + data.profileImg;

    //            var shape = {
    //                coords: [1, 1, 1, 20, 18, 20, 18, 1],
    //                type: 'poly'
    //            };

    //            var marker = new google.maps.Marker({
    //                position: markers[i].latLng,
    //                map: map,
    //                label: '' + (i + 1) + '',
    //                //shape: shape,
    //                //icon: data.profileImg ? image : null,
    //                title: data.meter_no
    //            });
    //            marker.description = "<b> Meter No : " + data.meter_no + "</b><br /> Start Date: " + $filter('date')(data.allocatedDate, "yyyy-MM-dd") + "<br /> Time: " + data.time;
    //            latlngbounds.extend(marker.position);
    //            google.maps.event.addListener(marker, "click", function (e) {
    //                infoWindow.setContent(this.description);
    //                infoWindow.open(map, this);
    //            });
    //        }
    //        map.setCenter(latlngbounds.getCenter());
    //        map.fitBounds(latlngbounds);




    //        // Get the route between the points on the map
    //        var wayPoints = [];
    //        for (var i = 1; i < markers.length - 1; i++) {
    //            wayPoints.push({
    //                location: markers[i].latLng,
    //                stopover: true
    //            });
    //        }

    //        directionsDisplay.setMap(null)
    //        directionsDisplay.setDirections({ routes: [] });
    //        directionsDisplay = null;



    //        directionsDisplay = new google.maps.DirectionsRenderer();
    //        directionsDisplay.setMap(map);

    //        if (markers.length >= 2) {
    //            service.route({
    //                origin: markers[0].latLng,
    //                destination: markers[markers.length - 1].latLng,
    //                waypoints: wayPoints,
    //                travelMode: google.maps.TravelMode[$scope.travelMode],
    //            }, function (result, status) {
    //                if (status == google.maps.DirectionsStatus.OK) {
    //                    for (var j = 0, len = result.routes[0].overview_path.length; j < len; j++) {
    //                        path.push(result.routes[0].overview_path[j]);
    //                    }
    //                    animatePath(map, poly, traceMarker, path);
    //                }
    //            });
    //        }

    //        document.getElementById("animate").addEventListener("click", function () {
    //            // Animate the path when the button is clicked
    //            animatePath(map, poly, traceMarker, path);
    //        });
    //    } else {
    //        //do else 
    //    }
    //};

    ////=====================
    //map1.data.loadGeoJson('../AngularJS/Home/eku.json');
    ////        //=====================

});



//Googlr route map with navigation  
//app.controller('RouteMapController', function ($scope, $filter, $rootScope, $timeout, $interval, $http) {

//    $scope.BasicLatitude = -25.9612731;
//    $scope.BasicLongitude = 28.6504824;

//    // Proxy created on the fly
//    var Hub = $.connection.kMHub;

//    // Declare a function on the job hub so the server can invoke it
//    Hub.client.displayMetersStatus = function () {
//        //Getting  allocated meters by user Id  with condition of 'modified date is not null'
//        $scope.GetAllocatedMetersByUser($scope.cat);
//    };

//    // Start the connection
//    // $.connection.hub.start();

//    $scope.travelMode = "DRIVING";

//    $scope.cat = "74";
//    $scope.SearchCandidates = function () {
//        $scope.GetAllocatedMetersByUser($scope.cat);
//    }



//    $http.post('../Master/GetFormConfig').then(function (reasons) {
//        if (reasons.data.success) {
//            $scope.UserList = reasons.data.users;
//        } else if (reasons.data.expire) {
//            window.location.href = "../Account/Login";
//        }
//    });


//    $scope.isNodata = true;
//    $scope.GetAllocatedMetersByUser = function (Id) {
//        $scope.isNodata = true;
//        if (Id && $scope.userRoute) {
//            $http.get('../Master/GetAllallocatedMetersByUserId?id=' + Id + '&userRoute=' + $scope.userRoute)
//                .then(function (res) {
//                    $scope.frmMap = false;
//                    console.log('---------getting online user from database for Map by Procedure--------------');
//                    if (res.data.success === 'true') {
//                        console.log(res.data)
//                        //console.log(res.data.RoutePoints)
//                        $scope.kiranOnlineUsers = res.data.RoutePoints;
//                        $scope.initialize($scope.kiranOnlineUsers);
//                        if ($scope.kiranOnlineUsers.length > 0) {
//                            $scope.isNodata = false;

//                        } else {
//                            alertify.alert("Warning", "No data found.", function () { });
//                            $scope.isNodata = true;
//                        }





//                    } else if (res.data.error) {
//                        console.log(res.data)
//                        alertify.alert("Error", "Problem showing User Level Access form. Please try after sometime.", function () { });
//                    } else if (res.data.expire) {
//                        window.location.href = "../Account/Login";
//                    } else { window.location.href = "../Account/Login"; }

//                });
//        } else{
//            alertify.alert("Required", "User and route are required!.", function () { });
//        }

//    }



//    $scope.GetLiveLocation = function (userId) {
//       // alert(userId);
//        $http.get('../Master/GetRouteCode?UserID=' + userId).then(function (reasons) {
//            console.log(reasons)
//            if (reasons.data.success==='true') {
//                $scope.UserRoutes = reasons.data.routeCode;
//            } else if (reasons.data.expire) {
//                window.location.href = "../Account/Login";
//            }
//        });

//    }


//    function animatePath(map, route, marker, pathCoords) {
//        var index = 0;
//        route.setPath([]);
//            for (var index = 0; index < pathCoords.length; index++)
//                setTimeout(function (offset) {
//                    route.getPath().push(pathCoords.getAt(offset));
//                    marker.setPosition(pathCoords.getAt(offset));
//                    map.panTo(pathCoords.getAt(offset));
//                    map.setZoom(18)
//                }, index * 1000, index);

//    }



//    var mapOptions = {


//        center: { lat: -25.9612731, lng: 28.6504824 },
//        zoom: 10,
//        streetViewControl: true,
//        styles: [
//            {
//                "featureType": "administrative",
//                "elementType": "geometry",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            },
//            {
//                "featureType": "administrative.land_parcel",
//                "elementType": "labels",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            },
//            {
//                "featureType": "poi",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            },
//            {
//                "featureType": "poi",
//                "elementType": "labels.text",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            },
//            {
//                "featureType": "road",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            },
//            {
//                "featureType": "road",
//                "elementType": "labels.icon",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            },
//            {
//                "featureType": "road.local",
//                "elementType": "labels",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            },
//            {
//                "featureType": "transit",
//                "stylers": [
//                    {
//                        "visibility": "off"
//                    }
//                ]
//            }
//        ]
//        //mapTypeId: google.maps.MapTypeId.ROADMAP
//    };

//    var map1 = new google.maps.Map(document.getElementById("map_canvas2"), mapOptions);


//    $scope.initialize = function (markers) {
//        var map = new google.maps.Map(document.getElementById("map_canvas1"), mapOptions);
//        //var map1 = new google.maps.Map(document.getElementById("map_canvas2"), mapOptions);
//        var infoWindow = new google.maps.InfoWindow();
//        var latlngbounds = new google.maps.LatLngBounds();
//        var image = {
//            url: 'https://cdn0.iconfinder.com/data/icons/user-icons-4/100/user-17-512.png',
//            scaledSize: new google.maps.Size(40, 40), // scaled size
//            //origin: new google.maps.Point(0, 0), // origin
//            //anchor: new google.maps.Point(0, 0) // anchor
//        };

//        //Initialize the path
//        var poly = new google.maps.Polyline({
//            map: map,
//            strokeColor: '#4986E7'
//        });
//        var traceMarker = new google.maps.Marker({
//            map: map,
//            icon: image
//        });
//        //Initialize the Direction Service
//        var service = new google.maps.DirectionsService();
//        var directionsDisplay = new google.maps.DirectionsRenderer();
//        //Initialize the Path Array
//        var path = new google.maps.MVCArray();


//        if (markers.length > 0) {
//            for (i = 0; i < markers.length; i++) {

//                var data = markers[i];
//                markers[i].latLng = new google.maps.LatLng(data.lat, data.lng);
//                var markerMapIcon = "../Attachments/" + data.profileImg;

//                var shape = {
//                    coords: [1, 1, 1, 20, 18, 20, 18, 1],
//                    type: 'poly'
//                };

//                var marker = new google.maps.Marker({
//                    position: markers[i].latLng,
//                    map: map,
//                    label: '' + (i + 1) + '',
//                    //shape: shape,
//                    //icon: data.profileImg ? image : null,
//                    title: data.meter_no
//                });
//                marker.description = "<b> Meter No : " + data.meter_no + "</b><br /> Start Date: " + $filter('date')(data.allocatedDate, "yyyy-MM-dd") + "<br /> Time: " + data.time;
//                latlngbounds.extend(marker.position);
//                google.maps.event.addListener(marker, "click", function (e) {
//                    infoWindow.setContent(this.description);
//                    infoWindow.open(map, this);
//                });
//            }
//            map.setCenter(latlngbounds.getCenter());
//            map.fitBounds(latlngbounds);




//            // Get the route between the points on the map
//            var wayPoints = [];
//            for (var i = 1; i < markers.length - 1; i++) {
//                wayPoints.push({
//                    location: markers[i].latLng,
//                    stopover: true
//                });
//            }

//            directionsDisplay.setMap(null)
//            directionsDisplay.setDirections({ routes: [] });
//            directionsDisplay = null;



//            directionsDisplay = new google.maps.DirectionsRenderer();
//            directionsDisplay.setMap(map);

//            if (markers.length >= 2) {
//                service.route({
//                    origin: markers[0].latLng,
//                    destination: markers[markers.length - 1].latLng,
//                    waypoints: wayPoints,
//                    travelMode: google.maps.TravelMode[$scope.travelMode],
//                }, function (result, status) {
//                    if (status == google.maps.DirectionsStatus.OK) {
//                        for (var j = 0, len = result.routes[0].overview_path.length; j < len; j++) {
//                            path.push(result.routes[0].overview_path[j]);
//                        }
//                        animatePath(map, poly, traceMarker, path);
//                    }
//                });
//            }

//            document.getElementById("animate").addEventListener("click", function () {
//                // Animate the path when the button is clicked
//                animatePath(map, poly, traceMarker, path);
//            });
//        } else {
//            //do else 
//        }
//    };

//    //=====================
//    map1.data.loadGeoJson('../AngularJS/Home/eku.json');
////        //=====================

//});




//app.controller('RouteMapController', function ($scope, $filter, $rootScope, $timeout, $interval, $http) {


//    // Proxy created on the fly
//    var Hub = $.connection.kMHub;

//    // Declare a function on the job hub so the server can invoke it
//    Hub.client.displayMetersStatus = function () {

//        //Getting  allocated meters by user Id  with condition of 'modified date is not null'
//        $scope.GetAllocatedMetersByUser($scope.cat);
//    };

//    // Start the connection
//    // $.connection.hub.start();

//    $scope.cat = "74";
//    $scope.SearchCandidates = function () {
//        $scope.GetAllocatedMetersByUser($scope.cat);
//    }



//    $http.post('../Master/GetFormConfig').then(function (reasons) {
//        if (reasons.data.success) {
//            $scope.UserList = reasons.data.users;
//        } else if (reasons.data.expire) {
//            window.location.href = "../Account/Login";
//        }
//    });




//    $scope.GetAllocatedMetersByUser = function (Id) {
//        path = [];
//        $http.get('../Master/GetAllallocatedMetersByUserId?id=' + Id)
//            .then(function (res) {
//                $scope.frmMap = false;
//                console.log('---------getting online user from database for Map by Procedure--------------');
//                if (res.data.success === 'true') {
//                    console.log(res.data)
//                    //console.log(res.data.RoutePoints)
//                    $scope.kiranOnlineUsers = res.data.RoutePoints;

//                    //alert($scope.kiranOnlineUsers.length)
//                    if ($scope.kiranOnlineUsers.length > 0) {

//                        //var source = new google.maps.LatLng(parseFloat($scope.kiranOnlineUsers[0].lat), parseFloat($scope.kiranOnlineUsers[0].lng));
//                        //var destination = new google.maps.LatLng(parseFloat($scope.kiranOnlineUsers[$scope.kiranOnlineUsers.length - 1].lat), parseFloat($scope.kiranOnlineUsers[$scope.kiranOnlineUsers.length - 1].lng));



//                        ////path = res.data.RoutePoints;
//                        //angular.forEach($scope.kiranOnlineUsers, function (c, i) {
//                        //    path.push([parseFloat(c.lng), parseFloat(c.lat)]);
//                        //    if (i != 0 && i != $scope.kiranOnlineUsers.length - 1) {
//                        //        var waystop1 = new google.maps.LatLng(parseFloat(c.lat), parseFloat(c.lng));

//                        //        waypts.push({
//                        //            location: waystop1,
//                        //            stopover: true
//                        //        });
//                        //    }
//                        //});
//                        //$scope.resizingMap(source, destination, waypts, $scope.kiranOnlineUsers);
//                        $scope.initialize($scope.kiranOnlineUsers);

//                    }





//                } else if (res.data.error) {
//                    console.log(res.data)
//                    alertify.alert("Error", "Problem showing User Level Access form. Please try after sometime.", function () { });
//                } else if (res.data.expire) {
//                    window.location.href = "../Account/Login";
//                } else { window.location.href = "../Account/Login"; }

//            });
//    }






//    function animatePath(map, route, marker, pathCoords) {
//        var index = 0;
//        route.setPath([]);
//        for (var index = 0; index < pathCoords.length; index++)
//            setTimeout(function (offset) {
//                route.getPath().push(pathCoords.getAt(offset));
//                marker.setPosition(pathCoords.getAt(offset));
//                map.panTo(pathCoords.getAt(offset));
//            }, index * 100, index);
//    }



//    var mapOptions = {
//        center: { lat: 41.85, lng: -87.65 },
//        zoom: 20,
//        //mapTypeId: google.maps.MapTypeId.ROADMAP
//    };
//    var map = new google.maps.Map(document.getElementById("map_canvas1"), mapOptions);
//    var infoWindow = new google.maps.InfoWindow();
//    var latlngbounds = new google.maps.LatLngBounds();
//    $scope.initialize = function (markers) {

//        for (i = 0; i < markers.length; i++) {

//            var data = markers[i];
//            markers[i].latLng = new google.maps.LatLng(data.lat, data.lng);
//            var markerMapIcon = "../Attachments/" + data.profileImg;
//            var image = {
//                url: 'https://cdn0.iconfinder.com/data/icons/user-icons-4/100/user-17-512.png',
//                scaledSize: new google.maps.Size(40, 40), // scaled size
//                //origin: new google.maps.Point(0, 0), // origin
//                //anchor: new google.maps.Point(0, 0) // anchor
//            };
//            var shape = {
//                coords: [1, 1, 1, 20, 18, 20, 18, 1],
//                type: 'poly'
//            };

//            var marker = new google.maps.Marker({
//                position: markers[i].latLng,
//                map: map,
//                label: '' + (i + 1) + '',
//                //shape: shape,
//                //icon: data.profileImg ? image : null,
//                title: data.meter_no
//            });
//            marker.description = "<b> Meter No : " + data.meter_no + "</b><br /> Start Date: " + $filter('date')(data.allocatedDate, "yyyy-MM-dd") + "<br /> Time: " + data.time;
//            latlngbounds.extend(marker.position);
//            google.maps.event.addListener(marker, "click", function (e) {
//                infoWindow.setContent(this.description);
//                infoWindow.open(map, this);
//            });
//        }
//        map.setCenter(latlngbounds.getCenter());
//        map.fitBounds(latlngbounds);

//        //Initialize the Path Array
//        var path = new google.maps.MVCArray();

//        //Initialize the Direction Service
//        var service = new google.maps.DirectionsService();

//        // Get the route between the points on the map
//        var wayPoints = [];
//        var wayPointsPath = [];
//        for (var i = 1; i < markers.length - 1; i++) {
//            wayPoints.push({
//                location: markers[i].latLng,
//                stopover: true
//            });


//        }
//        for (var i = 0; i < markers.length; i++) {
//            wayPointsPath.push(markers[i].latLng)
//        }

//        //Initialize the path
//        var poly = new google.maps.Polyline({
//            map: map,
//            strokeColor: '#4986E7'
//        });


//        var traceMarker = new google.maps.Marker({
//            map: map,
//            icon: image
//        });


//        const lineSymbol = {
//            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
//            scale: 4,
//            strokeColor: '#4986E7',
//        };
//        // Create the polyline and add the symbol to it via the 'icons' property.
//        const line = new google.maps.Polyline({
//            path: wayPointsPath,
//            strokeColor: '#4986E7',
//            icons: [
//                {
//                    icon: lineSymbol,
//                    offset: "100%",
//                },
//            ],
//            map: map,
//        });

//        if (markers.length >= 2) {
//            service.route({
//                origin: markers[0].latLng,
//                destination: markers[markers.length - 1].latLng,
//                waypoints: wayPoints,
//                travelMode: google.maps.DirectionsTravelMode.DRIVING
//            }, function (result, status) {
//                if (status == google.maps.DirectionsStatus.OK) {
//                    for (var j = 0, len = result.routes[0].overview_path.length; j < len; j++) {
//                        path.push(result.routes[0].overview_path[j]);
//                    }
//                    //animatePath(map, poly, lineSymbol, path);
//                    animateCircle(line);
//                }
//            });
//        }

//        document.getElementById("animate").addEventListener("click", function () {
//            // Animate the path when the button is clicked
//            animatePath(map, poly, traceMarker, path);
//        });

//    };


//    // Use the DOM setInterval() function to change the offset of the symbol
//    // at fixed intervals.
//    function animateCircle(line) {
//        let count = 0;
//        window.setInterval(() => {
//            count = (count + 1) % 200;

//            const icons = line.get("icons");

//            icons[0].offset = count / 2 + "%";
//            line.set("icons", icons);
//        }, 100);
//    }


//});

