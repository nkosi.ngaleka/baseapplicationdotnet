﻿app.controller('ReadController', function ($scope, $http, $filter) {

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "METER_NO";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getUsers = function () {

        var url = '../Master/GetReadMeters'
        var params = {
            UserID: $scope.UserID,
            Unreviewed: true,
            pageIndex: $scope.pageIndex,
            pageSize: $scope.pageSizeSelected,
            sorting: $scope.sorting,
            orderType: $scope.orderType,
            search: $scope.search
        };

        console.log(params);

        $http.post(url, params)
            .then(function (response) {
                $scope.frmMap = false;
                if (response.data.success === true) {
                    console.log(response)
                    if (response.data.items.length > 0) {
                        $scope.items = response.data.items;
                        $scope.totalCount = response.data.totalCount;


                    } else {
                        alertify.alert("SUCCESS", "Meters Not Available", function () { });

                    }
                    $scope.showLoader = false;
                    $scope.showTable = true;

                } else if (response.data.error) {
                    $scope.showLoader = false;
                    alertify.alert("Error", response.data.error, function () { });
                } else if (reasons.data.expire) {
                    window.location.href = "../Account/Login";
                } else { window.location.href = "../Account/Login"; }

                $scope.showLoader = false;
                $scope.showTable = true;
            });

        //var url = "../Master/GetAllocatedERF?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search;

        //$http.get(url)
        //    .then(function (response) {

        //        if (response.data.success) {
        //            console.log(response.data.success);
        //            $scope.items = response.data.success.items;
        //            $scope.totalCount = response.data.success.totalCount;
        //            $scope.showLoader = false;
        //            $scope.showTable = true;
        //        } else if (response.data.expire) {
        //            window.location.href = '../Home/Dashboard';
        //        }
        //    }, function (err) {
        //        var error = err;
        //    });
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "FirstName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.getUsers();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getUsers();
    }

    //Loading employees list on first time
    $scope.getUsers();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getUsers();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getUsers();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});