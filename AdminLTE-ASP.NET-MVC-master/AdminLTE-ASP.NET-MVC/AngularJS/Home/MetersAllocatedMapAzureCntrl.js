﻿app.controller('MetersAllocatedMapAzureCntrl', function ($scope, $filter, $rootScope, $timeout, $interval, $http) {

    // Proxy created on the fly
    var Hub = $.connection.kMHub;

    // Declare a function on the job hub so the server can invoke it
    Hub.client.displayMetersStatus = function () {

        //Getting  allocated meters by user Id  with condition of 'modified date is not null'
        $scope.GetAllocatedMetersByUser($scope.cat);
    };

    // Start the connection
    // $.connection.hub.start();

    $scope.cat = "74";
    $scope.SearchCandidates = function () {
        $scope.GetAllocatedMetersByUser($scope.cat);
    }



    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });





    $scope.GetAllocatedMetersByUser = function (Id) {
        path = [];
        $http.get('../Master/GetAllallocatedMetersByUserId?id=' + Id)
            .then(function (res) {
                $scope.frmMap = false;
                console.log('---------getting online user from database for Map by Procedure--------------');
                if (res.data.success === 'true') {
                    console.log(res.data.AllocatedMetersForUsers)
                    console.log(res.data.RoutePoints)
                    $scope.kiranOnlineUsers = res.data.RoutePoints;
                    //angular.forEach(res.data.AllocatedMetersForUsers, function (user, i) {
                    //    loadMarkers(user);
                    //});
                    //path = [
                    //    [30.9577, -29.7854],
                    //    [30.9591, -29.7851],
                    //    [30.9582, -29.7852],
                    //    [30.9597, -29.7847],
                    //    [30.9577, -29.7852]
                    //];
                    //console.log(path)

                    //path = res.data.RoutePoints;
                    angular.forEach($scope.kiranOnlineUsers, function (c, i) {
                        path.push([parseFloat(c.lng), parseFloat(c.lat)])
                    });

                    //$scope.GetMap();

                    if ($scope.kiranOnlineUsers.length === path.length) {
                        console.log(path);

                        //$scope.GetMap();

                    }





                } else if (res.data.error) {
                    console.log(res.data)
                    alertify.alert("Error", res.data.error, function () { });
                } else if (res.data.expire) {
                    window.location.href = "../Account/Login";
                } else { window.location.href = "../Account/Login"; }

            });
    }


    ////..........................................MAP functionaty...........................................................
    //var map;
    //var geocoder = new google.maps.Geocoder();
    //var marker;
    //var infowindow;
    //var SourceDestinationPoints = [];
    //var waypts = [];
    //var Contentlocations = [];
    //var km;
    //var kirannn = [];
    //var Stime1, JourneySDate1, JourneyEDate1;

    //$scope.createMarker = function (latlng, label, html, url) {
    //    //alert(url)
    //    var image = {
    //        url: url,
    //        scaledSize: new google.maps.Size(20, 20), // scaled size
    //        origin: new google.maps.Point(0, 0), // origin
    //        anchor: new google.maps.Point(0, 0) // anchor
    //    };
    //    var shape = {
    //        coords: [1, 1, 1, 20, 18, 20, 18, 1],
    //        type: 'poly'
    //    };
    //    //var icon = new google.maps.MarkerImage(""+url+"", new google.maps.Size(35, 36), new google.maps.Point(103, 34));
    //    var contentString = '<b>' + label + '</b><br>' + html;
    //    //alert(contentString)
    //    var marker = new google.maps.Marker({
    //        position: latlng,
    //        map: map,
    //        icon: image,
    //        //shape: shape,
    //        title: label,
    //        //zIndex: 1
    //    });
    //    //alert('kiran')
    //    google.maps.event.addListener(marker, 'click', function () {

    //        infowindow.setContent(contentString);
    //        infowindow.open(map, marker);
    //    });
    //    //alert('manam')
    //}


    //function resizingMap() {
    //    alert()
    //    //alert('resizingMap')
    //    $("#map-canvas").text("");
    //    $("#errormap").text("");
    //    //SourceDestinationPoints = [];
    //    //waypts = [];
    //    //while (waypts.length > 0) {
    //    //    //alert(waypts.length)
    //    //    waypts.pop();
    //    //}
    //    //while (SourceDestinationPoints.length > 0) {
    //    //    //alert(SourceDestinationPoints.length)
    //    //    SourceDestinationPoints.pop();
    //    //}
    //    //if (Mulstops == "") {
    //    //    //
    //    //}
    //    //else {
           
    //        var waystop1 = new google.maps.LatLng(14.442599, 79.986458);
         
    //        waypts.push({
    //            location: waystop1,
    //            stopover: true
    //        });
    //        //for (var i = 0; i < Mulstops.length; i++) {

    //        //    waypts.push({
    //        //        location: Mulstops[i][0],
    //        //        stopover: true
    //        //    });

    //        //}
    //   /* }*/
    //    var first = new google.maps.LatLng(17.385044, 78.486671);
    //    var second = new google.maps.LatLng(13.082680, 80.270721);
    //    var distancecal;
    //    $("#map-canvas").css({ "height": "400px", "margin:": "0" })
    //    $("#map-canvas1").css({ "height": "0", "margin:": "0" })
    //    var directionsService = new google.maps.DirectionsService;
    //    //var directionsDisplay = new google.maps.DirectionsRenderer;
    //    var directionsDisplay = new google.maps.DirectionsRenderer({
    //        suppressMarkers: true
    //    });
    //    map = new google.maps.Map(document.getElementById('map-canvas'), {
    //        zoom: 6
    //        //center: {lat: 41.85, lng: -87.65}
    //    });
                   

    //    infowindow = new google.maps.InfoWindow();
    //    directionsDisplay.setMap(map);
    //    //SourceDestinationPoints.push(Source);
    //    //SourceDestinationPoints.push(Destination);
    //    directionsService.route({
    //        origin: first,
    //        destination: second,
    //        waypoints: waypts,
    //        optimizeWaypoints: true,
    //        travelMode: 'DRIVING'
    //    }, function (response, status) {
    //        if (status === 'OK') {
    //            directionsDisplay.setDirections(response);
    //            var route = response.routes[0];
    //            console.log(response.routes[0])

    //            var summaryPanelj = $("#distance"); //document.getElementById('distance');
    //            summaryPanelj.text("");
    //            summaryPanelj.val("");
    //            var summaryPanel = document.getElementById('distance');
    //            summaryPanel.innerText = "";
    //            summaryPanel.innerHTML = '';
    //            //For each route, display summary information.
    //            for (var i = 0; i < route.legs.length; i++) {

    //                var routeSegment = i + 1;
    //                summaryPanel.innerHTML += '<b>Route: ' + routeSegment +
    //                    '</b><br>';
    //                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
    //                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
    //                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
    //                console.log('route.legs[i].distance')
    //                console.log(route.legs[i].duration.text)
    //                distancecal += route.legs[i].distance.text;
    //            }
    //            // Initialize the Geocoder
    //            //geocoder = new google.maps.Geocoder();

    //            if (geocoder) {
    //                //for (var k = 0; k < SourceDestinationPoints.length; k++) {
    //                geocoder.geocode({ 'address': SourceDestinationPoints[0] }, function (results, status) {

    //                    if (status === google.maps.GeocoderStatus.OK) {
    //                        $scope.createMarker(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()), "" + SourceDestinationPoints[0] + "", "<br /> Start Date: " + $filter('date')(new Date(Date.parse(JourneySDate1, "dd-MM-yyyy")), "yyyy-MM-dd") + "<br /> Time: " + Stime1 + "", "http://localhost:1342/Trans_Icons/truck3.png")

    //                    }
    //                });

    //                geocoder.geocode({ 'address': SourceDestinationPoints[1] }, function (results, status) {

    //                    if (status == google.maps.GeocoderStatus.OK) {
    //                        $scope.createMarker(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()), "" + SourceDestinationPoints[1] + "", "<br /> Start Date: " + $filter('date')(new Date(Date.parse(JourneyEDate1, "dd-MM-yyyy")), "yyyy-MM-dd") + "", "../Trans_Icons/truck3.png")

    //                    }
    //                });

    //                //}
    //            }
    //            var km1 = 0;
    //            for (var km = 0; km < waypts.length; km++) {
    //                if (geocoder) {

    //                    geocoder.geocode({
    //                        'address': waypts[km].location
    //                    }, function (results, status) {
    //                        if (status == google.maps.GeocoderStatus.OK) {
    //                            alert('3')
    //                            $scope.createMarker(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()), "" + waypts[km1].location + "", "<br /> Start Date: 2-2-2<br /> Time: 09:00Pm", "http://localhost:1342/Trans_Icons/truck3.png")
    //                            km1++;
    //                        }


    //                    });
    //                }
    //            }
    //            var summaryPanel1j = $("#distance1"); //document.getElementById('distance');
    //            summaryPanel1j.text("");
    //            summaryPanel1j.val("");
    //            var summaryPanel1 = document.getElementById('distance1');
    //            summaryPanel1.innerText = "";
    //            summaryPanel1.innerHTML = '';
    //            var dd = distancecal.split('undefined')[1].split("km");

    //            var xx = 0;
    //            for (var i = 0; i < dd.length; i++) {

    //                if (dd[i] != '') {
    //                    //alert(dd[i]);
    //                    xx = xx + parseInt(dd[i]);
    //                }



    //            }

    //            //summaryPanel1.innerHTML = "<a style='color:blue'>Distance: </a>" + xx + " km";
    //        } else {
    //            //window.alert('Directions request failed due to ' + status);
    //        }
    //    });
    //}
    //resizingMap();
    //$scope.GetMapRoute = function (Route, Mulstops, Source, Destination, STime, JourneySDate, JourneyEDate) {
    //    Stime1 = "";
    //    Stime1 = STime;
    //    JourneySDate1 = JourneySDate;
    //    JourneyEDate1 = JourneyEDate;
    //    while (kirannn.length > 0) {
    //        kirannn.pop();
    //    }
    //    for (var i = 0; i < Mulstops.length; i++) {
    //        if (Mulstops[i] != "") {
    //            var cc = Mulstops[i].split("!");

    //            kirannn.push(cc)

    //        }
    //    }
    //    console.log(kirannn);


    //    $('#myModal1').on('shown.bs.modal', function () {
    //        resizingMap(Route, kirannn, Source, Destination);

    //    }).modal('show');



    //}





    ////Azure Map

    //var map, pin, lineSource, pinSource;
    //var animationTime = 20000;
    //var animation;
    //var path = [];

    ////Create an array of points to define a path to animate along.
    ////var path = [
    ////    [30.9577, -29.7854],
    ////    [30.9591, -29.7851],
    ////    [30.9582, -29.7852],
    ////    [30.9597, -29.7847],
    ////    [30.9577, -29.7852]
    ////];

    //$scope.GetMap = function () {
    //    //Initialize a map instance.
    //    map = new atlas.Map('myMap', {
    //        center: path.length > 0 ? path[0] : [30.9696, -29.8791],
    //        zoom: path.length > 5 ? 10 : 16,
    //        view: 'Auto',

    //        //Add authentication details for connecting to Azure Maps.
    //        authOptions: {
    //            //Alternatively, use an Azure Maps key. Get an Azure Maps key at https://azure.com/maps. NOTE: The primary key should be used as the key.
    //            authType: 'subscriptionKey',
    //            subscriptionKey: 'XM04w2lUIwI4O5zwiSNpnnzIAHl_HiwAIbaJKA0K8IM'// '3ypllNUu43PnCq0SWJoN_dmOW4ncNT0tb_jDjxh1Mm8'// '<Your Azure Maps Key>'
    //        }
    //    });
    //    /* Set the style of the map*/
    //    //setTimeout(function () {
    //        map.setStyle({ style: 'satellite' });
    //   // }, 5000);
    //    //Wait until the map resources are ready.
    //    map.events.add('ready', function () {

    //        //Load a custom image icon into the map resources.
    //        map.imageSprite.createFromTemplate('arrow-icon', 'marker-arrow', 'teal', '#fff').then(function () {

    //            //Create data sources and add them to the map.
    //            lineSource = new atlas.source.DataSource();
    //            pinSource = new atlas.source.DataSource();
    //            map.sources.add([lineSource, pinSource]);

    //            //Create a layer to render the path.
    //            map.layers.add(new atlas.layer.LineLayer(lineSource, null, {
    //                strokeColor: 'DodgerBlue',
    //                strokeWidth: 4
    //            }));

    //            //Create a line for the path and add it to the data source.
    //            lineSource.add(new atlas.data.LineString(path));

    //            //Create a layer to render a symbol which we will animate.
    //            map.layers.add(new atlas.layer.SymbolLayer(pinSource, null, {
    //                iconOptions: {
    //                    //Pass in the id of the custom icon that was loaded into the map resources.
    //                    image: 'arrow-icon',

    //                    //Anchor the icon to the center of the image.
    //                    anchor: 'center',

    //                    //Rotate the icon based on the rotation property on the point data.
    //                    //The arrow icon being used in this case points down, so we have to rotate it 180 degrees.
    //                    rotation: ['+', 180, ['get', 'heading']],

    //                    //Have the rotation align with the map.
    //                    rotationAlignment: 'map',

    //                    //For smoother animation, ignore the placement of the icon. This skips the label collision calculations and allows the icon to overlap map labels.
    //                    ignorePlacement: true,

    //                    //For smoother animation, allow symbol to overlap all other symbols on the map.
    //                    allowOverlap: true
    //                },
    //                textOptions: {
    //                    //For smoother animation, ignore the placement of the text. This skips the label collision calculations and allows the text to overlap map labels.
    //                    ignorePlacement: true,

    //                    //For smoother animation, allow text to overlap all other symbols on the map.
    //                    allowOverlap: true
    //                }
    //            }));

    //            //Create a pin and wrap with the shape class and add to data source.
    //            pin = new atlas.Shape(new atlas.data.Feature(new atlas.data.Point(path[0])));
    //            pinSource.add(pin);

    //            //Create the animation.
    //            animation = atlas.animations.moveAlongPath(path, pin, {
    //                //Capture metadata so that data driven styling can be done.
    //                captureMetadata: true,

    //                duration: path.length > 5 ? 1000000 : 50000,
    //                loop: document.getElementById('loopAnimation').checked,
    //                reverse: document.getElementById('reverseAnimation').checked,
    //                rotationOffset: (document.getElementById('reverseAnimation').checked) ? 90 : 0,

    //                //If following enabled, add a map to the animation.
    //                map: (document.getElementById('followSymbol').checked) ? map : null,

    //                //Camera options to use when following.
    //                zoom: path.length > 5 ? 17 : 18,
    //                pitch: 45,
    //                rotate: true
    //            });
    //        });
    //    });
    //}

    //$scope.animationPlay = function () {
    //    animation.play()
    //}
    //$scope.animationPause = function () {
    //    animation.pause()
    //}
    //$scope.animationStop = function () {
    //    animation.stop()
    //}
    //$scope.animationReset = function () {
    //    animation.reset()
    //}

    //$scope.toggleFollow = function () {
    //    animation.setOptions({
    //        map: (animation.getOptions().map) ? null : map
    //    });
    //}

    //$scope.toggleFollowOffset = function () {
    //    animation.setOptions({
    //        rotationOffset: (animation.getOptions().rotationOffset === 0) ? 90 : 0
    //    });
    //}

    //$scope.toggleLooping = function () {
    //    animation.setOptions({
    //        loop: !animation.getOptions().loop
    //    });
    //};

    //$scope.toggleReverse = function () {
    //    animation.setOptions({
    //        reverse: !animation.getOptions().reverse
    //    });
    //}






});