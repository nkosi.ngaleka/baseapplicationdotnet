﻿app.controller('DashboardCtrl', function (config, $scope, $http, $filter, $timeout) {
    $scope.LoadedCount = '-';
    $scope.AllocatedCount = '-';
    $scope.ReadCount = '-';
    $scope.NotReadCount = '-';
    $scope.CompletedCount = '-';
    $scope.OpenProjectsCount = '-';
    $scope.ConditionWiseAssetPercent = {};
    $scope.ProjectsLocations = {};
    $scope.BasicLatitude = -26.164726;
    $scope.BasicLongitude = 28.369397;
    $scope.MetersMsg = 'Allocated Meters';
    $scope.isLoading = false;
    $scope.isAdmin = false;
    $scope.solarDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.MetersLocations = [];
    //Getting meters locations by click method
    $scope.GetMetersLocationByChange = function (val) {
        $scope.showPopupMapBasic()
        $scope.isLoading = true;
        switch (val) {
            case 'all':
                $scope.MetersMsg = 'Allocated Meters';
                break;
            case 'read':
                $scope.MetersMsg = 'Read Meters';
                break;
            case 'unread':
                $scope.MetersMsg = 'Unread Meters';
                break;
        }

        $scope.GetMetersLocations(val);

    }

    $scope.getMetersCount = function () {
        $http.post('../Home/GetDashboardCounts').then(function (response) {
            console.log('Dashboard', response.data);

            if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            } else {
                if (response.data.MetersCount.success) {
                    $scope.LoadedCount = response.data.MetersCount.items[0].Noofrecords;
                    $scope.AllocatedCount = response.data.MetersCount.items[1].Noofrecords;
                    $scope.ReadCount = response.data.MetersCount.items[2].Noofrecords;
                    $scope.NotReadCount = response.data.MetersCount.items[3].Noofrecords;
                    $scope.CompletedCount = (parseFloat($scope.ReadCount) / parseFloat($scope.AllocatedCount)) * 100;
                    // alert($scope.AllocatedCount)
                } else {
                    $scope.LoadedCount = '-';
                    $scope.AllocatedCount = '-';
                    $scope.ReadCount = '-';
                    $scope.NotReadCount = '-';
                    $scope.CompletedCount = '-';
                }
            }
               

            });
    }

    $scope.getSolarData = function () {
        $http.post('../Home/GetSolarCounts', { solarDate: $scope.solarDate}).then(function (response) {
            console.log('SolarCounts', response.data);

            if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            } else {
                if (response.data.SolarCount.success) {
                    $scope.SolarAttempted = response.data.SolarCount.Attempted[0].SolarAttempted;
                    $scope.SolarFailed = response.data.SolarCount.Failed[0].SolarFailed;
                    $scope.SolarSuccess = response.data.SolarCount.Successed[0].SolarSuccess;
                   
                } else {
                    $scope.SolarAttempted = '-';
                    $scope.SolarFailed = '-';
                    $scope.SolarSuccess = '-';
                    
                }
            }


        });
    }

    $scope.GetMetersLocations = function (val) {
        $scope.MetersLocations = [];
        debugger;
        $http.post('../Home/GetDashboardData?meterStatus=' + val).then(function (response) {
            console.log('Dashboard', response.data);
           
            if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            } else {
                if (response.data.MetersLocationsError == '') {
                    
                    //alert()
                    $scope.isAdmin = true;
                    if (response.data.MetersLocations.length > 0) {
                        $scope.MetersLocations = response.data.MetersLocations;

                        //Leaflet map loading initially
                        $scope.LeaftletClusterMap($scope.MetersLocations);

                        //$scope.showPopupMap($scope.MetersLocations);
                        $scope.isLoading = false;
                    } else {
                        $scope.isLoading = false;
                        $scope.LeaftletClusterMap([]);
                        //$scope.showPopupMapBasic()
                    }

                } else if (response.data.MetersLocationsError == "Not a Adminstrator") {
                    $scope.isAdmin = false;
                }

            }

        });
    }
    //Getting meters locations onload
    

    $scope.init = function () {
        $scope.getSolarData();
        $scope.getMetersCount();
        var timeoutPromise = $timeout(function () {
            $scope.GetMetersLocations('all');

        }, 500);
    }

    //Leaflet Map functionality

    var map = L.map('projectsMap').setView([-26.164726, 28.369397], 11);

    /*L.esri.Vector.vectorBasemapLayer(basemapEnum, {
        apiKey: apiKey
      }).addTo(map);
      */

    var osm = new L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: ''
    }).addTo(map);

   // $scope.LeaftletClusterMap([]);

    var markers = L.markerClusterGroup();
    $scope.LeaftletClusterMap = function (locations) {
        markers.clearLayers();
        if (locations.length > 0) {
            for (var i = 0; i < locations.length; i++) {
                var myIcon = '../Assets/img/map-marker-2-32.png';
                if (locations[i].IS_ONLINE !== 'Y') {
                    myIcon = '../Assets/img/map-marker-red-32.png';
                }

                var greenIcon = L.icon({
                    iconUrl: myIcon,
                });

                var title = '<p><span style="font-weight: bold">AccountNo: </span>' + locations[i].AccountNo + '</p><p><span style="font-weight: bold">Meter Number: </span>' + locations[i].METER_NO + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].ADDRESS + '</p>';
                try {
                    var marker = L.marker(L.latLng(parseFloat(locations[i].LATT), parseFloat(locations[i].LONG)), { icon: greenIcon }, { title: title });
                    marker.bindPopup(title);
                    markers.addLayer(marker)
                } catch (ex) {

                }


            }
            map.addLayer(markers);

        }
        
    }

    $scope.LeaftletClusterMap($scope.MetersLocations);


    // Set style function that sets fill color property
    function style(feature) {
        return {
            fillColor: '#72bcd4',
            fillOpacity: 0.5,
            weight: 1,
            opacity: 1,
            color: '#000',
            dashArray: '2'
        };
    }
    var highlight = {
        'fillColor': 'yellow',
        'weight': 2,
        'opacity': 1
    };

    function forEachFeature(feature, layer) {

        var popupContent = `<p><b>Ward No: </b> ${feature.properties.Name} </p>`;

        //"</br>Municipality Name: "+ feature.properties.MunicName +
        //"</br>GAPA_NAPA: "+ feature.properties.MunicName +
        //"</br>PGN_TYPE: "+ feature.properties.MapCode +
        //"</br>PROVINCE: "+ feature.properties.Province +'</p>';

        //alert("here!!");

        layer.bindPopup(popupContent);

        layer.on("click", function (e) {
            theLayer.setStyle(style); //resets layer colors
            layer.setStyle(highlight);  //highlights selected.
            //alert("here!!");
        });
    }

    // Null variable that will hold layer
    var theLayer = L.geoJson(null, { onEachFeature: forEachFeature, style: style });

    $.getJSON(config.eku_Url, function (data) {
        theLayer.addData(data);
    });

    theLayer.addTo(map);

    // for Layer Control    
    var baseMaps = {
        "Open Street Map": osm
    };

    var overlayMaps = {
        "My Data": theLayer
    };

    //Add layer control
    L.control.layers(baseMaps, overlayMaps).addTo(map);


    //Leaflet map end
    
    //Leaflet map end


    //Google Cluster map functionality
    //var map;
    //$scope.showPopupMapBasic = function () {

    //    var mapOptions = {
    //        zoom: 12,
    //        center: { lat: parseFloat($scope.BasicLatitude), lng: parseFloat($scope.BasicLongitude) },
    //        //mapTypeId: 'satellite',
    //        disableDefaultUI: false,
    //        //mapTypeId: 'hybrid',
    //        //styles: [
    //        //    {
    //        //        featureType: "road",
    //        //        stylers: [
    //        //            { visibility: "off" }
    //        //        ]
    //        //    }
    //        //]

    //    };


    //    map = new google.maps.Map(document.getElementById('projectsMap'), mapOptions);

    //}

    //var marker;
    //var gm_map;
    //var markerArray = [];
    //var geocoder = new google.maps.Geocoder();
    //var infoWindow = new google.maps.InfoWindow();
    //var clusterMarkers = [];
    //var clusterMarkers1 = [];
    //var marker, i;
    ////var myIcon = '../Assets/img/map-marker-2-32.png';
    //$scope.showPopupMap = function (locations) {
    //    clusterMarkers = [];
    //    clusterMarkers1 = [];

    //    console.log(locations);
    //    console.log(locations.length);

    //    for (i = 0; i < locations.length; i++) {

    //        var cont = '<p><span style="font-weight: bold">POD Number: </span>' + locations[i].POD_CODE + '</p><p><span style="font-weight: bold">Meter Number: </span>' + locations[i].METER_NO + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].LOCATION + '</p>';

    //        clusterMarkers.push(new google.maps.Marker({
    //            position: new google.maps.LatLng(parseFloat(locations[i].LATT), parseFloat(locations[i].LONG)),
    //            map: gm_map,

    //            //title: cont
    //        })
    //        )





    //    }
    //    console.log(clusterMarkers.length);

    //    var options_googlemaps = {
    //        //minZoom: 4,
    //        zoom: 10,
    //        center: new google.maps.LatLng(-25.9612731, 28.6504824),
    //        maxZoom: 40,
    //        //mapTypeId: google.maps.MapTypeId.ROADMAP,
    //        // mapTypeId: "satellite",
    //        streetViewControl: true,
    //        styles: [
    //            {
    //                "featureType": "administrative",
    //                "elementType": "geometry",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            },
    //            {
    //                "featureType": "administrative.land_parcel",
    //                "elementType": "labels",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            },
    //            {
    //                "featureType": "poi",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            },
    //            {
    //                "featureType": "poi",
    //                "elementType": "labels.text",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            },
    //            {
    //                "featureType": "road",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            },
    //            {
    //                "featureType": "road",
    //                "elementType": "labels.icon",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            },
    //            {
    //                "featureType": "road.local",
    //                "elementType": "labels",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            },
    //            {
    //                "featureType": "transit",
    //                "stylers": [
    //                    {
    //                        "visibility": "off"
    //                    }
    //                ]
    //            }
    //        ]
    //    }


    //    gm_map = new google.maps.Map(document.getElementById('projectsMap'), options_googlemaps);
    //    gm_map.setTilt(45);
    //    var options_markerclusterer = {
    //        gridSize: 20,
    //        maxZoom: 40,
    //        zoom: 2,
    //        zoomOnClick: true,
    //        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',

    //    };



    //    for (i = 0; i < clusterMarkers.length; i++) {

    //        //var cont = '<p><span style="font-weight: bold">ERF Number: </span>' + locations[i].ERFNumber + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].Location + '</p>';
    //        var cont = '<p><span style="font-weight: bold">POD Number: </span>' + locations[i].POD_CODE + '</p><p><span style="font-weight: bold">Meter Number: </span>' + locations[i].METER_NO + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].LOCATION + '</p>';

    //        //alert('same')
    //        var marker1 = clusterMarkers[i];
    //        var pos = marker1.getPosition();
    //        //alert(pos)
    //        if (clusterMarkers[i].position.equals(pos)) {
    //            //alert('same')

    //            var myIcon = '../Assets/img/map-marker-2-32.png';
    //            if (locations[i].IS_ONLINE !== 'Y') {
    //                myIcon = '../Assets/img/map-marker-red-32.png';
    //            }


    //            var a = 360.0 / clusterMarkers.length;
    //            var newLat = pos.lat() + -.00004 * Math.cos((+a * i) / 180 * Math.PI); //x 
    //            var newLng = pos.lng() + -.00004 * Math.sin((+a * i) / 180 * Math.PI); //Y 
    //            clusterMarkers1.push(new google.maps.Marker({
    //                position: new google.maps.LatLng(newLat, newLng),
    //                map: gm_map,
    //                icon: myIcon,
    //                title: locations[i].METER_NO

    //            })
    //            )
    //        }
    //    }

    //    var markerCluster = new MarkerClusterer(gm_map, clusterMarkers1, options_markerclusterer);

    //    for (i = 0; i < clusterMarkers1.length; i++) {
    //        var marker = clusterMarkers1[i];

    //        google.maps.event.addListener(marker, 'click', (function (marker) {
    //            return function () {
    //                infoWindow.setContent(cont);
    //                infoWindow.open(gm_map, this);
    //            }
    //        })(marker));
    //    }

    //    //=====================
    //    gm_map.data.loadGeoJson('../AngularJS/Home/eku.json');
    //    //=====================

    //}

});


