﻿app.controller('MeterReadingReportsCtrl', function ($scope, $http, $filter) {
  //  alert("ok");
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "OWNER_NAME";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.tableView = true;
    $scope.imageView = false;
    $scope.orderType = "ASC";
    $scope.search = "";
    $scope.isSearch = false;

    var date = new Date();
    $scope.fromdate = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
    $scope.todate = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');

    $scope.GetMeterReadingReports = function (accountno) {
        //alert(accountno);
        debugger;
        $scope.showLoader = true;
        $scope.showTable = false;

        if (accountno != undefined && accountno != "") {
            $scope.isSearch = true;
            var url = "../Report/GetMeterReadingReports?accountNo=" + accountno

            $http.get(url)
                .then(function (response) {
                    $scope.isSearch = false;
                    console.log(response);
                    if (response.data.success === true) {
                        if (response.data.items.length > 0) {
                            $scope.items = response.data.items;
                            $scope.totalCount = response.data.totalCount;

                        } else {
                            alertify.alert("SUCCESS", "Meters Not Available", function () { });

                        }

                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else if (response.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                }, function (err) {
                    var error = err;
                    alertify.alert("Error", "Data loading problem");
                    console.log(error);
                    $scope.isSearch = false;
                });
        }
        else {
            alertify.alert("MESSAGE", "Account Number is required");
        }
    }

    $scope.GetMeterReadingHistory = function (meterno) {

        $scope.showGalleryForm();

        var url = "../Report/GetMeterReadingHistory?meterno=" + meterno + "&pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search + "&from=" + $scope.fromdate + "&to=" + $scope.todate;

        $http.get(url)
            .then(function (response) {
                console.log(response);
                if (response.data.success === true) {
                    if (response.data.items.length > 0) {
                        $scope.itemHistory = response.data.items;
                        $scope.totalCount = response.data.totalCount;
                    } else {
                        alertify.alert("SUCCESS", "Audited Meters Not Available", function () { });

                    }

                    $scope.showLoader = false;
                    $scope.showTable = true;
                } else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            }, function (err) {
                var error = err;
            });
    }


    $scope.showLoaderOnly = function () {
        $scope.showTable = false;
        $scope.showDetails = false;
        $scope.showGallery = false;
        $scope.showLoader = true;

        $scope.tableView = false;
        $scope.imageView = false;
    }

    $scope.showGalleryForm = function () {
        $scope.showTable = false;
        $scope.showDetails = false;
        $scope.showLoader = false;
        $scope.showGallery = true;

        $scope.tableView = false;
        $scope.imageView = true;
    }

    $scope.displayTableView = function () {
        //$scope.showTable = true;
        //$scope.showDetails = false;
        //$scope.showGallery = false;

        $scope.tableView = true;
        $scope.imageView = false;

    }

    $scope.fileTitle = '';

    $scope.showImagePopup = function (ID, fileName) {
        // alert(ID)

        $scope.fileTitle = fileName;

        $('#popupImage').attr('src', '../Report/ViewAttachment/?ID=' + ID);
        $('#downloadImage').attr('href', '../Report/DownloadDBAttachment/?ID=' + ID);
    }


    // Gallery
    $scope.ViewImages = function (meter) {
        //alert(meter.ID)
        $scope.showLoaderOnly();

        $http.post('../Report/GetAuditedImages', { ID: meter.MID }).then(function (response) {
            console.log('respose');
            console.log(response);
            $scope.selectedMeter = meter;
            if (response.data.success) {

                $scope.imagesList = response.data.attachments;

                $scope.showGalleryForm();

            } else if (response.data.error) {
                alertify.alert('Message', "Problem loading images. Please try after sometime.", function () {
                    $scope.showAssetsTable();
                });

            } else if (response.data.expire) {
                window.location.href = '../Home/Home';
            }
        });
    }


    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "OWNER_NAME";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.GetMeterReadingReports();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetMeterReadingReports();
    }

    $scope.orderType = "ASC";
    $scope.search = "";
    //Loading employees list on first time
    //$scope.GetMeterReadingReports();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetMeterReadingReports();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetMeterReadingReports();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});