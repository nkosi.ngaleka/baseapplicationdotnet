﻿app.controller('AuditController', function ($scope, $http, $filter) {
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.SearchModel = { MeterNO: "", UserID: "", fromdate: "", todate: "" };//Empty when page load
    // $scope.UserID = 0;
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.


    $scope.changeUser = function (UserID) {
      //  alert(UserID)
        $scope.RouteList = [];
        $http.get('../Master/GetRouteCode?UserID=' + UserID).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.RouteList = reasons.data.routeCode;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }
    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;

        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetActivityReport = function (model) {
        console.log($scope.SearchModel)
        $scope.showLoader = true;
        $scope.showTable = false;
        //  console.log($scope.fromdate)
        //alert($scope.UserID)
        //if ($scope.SearchModel.UserID == undefined || $scope.SearchModel.UserID == null) {
        //    $scope.SearchModel.UserID = "";
        //}
        //if ($scope.SearchModel.MeterNO == undefined || $scope.SearchModel.MeterNO == null) {
        //    $scope.SearchModel.MeterNO = "";
        //}
        //if ($scope.SearchModel.fromdate == undefined || $scope.SearchModel.fromdate == null) {
        //    $scope.SearchModel.fromdate = "";
        //}
        //if ($scope.SearchModel.todate == undefined || $scope.SearchModel.todate == null) {
        //    $scope.SearchModel.fromdate = "";
        //}
        //var url = "../Report/ReportAllocatedMetersAuditbyMeter?meterNo=" + $scope.SearchModel.MeterNO + '&userID =' + $scope.SearchModel.UserID + '&fromdate =' + $scope.SearchModel.fromdate + '&todate =' + model.todate;
        // var url = '../Report/ReportAllocatedMetersAuditbyAll?meterNo=1234&userId =91&fromDate =2022-05-12&toDate =';

        $http.post("../Report/ReportActivity", {
            route: $scope.SearchModel.Route,
            userId: $scope.SearchModel.UserID,
            fromDate: $scope.SearchModel.fromdate,
            toDate: $scope.SearchModel.todate,
            pageIndex: $scope.pageIndex,
            pageSize: $scope.pageSizeSelected
        })
            .then(function (res) {
                console.log(res);
                if (res.data.response.statusCode === 200) {
                    $scope.items = res.data.response.result.items;
                    $scope.totalCount = res.data.response.result.totalCount;
                    if ($scope.items.length > 0) {
                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else {
                        $scope.showLoader = false;
                        $scope.showTable = true;
                        alertify.alert("Error", "Activity Report  not available.", function () { });
                        // window.location.href = '../Home/Dashboard';                    
                    }


                } else {
                    alertify.alert("Error", res.data.response.message, function () { });
                    $scope.showLoader = false;
                    $scope.showTable = false;

                }

            }, function (err) {
                var error = err;
            });
    }

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetActivityReport($scope.SearchModel);
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetActivityReport($scope.SearchModel);
    };

});
