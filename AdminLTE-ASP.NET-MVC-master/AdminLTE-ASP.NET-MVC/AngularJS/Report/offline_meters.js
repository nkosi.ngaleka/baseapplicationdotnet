﻿app.controller('OfflineReportController', function ($scope, $http, $filter) {
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "FirstName";
    $scope.search = "";
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.SearchModel = { UserID: "", fromdate: "", todate: "" };//Empty when page load
    var date = new Date();
    $scope.SearchModel.fromdate = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
    $scope.SearchModel.todate = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');

    // $scope.UserID = 0;


    //$scope.changeUser = function (UserID) {
    //    //  alert(UserID)
    //    $scope.RouteList = [];
    //    $http.get('../Master/GetRouteCode?UserID=' + UserID).then(function (reasons) {
    //        console.log(reasons)
    //        if (reasons.data.success) {
    //            $scope.RouteList = reasons.data.routeCode;
    //        } else if (reasons.data.expire) {
    //            window.location.href = "../Account/Login";
    //        }
    //    });
    //}
    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;

        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });

    $scope.GetOfflineReport = function (model) {
      //  alert()
        console.log($scope.SearchModel)
        $scope.showLoader = true;
        $scope.showTable = false;
       
        $http.post("../Report/OfflineReport", {
            userID: $scope.SearchModel.UserID,
            fromDate: $scope.SearchModel.fromdate,
            toDate: $scope.SearchModel.todate,
            pageIndex: $scope.pageIndex,
            pageSize: $scope.pageSizeSelected,
            search: $scope.search
        })
            .then(function (res) {
                console.log(res);
                $scope.showLoader = false;
                if (res.data.success) {
                    $scope.items = res.data.items;
                    $scope.totalCount = res.data.totalCount;
                    if ($scope.items.length > 0) {
                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else {
                        $scope.showLoader = false;
                        $scope.showTable = true;
                        alertify.alert("Error", "Offline Report not available.", function () { });
                        // window.location.href = '../Home/Dashboard';                    
                    }


                } else {
                    alertify.alert("Error", res.data.error, function () { });
                    $scope.showLoader = false;
                    $scope.showTable = false;

                }

            }, function (err) {
                var error = err;
            });
    }

    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetOfflineReport($scope.SearchModel);
    }

    //Loading employees list on first time
   // $scope.GetOfflineReport($scope.SearchModel);

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetOfflineReport($scope.SearchModel);
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetOfflineReport($scope.SearchModel);
    };

});
