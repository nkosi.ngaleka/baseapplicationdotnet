﻿app.controller('UserRouteSummaryReportController', function ($scope, $http, $filter) {

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 25; // Maximum number of items per page.
    $scope.sorting = "";
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.orderType = "ASC";
    $scope.search = "FirstName";
    $scope.isFormValid = false;
    $scope.showError = false;
    $scope.$watch('SearchCriteriaForm.$valid', function (newValue) {
        $scope.isFormValid = newValue;
    });
    
    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;
            
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });

    $scope.GetFormUser = function () {
        $scope.items = [];
        $scope.GetUserSummaryReport();
    }


    $scope.GetUserSummaryReport = function () {
        $scope.showError = true;

        if ($scope.isFormValid && !$scope.errShow) {
            $scope.showLoader = true;
            $scope.showTable = false;
            $scope.showError = false;
            var url = "../Report/GetUserRouteSummary?UserID=" + $scope.UserID;
            $http.get(url)
                .then(function (response) {
                    console.log(response);
                    if (response.data.success === true) {
                        if (response.data.items.length > 0) {
                            $scope.items = response.data.items;
                            $scope.totalCount = response.data.totalCount;


                        } else {
                            alertify.alert("SUCCESS", "Meters Not Available", function () { });

                        }
                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else if (response.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                }, function (err) {
                    var error = err;
                });
        }
        
    }

   
   

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});