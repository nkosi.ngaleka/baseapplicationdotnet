﻿app.controller('SRCtrl', function ($scope, $http, $filter) {

    $scope.imgLoaderSearch = false;

    //$scope.LoadingConditions = true;

    $scope.sorting = "FirstName";
    var sortingOrder = false;
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.downloadBtn = false;
    $scope.maxSize = 5;    
    $scope.totalCount = 0; 
    $scope.pageIndex = 1;  
    $scope.pageSizeSelected = '10'; 

    //$scope.ConditionList = [{ "Condition": "Faulty Meters" }, { "Condition": "Tampered Meters" }];
    $scope.ConditionList = [];

    //Gets 
    $http.post('../Report/GetConditions').then(function (cdata) {

        console.log(cdata);

        if (cdata.data.success === true) {
            $scope.ConditionList = cdata.data.conditions;
            $scope.LoadingConditions = false;
        } else if (cdata.data.success === false) {
            $scope.LoadingConditions = false;
        }

        $scope.LoadingConditions = false;
    });

    $scope.SearchConditionWiseMeters = function () {

        $scope.NoResult = "";
     
        $scope.showLoader = true;
        $scope.showTable = false;
        $scope.downloadBtn = false;
        $scope.imgLoaderSearch = true;
     
        var url = "../Report/GetConditionWise?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search + "&condition=" + $scope.Condition;

        $http.get(url)
           .then(function (response) {

               if (response.data.success) {
                   $scope.showLoader = false;
                   $scope.imgLoaderSearch = false;
                   $scope.showLoader = false;
                   $scope.showTable = true;
                   $scope.downloadBtn = true;

                   $scope.items = response.data.success.items;
                   $scope.totalCount = response.data.success.totalCount;

               } else if (response.data.expire) {
                   window.location.href = '../Home/Dashboard';
               }
               else if (response.data.success.ErrorMessage) {
                   alert("Problem loading data.");
                   console.log(response.data.success.ErrorMessage);
               }
           }, function (err) {
               var error = err;
           });
    }

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "FirstName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.SearchConditionWiseMeters();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.SearchConditionWiseMeters();
    }

    //Loading employees list on first time
    $scope.SearchConditionWiseMeters();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.SearchConditionWiseMeters();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.SearchConditionWiseMeters();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});


app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});