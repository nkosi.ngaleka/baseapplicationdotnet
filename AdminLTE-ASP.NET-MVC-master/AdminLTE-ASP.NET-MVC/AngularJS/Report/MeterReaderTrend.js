﻿app.controller('UserRouteSummaryReportController', function ($scope, $http, $filter) {

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "";
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.orderType = "ASC";
    $scope.search = "FirstName";
    $scope.isFormValid = false;
    $scope.showError = false;
    $scope.$watch('SearchCriteriaForm.$valid', function (newValue) {
        $scope.isFormValid = newValue;
    });

    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;

        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });

    $scope.GetFormUser = function () {
        $scope.items = [];
        $scope.GetUserSummaryReport();
    }
    $scope.SearchModel = { UserID: "", fromdate: "", todate: "" };//Empty when page load
    var date = new Date();
    $scope.SearchModel.fromdate = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
    $scope.SearchModel.todate = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');


    $scope.GetUserSummaryReport = function () {
        $scope.showError = true;
        $scope.showLoader = true;
        $scope.showTable = false;
        $scope.showError = false;
        var url = "../Report/GetMeterTrend?UserID=" + $scope.SearchModel.UserID + "&FromDate=" + $scope.SearchModel.fromdate + "&ToDate=" + $scope.SearchModel.todate;
        $http.get(url)
            .then(function (res) {
                console.log(res);
                if (res.data.response.statusCode === 200) {
                    $scope.items = res.data.response.result;
                    if ($scope.items.length > 0) {
                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else {
                        $scope.showLoader = false;
                        $scope.showTable = false;
                        alertify.alert("Error", "Meter Reader Trend not available .", function () { });
                        // window.location.href = '../Home/Dashboard';                    
                    }


                } else {
                    alertify.alert("Error", res.data.response.message, function () { });
                    $scope.showLoader = false;
                    $scope.showTable = false;

                }
            }, function (err) {
                var error = err;
            });

    }

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetUserSummaryReport();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetUserSummaryReport();
    };


    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});