﻿app.controller('AMReportController', function (config,$scope, $http, $filter) {

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "MODIFIED_DATE";
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.tableView = true;
    $scope.imageView = false;
    $scope.orderType = "ASC";
    $scope.search = "";
    $scope.isFormValid = false;
    $scope.showError = false;



    $scope.$watch('SearchCriteriaForm.$valid', function (newValue) {
        // alert(newValue)
        $scope.isFormValid = newValue;
    });


    $http.post('../Master/GetCCC').then(function (reasons) {
        console.log(reasons)
        if (reasons.data.success) {
            $scope.CCCList = reasons.data.CCC;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetCycle = function (ccc) {
        $scope.CycleList = [];
        $scope.routeList = [];
        $http.post('../Master/GetCycle?CCC=' + ccc).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.CycleList = reasons.data.Cycles;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }

    $scope.getRouteCode = function (CCC, Cycle) {
        //  alert(CCC)
        $scope.routeList = [];
        var params = {
            Cycle: $scope.Cycle,
            CCC: $scope.CCC
        };
        $http.post('../Master/GetRouteCodeCycle', params).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.routeList = reasons.data.Routes;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }
    $http.post('../Master/GetReadCode').then(function (reasons) {
        if (reasons.data.success) {
            $scope.readCodeList = reasons.data.ReadCode;
            console.log($scope.readCodeList)
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetFormUser = function () {
        $scope.items = [];
        $scope.GetMeterReport();
    }
    //var date = new Date();
    //$scope.fromdate = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
    //$scope.todate = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');

    $scope.GetMeterReport = function () {
        $scope.showError = true;
        //alert($scope.isFormValid)
        $scope.SearchCriteriaFormSubmitted = true;

        if ($scope.isFormValid && !$scope.errShow) {
            $scope.showLoader = true;
            $scope.showTable = false;
            $scope.showError = false;
            var url = "../Report/GetRouteReadCodeMeters?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search + "&CCC=" + $scope.CCC + "&Cycle=" + $scope.Cycle + "&routeCode=" + $scope.Route + "&readCode=" + $scope.ReadCode;
            // alert(url)
            $http.get(url)
                .then(function (response) {
                    console.log(response);
                    if (response.data.success === true) {
                        if (response.data.items.length > 0) {
                            $scope.items = response.data.items;
                            $scope.totalCount = response.data.totalCount;


                        } else {
                            alertify.alert("SUCCESS", "Meters Not Available", function () { });

                        }
                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else if (response.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                }, function (err) {
                    var error = err;
                });
        } else {
            $scope.showLoader = false;

        }
    }


    $scope.showLoaderOnly = function () {
        $scope.showTable = false;
        $scope.showDetails = false;
        $scope.showGallery = false;
        $scope.showLoader = true;

        $scope.tableView = false;
        $scope.imageView = false;
    }

    $scope.showGalleryForm = function () {
        $scope.showTable = false;
        $scope.showDetails = false;
        $scope.showLoader = false;
        $scope.showGallery = true;

        $scope.tableView = false;
        $scope.imageView = true;
    }

    $scope.displayTableView = function () {
        $scope.showTable = true;
        $scope.showDetails = false;
        $scope.showGallery = false;

        $scope.tableView = true;
        $scope.imageView = false;
    }

    $scope.fileTitle = '';

    $scope.showImagePopup = function (ID, fileName) {
        // alert(ID)

        $scope.fileTitle = fileName;

        $('#popupImage').attr('src', '../Report/ViewAttachment/?ID=' + ID);
        $('#downloadImage').attr('href', '../Report/DownloadDBAttachment/?ID=' + ID);
    }


    // Gallery
    $scope.ViewImages = function (meter) {
        //alert(meter.ID)
        $scope.showLoaderOnly();

        $http.post('../Report/GetAuditedImages', { ID: meter.MID }).then(function (response) {
            console.log('respose');
            console.log(response);
            $scope.selectedMeter = meter;
            if (response.data.success) {

                $scope.imagesList = response.data.attachments;

                $scope.showGalleryForm();

            } else if (response.data.error) {
                alertify.alert('Message', "Problem loading images. Please try after sometime.", function () {
                    $scope.showAssetsTable();
                });

            } else if (response.data.expire) {
                window.location.href = '../Home/Home';
            }
        });
    }


    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "MODIFIED_DATE";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.GetMeterReport();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetMeterReport();
    }

    $scope.orderType = "ASC";
    $scope.search = "";
    //Loading employees list on first time
    // $scope.GetMeterReport();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetMeterReport();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetMeterReport();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }




    // showing Map and table format based on search criteria and 
    $scope.isMap = false;
    $scope.isMapBtnLoading = false;

    $scope.ShowReadCodesMap = function (route, readCode) {
        //$scope.showPopupMapBasic();
        $scope.isMap = true;
        $scope.isMapBtnLoading = true;
        //alert(route)
        $http.get('../Report/GetRouteReadCodeMetersReportMap?route=' + route + '&readCode=' + readCode).then(function (res) {
            console.log(res.data);
            //alert()
            if (res.data.response.statusCode === 200) {
                //$scope.showPopupMap(res.data.response.result);
                $scope.LeaftletClusterMap(res.data.response.result);
            } else {
                alertify.alert("Error", res.data.response.message, function () { });
                //$scope.showPopupMapBasic();
                $scope.LeaftletClusterMap();
            }
            //$scope.isMap = false;
            $scope.isMapBtnLoading = false
        }, function (err) {
            console.log(err);
            $scope.isMap = false;
            $scope.isMapBtnLoading = false;
        });

        //$scope.isMap = false;
        $scope.isMapBtnLoading = false;
    }




    /*Leaflet*/
    $scope.BasicLatitude = -26.164726;
    $scope.BasicLongitude = 28.369397;

    //Leaflet Map functionality

    var map = L.map('readingcodessMap').setView([-26.164726, 28.369397], 11);

    /*L.esri.Vector.vectorBasemapLayer(basemapEnum, {
        apiKey: apiKey
      }).addTo(map);
      */

    var osm = new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: ''
    }).addTo(map);


    var markers = L.markerClusterGroup();

    $scope.LeaftletClusterMap = function (locations) {
      markers.clearLayers();
        for (var i = 0; i < locations.length; i++) {
            var myIcon = '../Assets/img/map-marker-2-32.png';
            if (locations[i].IS_ONLINE !== 'Y') {
                myIcon = '../Assets/img/map-marker-red-32.png';
            }

            var greenIcon = L.icon({
                iconUrl: myIcon,
            });

            var title = '<p><span style="font-weight: bold">POD Number: </span>' + locations[i].POD_CODE + '</p><p><span style="font-weight: bold">Meter Number: </span>' + locations[i].METER_NO + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].ADDRESS + '</p>';
            var marker = L.marker(L.latLng(parseFloat(locations[i].P_LATITUDE), parseFloat(locations[i].P_LONGITUDE)), { icon: greenIcon }, { title: title });
            marker.bindPopup(title);
            markers.addLayer(marker)

        }

        map.addLayer(markers);
    }


    // Set style function that sets fill color property
    function style(feature) {
        return {
            fillColor: '#72bcd4',
            fillOpacity: 0.5,
            weight: 1,
            opacity: 1,
            color: '#000',
            dashArray: '2'
        };
    }
    var highlight = {
        'fillColor': 'yellow',
        'weight': 2,
        'opacity': 1
    };

    function forEachFeature(feature, layer) {

        var popupContent = `<p><b>Ward No: </b> ${feature.properties.Name} </p>`;

        //"</br>Municipality Name: "+ feature.properties.MunicName +
        //"</br>GAPA_NAPA: "+ feature.properties.MunicName +
        //"</br>PGN_TYPE: "+ feature.properties.MapCode +
        //"</br>PROVINCE: "+ feature.properties.Province +'</p>';

        //alert("here!!");

        layer.bindPopup(popupContent);

        layer.on("click", function (e) {
            theLayer.setStyle(style); //resets layer colors
            layer.setStyle(highlight);  //highlights selected.
            //alert("here!!");
        });
    }

    // Null variable that will hold layer
    var theLayer = L.geoJson(null, { onEachFeature: forEachFeature, style: style });

    $.getJSON(config.eku_Url, function (data) {
        theLayer.addData(data);
    });

    theLayer.addTo(map);

    // for Layer Control    
    var baseMaps = {
        "Open Street Map": osm
    };

    var overlayMaps = {
        "My Data": theLayer
    };

    //Add layer control
    L.control.layers(baseMaps, overlayMaps).addTo(map);


    //Leaflet map end


    /* Google map functionality*/


    //var map;
    //$scope.showPopupMapBasic = function () {

    //    var mapOptions = {
    //        zoom: 12,
    //        center: { lat: parseFloat($scope.BasicLatitude), lng: parseFloat($scope.BasicLongitude) },
    //        //mapTypeId: 'satellite',
    //        disableDefaultUI: false,
    //        //mapTypeId: 'hybrid',
    //        styles: [
    //            {
    //                featureType: "road",
    //                stylers: [
    //                    { visibility: "off" }
    //                ]
    //            }
    //        ]
    //    };

    //    map = new google.maps.Map(document.getElementById('readingcodessMap'), mapOptions);


    //}


    //var marker;
    //var gm_map;
    //var markerArray = [];
    //var geocoder = new google.maps.Geocoder();
    //var infoWindow = new google.maps.InfoWindow();
    //var clusterMarkers = [];
    //var clusterMarkers1 = [];
    //var marker, i;
    ////var myIcon = '../Assets/img/map-marker-2-32.png';
    //$scope.showPopupMap = function (locations) {
    //    clusterMarkers = [];
    //    clusterMarkers1 = [];

    //    console.log(locations);
    //    console.log(locations.length);

    //    for (i = 0; i < locations.length; i++) {

    //        var cont = '<p><span style="font-weight: bold">POD Number: </span>' + locations[i].POD_CODE + '</p><p><span style="font-weight: bold">Meter Number: </span>' + locations[i].METER_NO + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].ADDRESS + '</p>';

    //        clusterMarkers.push(new google.maps.Marker({
    //            position: new google.maps.LatLng(parseFloat(locations[i].P_LATITUDE), parseFloat(locations[i].P_LONGITUDE)),
    //            map: gm_map,

    //            //title: cont
    //        })
    //        )





    //    }
    //    console.log(clusterMarkers.length);

    //    var options_googlemaps = {
    //        //minZoom: 4,
    //        zoom: 10,
    //        center: new google.maps.LatLng(-25.9612731, 28.6504824),
    //        maxZoom: 40,
    //        //mapTypeId: google.maps.MapTypeId.ROADMAP,
    //        //mapTypeId: "satellite",
    //        streetViewControl: true
    //    }


    //    gm_map = new google.maps.Map(document.getElementById('readingcodessMap'), options_googlemaps);
    //    gm_map.setTilt(45);
    //    var options_markerclusterer = {
    //        gridSize: 20,
    //        maxZoom: 40,
    //        zoom: 2,
    //        zoomOnClick: true,
    //        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',

    //    };

    //    for (i = 0; i < clusterMarkers.length; i++) {

    //        //var cont = '<p><span style="font-weight: bold">ERF Number: </span>' + locations[i].ERFNumber + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].Location + '</p>';
    //        var cont = '<p><span style="font-weight: bold">POD Number: </span>' + locations[i].POD_CODE + '</p><p><span style="font-weight: bold">Meter Number: </span>' + locations[i].METER_NO + '</p><p><span style="font-weight: bold">Location: </span>' + locations[i].ADDRESS + '</p>';

    //        //alert('same')
    //        var marker1 = clusterMarkers[i];
    //        var pos = marker1.getPosition();
    //        //alert(pos)
    //        if (clusterMarkers[i].position.equals(pos)) {
    //            //alert('same')

    //            var myIcon = '../Assets/img/map-marker-2-32.png';
    //            if (locations[i].IS_ONLINE !== 'Y') {
    //                myIcon = '../Assets/img/map-marker-red-32.png';
    //            }


    //            var a = 360.0 / clusterMarkers.length;
    //            var newLat = pos.lat() + -.00004 * Math.cos((+a * i) / 180 * Math.PI); //x 
    //            var newLng = pos.lng() + -.00004 * Math.sin((+a * i) / 180 * Math.PI); //Y 
    //            clusterMarkers1.push(new google.maps.Marker({
    //                position: new google.maps.LatLng(newLat, newLng),
    //                map: gm_map,
    //                icon: myIcon,
    //                title: locations[i].METER_NO

    //            })
    //            )
    //        }
    //    }

    //    var markerCluster = new MarkerClusterer(gm_map, clusterMarkers1, options_markerclusterer);

    //    for (i = 0; i < clusterMarkers1.length; i++) {
    //        var marker = clusterMarkers1[i];

    //        google.maps.event.addListener(marker, 'click', (function (marker) {
    //            return function () {
    //                infoWindow.setContent(cont);
    //                infoWindow.open(gm_map, this);
    //            }
    //        })(marker));
    //    }

    //}

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});