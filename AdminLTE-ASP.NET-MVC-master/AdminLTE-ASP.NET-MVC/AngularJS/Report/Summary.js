﻿app.controller('SummaryReportController', function ($scope, $http, $filter) {

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "ROUTE_CODE";
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.orderType = "ASC";
    $scope.search = "";
    $scope.CCC = "";
    $scope.Cycle = "";
    $http.post('../Master/GetCCC').then(function (reasons) {
        console.log(reasons)
        if (reasons.data.success) {
            $scope.CCCList = reasons.data.CCC;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetCycle = function (ccc) {
        $http.post('../Master/GetCycle?CCC=' + ccc).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.CycleList = reasons.data.Cycles;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }

    var date = new Date();
    $scope.fromdate = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
    $scope.todate = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');

    $scope.GetSummaryReport = function () {
        $scope.loaderGet = true;
        $scope.showLoader = true;
        if ($scope.Cycle == undefined) {
            $scope.Cycle = "";
        }
        if ($scope.CCC == undefined) {
            $scope.CCC = "";
        }
        var url = "../Report/GetSummary?fromDate=" + $scope.fromdate + "&toDate=" + $scope.todate + "&CCC=" + $scope.CCC + "&Cycle=" + $scope.Cycle + "&pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search;
       // var url = "http://196.41.72.207:8082/MyCOEAdmin/api/Ekurhuleni/GetAllCustomerReadings?StartDate=2022-05-01&EndDate=2022-05-26";

        $http.get(url)
            .then(function (response) {
                console.log(response);
                $scope.showLoader = false;
                $scope.loaderGet = false;
                if (response.data.success === true) {
                    if (response.data.items.length > 0) {
                        $scope.items = response.data.items;
                        $scope.totalCount = response.data.totalCount;


                    } else {
                        alertify.alert("SUCCESS", "Meters Not Available", function () { });

                    }
                   
                    $scope.showTable = true;
                } else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            }, function (err) {
                var error = err;
            });
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true f or desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "ROUTE_CODE";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.GetSummaryReport();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetSummaryReport();
    }

    $scope.orderType = "ASC";
    $scope.search = "";
    //Loading employees list on first time
    //$scope.GetSummaryReport();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetSummaryReport();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetSummaryReport();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});