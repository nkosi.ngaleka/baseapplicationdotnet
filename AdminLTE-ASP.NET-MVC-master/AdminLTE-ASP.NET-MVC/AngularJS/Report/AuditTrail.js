﻿app.controller('AuditController', function ($scope, $http, $filter) {
     $scope.showLoader = false;
    $scope.showTable = false;
   // $scope.UserID = 0;
    $scope.SearchModel = { UserID: "", fromdate: "", todate: "" };//Empty when page load


    var date = new Date();
    $scope.fromdate =$filter('date')(new Date(), 'yyyy-MM-dd');
    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;

        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetAuditeTrailReport = function () {
        $scope.showLoader = true;
        $scope.showTable = false;
      //  console.log($scope.fromdate)
        //alert($scope.UserID)
        //if ($scope.UserID == undefined || $scope.UserID == null) {
        //    $scope.UserID = 0;
        //}
        var url = "../Report/ReportFieldWorkerLoginTimingsByUserId?userId=" + $scope.SearchModel.UserID + "&fromdate=" + $scope.SearchModel.fromdate + "&todate=" + $scope.SearchModel.todate ;
       
        $http.get(url)
            .then(function (res) {
                console.log(res);
                if (res.data.response.statusCode === 200) {
                    $scope.items = res.data.response.result;
                    if ($scope.items.length > 0) {
                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else {
                        $scope.showLoader = false;
                        $scope.showTable = false;
                        alertify.alert("Error", "Audit Trail not available .", function () { });
                        // window.location.href = '../Home/Dashboard';                    
                    }


                } else {
                    alertify.alert("Error", res.data.response.message, function () { });
                    $scope.showLoader = false;
                    $scope.showTable = false;

                }
            }, function (err) {
                var error = err;
            });
    }


    
});

//app.$inject = ['$scope', '$filter'];

//app.directive("customSort", function () {
//    return {
//        restrict: 'A',
//        transclude: true,
//        scope: {
//            order: '=',
//            sort: '='
//        },
//        template:
//            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
//            '    <span ng-transclude></span>' +
//            '    <i ng-class="selectedCls(order)"></i>' +
//            '</a>',
//        link: function (scope) {

//            // change sorting order
//            scope.sort_by = function (newSortingOrder) {
//                var sort = scope.sort;

//                if (sort.sortingOrder == newSortingOrder) {
//                    sort.reverse = !sort.reverse;
//                }

//                sort.sortingOrder = newSortingOrder;
//            };


//            scope.selectedCls = function (column) {
//                if (column == scope.sort.sortingOrder) {
//                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
//                }
//                else {
//                    return 'icon-sort'
//                }
//            };
//        }// end link
//    }
//});