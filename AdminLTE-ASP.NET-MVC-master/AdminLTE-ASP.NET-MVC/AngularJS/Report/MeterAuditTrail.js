﻿app.controller('AuditController', function ($scope, $http, $filter) {
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "OWNER_NAME";

    $scope.orderType = "ASC";
    $scope.search = "";

    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.SearchModel = { MeterNO: "", UserID: "", fromdate: "", todate: "", CCC : "", Cycle : "", Route : ""};//Empty when page load
    // $scope.UserID = 0;

    var date = new Date();
    $scope.SearchModel.fromdate = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
    $scope.SearchModel.todate = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');

    //$http.post('../Master/GetAllocatedAditMeters').then(function (reasons) {
    //    console.log(reasons)
    //    if (reasons.data.success) {
    //        $scope.AllocatedMetersList = reasons.data.AllocatedMeters;

    //    } else if (reasons.data.expire) {
    //        window.location.href = "../Account/Login";
    //    }
    //});
    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;

        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $http.post('../Master/GetCCC').then(function (reasons) {
        console.log(reasons)
        if (reasons.data.success) {
            $scope.CCCList = reasons.data.CCC;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetCycle = function (ccc) {
        $scope.CycleList = [];
        $scope.routeList = [];
        $http.post('../Master/GetCycle?CCC=' + ccc).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.CycleList = reasons.data.Cycles;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }

    $scope.getRouteCode = function (CCC, Cycle) {
        //  alert(CCC)
        //var url = '../Master/GetUnAllocatedList'
        $scope.routeList = [];
        var params = {
            Cycle: Cycle,
            CCC: CCC
        };
        $http.post('../Master/GetRouteCodeCycle', params).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.RouteList = reasons.data.Routes;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }
    $scope.GetMeterAuditeTrailReport = function () {
        console.log($scope.SearchModel)
        $scope.showLoader = true;
        $scope.showTable = false;
        
        $http.post("../Report/ReportAllocatedMetersAuditbyMeter", {
            pageIndex: $scope.pageIndex,
            pageSize: $scope.pageSizeSelected,
            search: $scope.search,
            meterNo: $scope.SearchModel.MeterNO,
            userId: $scope.SearchModel.UserID,
            CCC: $scope.SearchModel.CCC,
            Cycle: $scope.SearchModel.Cycle,
            Route: $scope.SearchModel.Route,
            fromDate: $scope.SearchModel.fromdate,
            toDate: $scope.SearchModel.todate
        })
            .then(function (res) {
                console.log(res);
                if (res.data.response.statusCode === 200) {
                    $scope.items = res.data.response.result.items;
                    $scope.totalCount = res.data.response.result.totalCount;
                    if ($scope.items.length > 0) {
                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else {
                        $scope.showLoader = false;
                        $scope.showTable = false;
                        alertify.alert("Success", "Audit Trail meters not available.", function () { });
                        // window.location.href = '../Home/Dashboard';                    
                    }
                   
                    
                } else {
                    alertify.alert("Error", res.data.response.message, function () { });
                    $scope.showLoader = false;
                    $scope.showTable = false;

                }
                
            }, function (err) {
                var error = err;
            });
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "OWNER_NAME";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.GetMeterAuditeTrailReport();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetMeterAuditeTrailReport();
    }

    $scope.orderType = "ASC";
    $scope.search = "";
    //Loading employees list on first time
   // $scope.GetMeterAuditeTrailReport();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetMeterAuditeTrailReport();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetMeterAuditeTrailReport();
    };

});
