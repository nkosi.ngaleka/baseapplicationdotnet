﻿app.factory('Excel', function ($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return {
        tableToExcel: function (tableId, worksheetName) {
            
            var table = $(tableId),
                ctx = { worksheet: worksheetName, table: table.html() },
                href = uri + base64(format(template, ctx));
            return href;
        }
    };
})

app.controller('DepreciationCtrl', function ($scope, $http, Excel, $timeout) {
    
    
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        
        $scope.exportHref = Excel.tableToExcel(tableId, 'DepreciationTable');
        //location.href = $scope.exportHref;
        $timeout(function () {
            //location.href = $scope.exportHref;
            var link = document.createElement('a');
            link.download = "export";
            link.href = $scope.exportHref;
            link.click();
        }, 100); // trigger download
    }

    $http.post('../Depreciation/getFinancialPeriods').then(function (response) {
     
        if (response.data.success) {
            $scope.months = response.data.success;

        } else if (response.data.expire) {
            window.location.href = '../Home/Dashboard';
        }

    });
    //$scope.months = [{ id: 1, name: "January" },
	//				 { id: 2, name: "February" },
	//				 { id: 3, name: "March" },
	//				 { id: 4, name: "April" },
	//				 { id: 5, name: "May" },
	//				 { id: 6, name: "June" },
	//				 { id: 7, name: "July" },
	//				 { id: 8, name: "August" },
	//				 { id: 9, name: "September" },
	//				 { id: 10, name: "October" },
	//				 { id: 11, name: "November" },
	//				 { id: 12, name: "December" }
    //];

    $scope.years = [];
    var d = new Date();
    for (var i = (d.getFullYear()) ; i > (d.getFullYear() - 5) ; i--) {
        $scope.years.push({ key: (i) + ' - ' + (i + 1).toString().substr(2, 2), value: i + 1 });
    }

    $scope.dTable = false;
    $scope.dPrint = false;
    $scope.dForm = true;
    $scope.dButton = false;
    $scope.employees1 = null;

    //alert('ok')
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.totalCount = 0;
    
    //Sorting 

   
    $scope.imgLoader_Save = false;
    $scope.Barcode = "NO";
    $scope.Month = 0;
    $scope.Year = 0;
    //$scope.showMsgs = true;
    $scope.searchDepreciationAssets = function (Barcode,Month,Year) {
        if (Barcode != undefined || Month !=undefined || Year != undefined) {
            
            $scope.showMsgs = false;

            $scope.imgLoader_Save = true;

            if (Barcode == undefined || Barcode == "")
                $scope.Barcode = "NO";
            else
                $scope.Barcode = Barcode;

            if (Month == undefined || Month == "")
                $scope.Month = 0;
            else
                $scope.Month = Month;

            if (Year == undefined || Year == "")
                $scope.Year = 0;
            else
                $scope.Year = Year;
            //alert($scope.DepreciationData.AssetID)
            //alert($scope.DepreciationData.Month)
            //alert($scope.DepreciationData.Year)
            //alert($scope.Barcode)
            //alert($scope.Month)
            //alert($scope.Year)

            $scope.pageIndex = 1;

            $http.get("../Depreciation/searchDepreciationAssets?pageIndex=" + $scope.pageIndex
                + "&pageSize=" + $scope.pageSizeSelected
                + "&sorting=" + $scope.sorting
                + "&search=" + $scope.search
                + "&Barcode=" + $scope.Barcode
                + "&Month=" + $scope.Month
                + "&Year=" + $scope.Year).then(
                       function (response) {
                           //console.log('Pagination')
                           //console.log(response.data.success) 
                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           } else {
                               $scope.employees = response.data.success.employees;
                               //$scope.employees1 = response.data.success.employees1;
                               $scope.totalCount = response.data.success.totalCount;
                               $scope.showLoader = false;
                               $scope.showTable = true;
                               $scope.dButton = true;
                               $scope.dTable = true;
                               $scope.imgLoader_Save = false;
                           }
                       },
                       function (err) {
                           var error = err;
                       });
            
        } else {
            $scope.showMsgs = true;
        }
    }



    $scope.getDepreciationAssets = function (Barcode,Month,Year) {
        if (Barcode != undefined || Month !=undefined || Year != undefined) {
            
            $scope.showMsgs = false;

            $scope.imgLoader_Save = true;

            if (Barcode == undefined || Barcode == "")
                $scope.Barcode = "NO";
            else
                $scope.Barcode = Barcode;

            if (Month == undefined || Month == "")
                $scope.Month = 0;
            else
                $scope.Month = Month;

            if (Year == undefined || Year == "")
                $scope.Year = 0;
            else
                $scope.Year = Year;
            //alert($scope.DepreciationData.AssetID)
            //alert($scope.DepreciationData.Month)
            //alert($scope.DepreciationData.Year)
            //alert($scope.Barcode)
            //alert($scope.Month)
            //alert($scope.Year)

            $http.get("../Depreciation/searchDepreciationAssets?pageIndex=" + $scope.pageIndex
                + "&pageSize=" + $scope.pageSizeSelected
                + "&sorting=" + $scope.sorting
                + "&search=" + $scope.search
                + "&Barcode=" + $scope.Barcode
                + "&Month=" + $scope.Month
                + "&Year=" + $scope.Year).then(
                       function (response) {
                           //console.log('Pagination')
                           //console.log(response.data.success)

                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           } else {

                               $scope.employees = response.data.success.employees;
                               //$scope.employees1 = response.data.success.employees1;
                               $scope.totalCount = response.data.success.totalCount;
                               $scope.showLoader = false;
                               $scope.showTable = true;
                               $scope.dButton = true;
                               $scope.dTable = true;
                               $scope.imgLoader_Save = false;
                           }
                       },
                       function (err) {
                           var error = err;
                       });
            
        } else {
            $scope.showMsgs = true;
        }
    }

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "AssetID";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.searchDepreciationAssets($scope.Barcode,$scope.Month,$scope.Year);
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.searchDepreciationAssets($scope.Barcode, $scope.Month, $scope.Year);
    }
    
    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getDepreciationAssets($scope.Barcode, $scope.Month, $scope.Year);
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.searchDepreciationAssets($scope.search,$scope.Barcode, $scope.Month, $scope.Year);
    };
    

    $scope.printDepreciationAsset = function () {

        $http.get("../Depreciation/ExportToExcel?Barcode=" + $scope.Barcode
                + "&Month=" + $scope.Month
                + "&Year=" + $scope.Year).then(
                       function (response) {
                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           }
                           //console.log('Pagination')
                           //console.log(response.data.success)
                           
                       },
                       function (err) {
                           var error = err;
                       });
    }
    $scope.printPreviewDepreciationAsset = function () {
        $scope.dTable = false;
        $scope.dForm = false;
        $scope.dPrint = true;
        

            $scope.date = new Date();
    }
    $scope.goBackDepreciationAssetTable = function () {

        $scope.dTable = true;
        $scope.dForm = true;
        $scope.dPrint = false;
    }

    $scope.Depreciationexport = function () {
        //alert('hi')
        html2canvas(document.getElementById('exportthis'), {

            onrendered: function (canvas) {

                var data = canvas.toDataURL();

                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                $timeout(function () {
                    pdfMake.createPdf(docDefinition).download("Asset_Depreciation_List.pdf");
                }, 100);
            }
        });

    }
});