﻿//Approve Review
app.controller('ApproveAssetReviewController', function ($scope, $http) {

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.imgLoader_Save = false;
    $scope.imgLoader_Save1 = false;
    $scope.getList = function () {
        $http.get("../Asset/getApprovalPendingReviewAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(

            function (response) {

                console.log(response)

                if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                } else {
                    $scope.assets = response.data.success.items;
                    $scope.totalCount = response.data.success.totalCount;
                    $scope.showLoader = false;
                    $scope.showTable = true;
                }
            },
            function (err) {
                var error = err;
            });

    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "AssetName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getList();
    }
    //Loading employees list on first time
    $scope.getList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getList();
    };

    //Modal popup open event
    //Gets selected disposed asset
    $scope.showPopup = function (ID)
    {
        $scope.AssetID = ID;
        $http.post('../Asset/getSelectedApprovalReviewAsset', { ReviewID: ID }).then(function (assetsdata) {

            if (assetsdata.data.success) {
                $scope.AssetDetail = assetsdata.data.success[0];
                console.log($scope.AssetDetail)
                $scope.LoadReviewAssetAttachmentsList(ID);

                //$('.AssetDescription').trigger('click');
            } else if (assetsdata.data.error) {
                alertify.alert('Error', assetsdata.data.error, function () {
                });
            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    //Gets attachments of disposed asset
    $scope.LoadReviewAssetAttachmentsList = function (ReviewID) {
        $http.post('../Asset/getReviewAssetAttachments', { ReviewID: ReviewID }).then(function (attachments) {

            if (attachments.data.success) {
                $scope.AttachmentList = attachments.data.success;
                //console.log($scope.AttachmentList)

            } else if (attachments.data.error) {
                alertify.alert('Error', attachments.data.error, function () {
                });
            } else if (attachments.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    //Set Approve/Reject to New asset
    $scope.ApproveRejectTab = true;


    //$scope.InvoiceAttached = 'Yes';
    //$scope.CorrectPurchasePriceCaptured = 'Yes';
    //$scope.CorrectSupplierSelected = 'Yes';
    //$scope.CorrectVoteCaptured = 'Yes';

    $scope.NewApproveReject = function (id, status, Commentary, UserPIN) {
        console.log(id);
        console.log(status);
        console.log(Commentary);
        console.log(UserPIN);
        
        if (Commentary != undefined && UserPIN != undefined) {
            
            if (status == "Approved") {
                $scope.imgLoader_Save = true;
                $scope.imgLoader_Save1 = false;
            }
            else {
                $scope.imgLoader_Save1 = true;
                $scope.imgLoader_Save = false;
            }
            
            $scope.rejectMsg = false;

            var parameters = {
                ReviewID: id,
                Commentary: Commentary,
                Status: status,
                UserPIN: UserPIN
            }

            $http.get('../Asset/NewApproveRejectReviewAsset', { params: parameters }).then(function (res) {
                $scope.imgLoader_Save = false;
                $scope.imgLoader_Save1 = false;
                if (res.data.success) {

                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Home/ApproveRejectAssetReview';
                    });
                } else if (res.data.error) {
                    alertify.alert('Error', res.data.error, function () {
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            });

        } else {
            $scope.rejectMsg = true;
        }
    }

});
