﻿app.controller('GenerateDepreciationCtrl', function ($scope, $http) {
    $scope.YearDisable = true;

    $http.post('../Depreciation/getFinancialPeriods').then(function (response) {

        if (response.data.success) {
            $scope.months = response.data.success;
        } else if (response.data.expire) {
            window.location.href = '../Home/Dashboard';
        }

    });

       //$scope.months = [
       //              { id: 1, name: "January" },
	   // 			 { id: 2, name: "February" },
	   // 			 { id: 3, name: "March" },
	   // 			 { id: 4, name: "April" },
	   // 			 { id: 5, name: "May" },
	   // 			 { id: 6, name: "June" },
	   // 			 { id: 7, name: "July" },
	   // 			 { id: 8, name: "August" },
	   // 			 { id: 9, name: "September" },
	   // 			 { id: 10, name: "October" },
	   // 			 { id: 11, name: "November" },
	   // 			 { id: 12, name: "December" }
       //];

       $scope.MonthChange = function (value) {
           if (value == undefined) {
               $scope.YearDisable = true;

           } else {
               $scope.YearDisable = false;
               $scope.years = [];
               var d = new Date();
               for (var i = (d.getFullYear()) ; i > (d.getFullYear() - 5) ; i--) {
                   $scope.years.push({ key: (i) + ' - ' + (i + 1).toString().substr(2,2), value: i+1 });
               }
           }
       }
    
    $scope.depreciationTable = false;
    //alert('ok')
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.totalCount = 0;
    $scope.imgLoader_Save = false;
    
    $scope.Month = 0;
    $scope.Year = 0;
    //$scope.showMsgs = true;
    $scope.searchDepreciationAssets = function (month, year) {
        if (month != undefined) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;
            

            if (month == undefined || month == "")
                $scope.Month = 0;
            else
                $scope.Month = month;
            
            if (year == undefined || year == "")
                $scope.Year = 0;
            else
                $scope.Year = year;
            
            $scope.pageIndex = 1;
            $http.get("../Depreciation/generateDepreciationAssets?pageIndex=" + $scope.pageIndex
                + "&pageSize=" + $scope.pageSizeSelected
                + "&sorting=" + $scope.sorting
                + "&search=" + $scope.search
                + "&Month=" + $scope.Month
                + "&Year=" + $scope.Year
                + "&Generate=true").then(
                       function (response) {
                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           } else {
                               $scope.items = response.data.success.employees;
                               $scope.totalCount = response.data.success.totalCount;
                               $scope.showLoader = false;
                               $scope.showTable = true;
                               $scope.imgLoader_Save = false;
                               $scope.depreciationTable = true;
                           }
                       },
                       function (err) {
                           var error = err;
                       });
            
        } else {
            $scope.showMsgs = true;
        }
    }

    $scope.getDepreciationAssets = function (month, year) {
        if (month != undefined) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;


            if (month == undefined || month == "")
                $scope.Month = 0;
            else
                $scope.Month = month;

            if (year == undefined || year == "")
                $scope.Year = 0;
            else
                $scope.Year = year;


            $http.get("../Depreciation/generateDepreciationAssets?pageIndex=" + $scope.pageIndex
                + "&pageSize=" + $scope.pageSizeSelected
                + "&sorting=" + $scope.sorting
                + "&search=" + $scope.search
                + "&Month=" + $scope.Month
                + "&Year=" + $scope.Year
                + "&Generate=false").then(
                       function (response) {
                          
                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           } else {
                               $scope.employees = response.data.success.employees;
                               $scope.totalCount = response.data.success.totalCount;
                               $scope.showLoader = false;
                               $scope.showTable = true;
                               $scope.imgLoader_Save = false;
                               $scope.depreciationTable = true;
                           }
                       },
                       function (err) {
                           var error = err;
                       });

        } else {
            $scope.showMsgs = true;
        }
    }

    $scope.reverse = false;
    $scope.column = "AssetID";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.searchDepreciationAssets($scope.Month, $scope.Year);
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.searchDepreciationAssets($scope.Month, $scope.Year);
    }
    ////Loading employees list on first time
    //$scope.getEmployeeList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getDepreciationAssets($scope.Month, $scope.Year);
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.searchDepreciationAssets($scope.Month, $scope.Year);
    };

});


