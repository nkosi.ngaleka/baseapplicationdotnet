﻿//Approve New Asset
app.controller('DisposeAssetCtrl', function ($scope, $http) {

    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.imgLoader_Save = false;
    $scope.imgLoader_Save1 = false;
    $scope.getVerifyNewAssetsList = function () {
        $http.get("../Asset/GetApprovalPendingNewAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search)
            .then(
                       function (response) {

                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           } else {
                               $scope.items = response.data.success.employees;
                               $scope.totalCount = response.data.success.totalCount;
                               $scope.showLoader = false;
                               $scope.showTable = true;
                           }
                       },
                       function (err) {
                           var error = err;
                       });

    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "AssetName";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getVerifyNewAssetsList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getVerifyNewAssetsList();
    }
    //Loading employees list on first time
    $scope.getVerifyNewAssetsList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getVerifyNewAssetsList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getVerifyNewAssetsList();
    };



    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    //Modal popup open event
    //Gets selected disposed asset
    $scope.showPopup = function (ID)
    {
        $scope.AssetID = ID;
        $http.post('../Asset/GetApprovalPendingNewAsset', { AssetID: ID }).then(function (assetsdata) {

            if (assetsdata.data.success) {
                $scope.AssetDetail = assetsdata.data.success[0];
                //console.log($scope.AssetDetail)
                $scope.LoadDisposedAssetAttachmentsList(ID);

                //$('.AssetDescription').trigger('click');

                var a = [], b = [], c = [];

                a = assetsdata.data.InfraData.TextPlusDate;
                b = assetsdata.data.InfraData.Dropdown;
                c = assetsdata.data.InfraData.FileUpload;

                $scope.InfraDataTextPlusDate = assetsdata.data.InfraData.TextPlusDate;
                $scope.InfraDataDropdown = assetsdata.data.InfraData.Dropdown;
                $scope.InfraDataFileUpload = assetsdata.data.InfraData.FileUpload;

                if (a.length == 0 && a.length == 0 && a.length == 0) {
                    $scope.NoInfraData = true;
                } else {
                    $scope.NoInfraData = false;
                }


            } else if (assetsdata.data.error) {
                alertify.alert('Error', assetsdata.data.error, function () {
                });
            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    //Gets attachments of disposed asset
    $scope.LoadDisposedAssetAttachmentsList = function (AssetID) {
        $http.post('../Asset/getAttachments', { AssetID: AssetID }).then(function (attachments) {

            if (attachments.data.success) {
                $scope.AttachmentList = attachments.data.success;
                //console.log($scope.AttachmentList)

            } else if (attachments.data.error) {
                alertify.alert('Error', attachments.data.error, function () {
                });
            } else if (attachments.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    //Set Approve/Reject to New asset
    $scope.ApproveRejectTab = true;


    $scope.InvoiceAttached = 'Yes';
    $scope.CorrectPurchasePriceCaptured = 'Yes';
    $scope.CorrectSupplierSelected = 'Yes';
    $scope.CorrectVoteCaptured = 'Yes';


    $scope.NewApproveReject = function (id, status, Commentary, UserPIN) {
        
        if (Commentary != undefined && UserPIN != undefined) {
            
            if (status == "Approve") {
                $scope.imgLoader_Save = true;
                $scope.imgLoader_Save1 = false;
            }
            else {
                $scope.imgLoader_Save1 = true;
                $scope.imgLoader_Save = false;
            }
            
            $scope.rejectMsg = false;

            var parameters = {
                AssetID: $scope.AssetID,
                Commentary: Commentary,
                Status: status,
                UserPIN: UserPIN,
                InvoiceAttached: $scope.InvoiceAttached,
                CorrectPurchasePriceCaptured: $scope.CorrectPurchasePriceCaptured,
                CorrectSupplierSelected: $scope.CorrectSupplierSelected,
                CorrectVoteCaptured: $scope.CorrectVoteCaptured,
                InvoiceAttachedCommentary: $scope.InvoiceAttached == 'No' ? $scope.InvoiceAttachedCommentary : null,
                CorrectPurchasePriceCapturedCommentary: $scope.CorrectPurchasePriceCaptured == 'No' ? $scope.CorrectPurchasePriceCapturedCommentary : null,
                CorrectSupplierSelectedCommentary: $scope.CorrectSupplierSelected == 'No' ? $scope.CorrectSupplierSelectedCommentary : null,
                CorrectVoteCapturedCommentary: $scope.CorrectVoteCaptured == 'No' ? $scope.CorrectVoteCapturedCommentary : null

            }

            $http.get('../Asset/NewApproveRejectAsset', { params: parameters }).then(function (res) {
                $scope.imgLoader_Save = false;
                $scope.imgLoader_Save1 = false;
                if (res.data.success) {

                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Home/ApproveRejectAssets';
                    });
                } else if (res.data.error) {
                    alertify.alert('Error', res.data.error, function () {
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            });

        } else {
            $scope.rejectMsg = true;
        }
    }

});
