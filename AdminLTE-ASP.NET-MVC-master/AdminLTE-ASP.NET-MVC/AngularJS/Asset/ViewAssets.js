﻿app.controller('AssetCtrl', function ($scope, $http) {


    //Reading query string to decide add/edit
    function getUrlParameter(param, dummyPath) {

        var sPageURL = dummyPath || window.location.search.substring(1),
            sURLVariables = sPageURL.split(/[&||?]/),
            res;

        for (var i = 0; i < sURLVariables.length; i += 1) {
            var paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');

            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }

        return res;
    }

    $scope.showTable = true;
    $scope.showDetails = false;
    $scope.showGallery = false;
    $scope.showLoader = false;

    $scope.filtered = false;
    $scope.filterConditionColor = false;

    $scope.showAssetsTable = function () {
        $scope.showTable = true;
        $scope.showDetails = false;
        $scope.showGallery = false;
    }

    $scope.showLoaderOnly = function () {
        $scope.showTable = false;
        $scope.showDetails = false;
        $scope.showGallery = false;
        $scope.showLoader = true;
    }

    $scope.showGalleryForm = function () {
        $scope.showTable = false;
        $scope.showDetails = false;
        $scope.showLoader = false;
        $scope.showGallery = true;
    }

    $scope.showAssetDetails = function () {
        $scope.showTable = false;
        $scope.showDetails = true;
    }

    $scope.AssetDetails = {};

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.totalCount = 0;

    $scope.getViewAssetsList = function () {

        var conditionID = getUrlParameter('c');
        $scope.filterCondition = decodeURI(getUrlParameter('cn'));
        $scope.filterConditionColor = decodeURI(getUrlParameter('cc'));
        console.log(conditionID);
        var url = '';

        if (conditionID !== undefined) {
            url = "../Asset/GetAllAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search + "&conditionID=" + conditionID;
            $scope.filtered = true;
        } else {
            url = "../Asset/GetAllAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search;
            $scope.filtered = false;
        }

        $http.get(url)
            .then(
            function (response) {
                console.log(response)
                if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                } else {
                    console.log(response)
                    $scope.items = response.data.success.employees;
                    $scope.totalCount = response.data.success.totalCount;
                    $scope.showLoader = false;
                    $scope.showTable = true;
                }
            },
            function (err) {
                var error = err;
            });
    }

    //$scope.PendingAssetCount = 0;

    //$http.post('../Asset/getPendingAssetCount').then(function (response) {
    //    if (response.data.success) {
    //        $scope.PendingAssetsCount = response.data.success;
    //    } else if (response.data.expire) {
    //        window.location.href = '../Home/Dashboard';
    //    }
    //});

    //Edit Asset
    $scope.EditAsset = function (EncAssetID) {
        window.location.href = "../Home/CreateAsset?editAssetID=" + EncAssetID;
    }

    // Gallery
    $scope.ViewImages = function (EncAssetID) {

        $scope.showLoaderOnly();

        $http.post('../Asset/GetFullAssetDetails', { EncAssetID: EncAssetID }).then(function (response) {
            console.log('respose');
            console.log(response);

            if (response.data.success) {

                $scope.AssetDetail = response.data.AssetData[0];
                $scope.LastReviewDetails = response.data.LastReviewDetails;

                // loading infrastructure data
                if (response.data.InfraData.success) {
                    var a = [], b = [], c = [];

                    a = response.data.InfraData.TextPlusDate;
                    b = response.data.InfraData.Dropdown;
                    c = response.data.InfraData.FileUpload;

                    $scope.InfraDataTextPlusDate = response.data.InfraData.TextPlusDate;
                    $scope.InfraDataDropdown = response.data.InfraData.Dropdown;
                    $scope.InfraDataFileUpload = $scope.ArrayGroupByKey(response.data.InfraData.FileUpload);

                    if (a.length == 0 && a.length == 0 && a.length == 0) {
                        $scope.NoInfraData = true;
                    } else {
                        $scope.NoInfraData = false;
                    }
                }
                else {
                    $scope.InfraDataTextPlusDate = [];
                    $scope.InfraDataDropdown = [];
                    $scope.InfraDataFileUpload = [];

                    $scope.InfraError = true;
                    $scope.InfraErrorMessage = "Problem showing infrastructure data.";
                }

                // loading attachments data
                $scope.AssetAttachmentsList = response.data.AssetAttachments;
                $scope.AssetReviewAttachmentsList = response.data.AssetReviewAttachments;

                $scope.showGalleryForm();

            } else if (response.data.error) {
                alertify.alert('Message', "Problem loading images. Please try after sometime.", function () {
                    $scope.showAssetsTable();
                });

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
        });
    }


    $scope.fileTitle = '';

    $scope.showAssetImagePopup = function (encId, fileName) {

        $scope.fileTitle = fileName;

        $('#popupImage').attr('src', '../Asset/ViewAssetAttachment/?ID=' + encId);
    }


    $scope.showReviewImagePopup = function (encId, fileName) {

        $scope.fileTitle = fileName;

        $('#popupImage').attr('src', '../Asset/ViewReviewAttachment/?ID=' + encId);
    }


    //View Asset
    $scope.ViewAsset = function (EncAssetID) {

        $scope.AssetDetails = {};

        $scope.AssetDetails = {};
        $scope.AssetAttachmentsList = [];
        $scope.AssetReviewAttachmentsList = [];

        $scope.showAssetDetails();

        $http.post('../Asset/GetFullAssetDetails', { EncAssetID: EncAssetID }).then(function (response) {
            console.log('respose');
            console.log(response);

            if (response.data.success) {

                $scope.AssetDetail = response.data.AssetData[0];
                $scope.LastReviewDetails = response.data.LastReviewDetails;

                // loading infrastructure data
                if (response.data.InfraData.success) {
                    var a = [], b = [], c = [];

                    a = response.data.InfraData.TextPlusDate;
                    b = response.data.InfraData.Dropdown;
                    c = response.data.InfraData.FileUpload;

                    $scope.InfraDataTextPlusDate = response.data.InfraData.TextPlusDate;
                    $scope.InfraDataDropdown = response.data.InfraData.Dropdown;
                    $scope.InfraDataFileUpload = $scope.ArrayGroupByKey(response.data.InfraData.FileUpload);

                    if (a.length == 0 && a.length == 0 && a.length == 0) {
                        $scope.NoInfraData = true;
                    } else {
                        $scope.NoInfraData = false;
                    }
                }
                else {
                    $scope.InfraDataTextPlusDate = [];
                    $scope.InfraDataDropdown = [];
                    $scope.InfraDataFileUpload = [];

                    $scope.InfraError = true;
                    $scope.InfraErrorMessage = "Problem showing infrastructure data.";
                }

                // loading attachments data
                $scope.AssetAttachmentsList = response.data.AssetAttachments;
                $scope.AssetReviewAttachmentsList = response.data.AssetReviewAttachments;

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
        });
    }


    $scope.ArrayGroupByKey = function (items) {

        var groups = items.reduce(function (obj, item) {
            obj[item.Key] = obj[item.Key] || [];
            obj[item.Key].push(item.Value);
            return obj;
        }, {});
        var myArray = Object.keys(groups).map(function (key) {
            return { Key: key, Value: groups[key] };
        });
        console.log(myArray);
        return myArray;
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "AssetName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getViewAssetsList();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getViewAssetsList();
    }

    //Loading employees list on first time
    $scope.getViewAssetsList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getViewAssetsList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getViewAssetsList();
    };
});


