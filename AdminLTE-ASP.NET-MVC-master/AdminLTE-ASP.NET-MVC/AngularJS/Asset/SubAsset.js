﻿    app.controller('AssetCtrl', function ($scope, $http) {
        $scope.showTable = true;
        $scope.showDetails = false;

        $scope.showAssetsTable = function () {
            $scope.showTable = true;
            $scope.showDetails = false;
        }

        $scope.showAssetDetails = function () {
            $scope.showTable = false;
            $scope.showDetails = true;
        }

        console.log($scope.AssetAttachmentsList)

    $scope.AssetDetails = {};

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.totalCount = 0;
    $scope.getViewAssetsList = function () {
        $http.get("../Asset/GetActiveAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&search=" + $scope.search).then(
                       function (response) {
                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           } else {
                               console.log(response)
                               $scope.items = response.data.success.employees;
                               $scope.totalCount = response.data.success.totalCount;
                               $scope.showLoader = false;
                               $scope.showTable = true;
                           }
                       },
                       function (err) {
                           var error = err;
                       });
    }

    $scope.PendingAssetCount = 0;

    $http.post('../Asset/getPendingAssetCount').then(function (response) {
        if (response.data.success) {
            $scope.PendingAssetsCount = response.data.success;
        } else if (response.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
    });

    //Edit Asset
    $scope.CreateSubAsset = function (EncAssetID) {
        window.location.href = "../Home/CreateAsset?ParentAssetID=" + EncAssetID;
    }

    $scope.ViewImages = function (EncAssetID) {
        $http.post('../Asset/GetFullAssetDetails', { EncAssetID: EncAssetID }).then(function (response) {
            console.log('respose');
            console.log(response);

            if (response.data.success) {

                $scope.AssetDetail = response.data.AssetData[0];
                
                $scope.LastReviewDetails = response.data.LastReviewDetails;

                // loading infrastructure data
                if (response.data.InfraData.success) {
                    var a = [], b = [], c = [];

                    a = response.data.InfraData.TextPlusDate;
                    b = response.data.InfraData.Dropdown;
                    c = response.data.InfraData.FileUpload;

                    $scope.InfraDataTextPlusDate = response.data.InfraData.TextPlusDate;
                    $scope.InfraDataDropdown = response.data.InfraData.Dropdown;
                    $scope.InfraDataFileUpload = $scope.ArrayGroupByKey(response.data.InfraData.FileUpload);

                    if (a.length == 0 && a.length == 0 && a.length == 0) {
                        $scope.NoInfraData = true;
                    } else {
                        $scope.NoInfraData = false;
                    }
                }
                else {
                    $scope.InfraDataTextPlusDate = [];
                    $scope.InfraDataDropdown = [];
                    $scope.InfraDataFileUpload = [];

                    $scope.InfraError = true;
                    $scope.InfraErrorMessage = "Problem showing infrastructure data.";
                }

                // loading attachments data
                $scope.AssetAttachmentsList = response.data.AssetAttachments;
                $scope.AssetReviewAttachmentsList = response.data.AssetReviewAttachments;

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
        });
    }

   
    //View Asset
    $scope.ViewAsset = function (EncAssetID) {

        $scope.AssetDetails = {};

        $scope.AssetDetails = {};
        $scope.AssetAttachmentsList = [];
        $scope.AssetReviewAttachmentsList = [];

        $scope.showAssetDetails();

        $http.post('../Asset/GetFullAssetDetails', { EncAssetID: EncAssetID }).then(function (response) {
           

            if (response.data.success) {

                $scope.AssetDetail = response.data.AssetData[0];
                $scope.InsuranceDetails = response.data.InsuranceData;
                console.log("Insurance");
                console.log($scope.InsuranceDetails);
                $scope.WarrantyDetails = response.data.WarrantyData;
                $scope.MaintenanceDetails = response.data.MaintenanceData;
                $scope.ServiceDetails = response.data.ServiceData;
                $scope.SafetyNotesDetails = response.data.SafetyNotesData;
                $scope.LastReviewDetails = response.data.LastReviewDetails;
              
                // loading infrastructure data
                if (response.data.InfraData.success) {
                    var a = [], b = [], c = [];

                    a = response.data.InfraData.TextPlusDate;
                    b = response.data.InfraData.Dropdown;
                    c = response.data.InfraData.FileUpload;

                    $scope.InfraDataTextPlusDate = response.data.InfraData.TextPlusDate;
                    $scope.InfraDataDropdown = response.data.InfraData.Dropdown;
                    $scope.InfraDataFileUpload = $scope.ArrayGroupByKey(response.data.InfraData.FileUpload);
                  
                    if (a.length == 0 && a.length == 0 && a.length == 0) {
                        $scope.NoInfraData = true;
                    } else {
                        $scope.NoInfraData = false;
                    }
                }
                else {
                    $scope.InfraDataTextPlusDate = [];
                    $scope.InfraDataDropdown = [];
                    $scope.InfraDataFileUpload = [];

                    $scope.InfraError = true;
                    $scope.InfraErrorMessage = "Problem showing infrastructure data.";
                }

                // loading attachments data
                $scope.AssetAttachmentsList = response.data.AssetAttachments;
                $scope.AssetReviewAttachmentsList = response.data.AssetReviewAttachments;

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
        });
    }

    $scope.InsuranceShow = false;

    $scope.WarrantyShow = false;
    $scope.MaintenanceShow = false;
    $scope.ServiceShow = false;
    $scope.SafetyNotesShow = false;

    $scope.InsuranceCheck = function (insuranceval) {
        $scope.Insurance = insuranceval;
        var x = insuranceval;
        if (x == 'Y') {
            $scope.InsuranceShow = true;
        }
        else if (x == 'N') {
            $scope.InsuranceShow = false;
        }

    }
    $scope.WarrantyCheck = function (warrantyval) {
        $scope.Warranty = warrantyval;
        var x = warrantyval;
        if (x == 'Y') {
            $scope.WarrantyShow = true;
        }
        else if (x == 'N') {
            $scope.WarrantyShow = false;
        }

    }
    $scope.MaintenanceCheck = function (maintainval) {
        $scope.Maintenance = maintainval;
        var x = maintainval;
        if (x == 'Y') {
            $scope.MaintenanceShow = true;
        }
        else if (x == 'N') {
            $scope.MaintenanceShow = false;
        }

    }

    $scope.ServiceCheck = function (serviceval) {
        $scope.Service = serviceval;

        var x = serviceval;
        if (x == 'Y') {
            $scope.ServiceShow = true;
        }
        else if (x == 'N') {
            $scope.ServiceShow = false;
        }

    }

    $scope.SafetyNotesCheck = function (notifyval) {
        $scope.SafetyNotes = notifyval;

        var x = notifyval;
        if (x == 'Y') {
            $scope.SafetyNotesShow = true;
        }
        else if (x == 'N') {
            $scope.SafetyNotesShow = false;
        }

    }


    $scope.ArrayGroupByKey = function (items) {

        var groups = items.reduce(function (obj, item) {
            obj[item.Key] = obj[item.Key] || [];
            obj[item.Key].push(item.Value);
            return obj;
        }, {});
        var myArray = Object.keys(groups).map(function (key) {
            return { Key: key, Value: groups[key] };
        });
        console.log(myArray);
        return myArray;
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "AssetID";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getViewAssetsList();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getViewAssetsList();
    }

    //Loading employees list on first time
    $scope.getViewAssetsList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getViewAssetsList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getViewAssetsList();
    };
});


