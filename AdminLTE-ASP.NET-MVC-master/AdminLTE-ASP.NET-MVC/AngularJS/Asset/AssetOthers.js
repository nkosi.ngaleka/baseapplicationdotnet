﻿app.controller('OthersCtrl', function ($scope, $filter, $http, $compile) {
    $scope.isFormValid = false;
    $scope.$watch('AssetOthersForm.$valid', function (newValue) {
        $scope.isFormValid = newValue;
        //alert(newValue)
    });

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = '10'; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.totalCount = 0;

  
    $scope.getViewAssetsList = function ()
    {
        $http.get("../Asset/GetAllAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&search=" + $scope.search).then(
            function (response) {
                if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                } else {
                    $scope.items = response.data.success.employees;
                    $scope.totalCount = response.data.success.totalCount;
                    $scope.showLoader = false;
                    $scope.showTable = true;
                }
            },
            function (err) {
                var error = err;
            });
    }

    $scope.getViewAssetsList();

    $scope.showAssetInsuranceDetails = function () {
        $scope.showTable = false;
        $scope.InsuranceShow = true;
    }

    $scope.showAssetWarrantyDetails = function () {
        $scope.showTable = false;
        $scope.WarrantyShow = true;
    }

    $scope.showAssetMaintenanceDetails = function () {
        $scope.showTable = false;
        $scope.MaintenanceShow = true;
    }

    $scope.showAssetServiceDetails = function () {
        $scope.showTable = false;
        $scope.ServiceShow = true;
    }

    $scope.showAssetSafetyNotesDetails = function () {
        $scope.showTable = false;
        $scope.SafetyNotesShow = true;
    }
    

    $scope.ViewInsurance = function (EncAssetID) {
        $scope.AssetID = EncAssetID;
        //$scope.showAssetInsuranceDetails();

        $http.post('../Asset/GetAssetInsuranceDetails', { EncAssetID: EncAssetID }).then(function (response) {
            console.log('respose');
            console.log(response);

            if (response.data.success) {
                if (response.data.AssetData.length > 0) {
                    $scope.AssetName = response.data.AssetData[0].AssetName;
                    $scope.BarcodeNumber = response.data.AssetData[0].BarcodeNumber;
                }
                if (response.data.AssetInsuranceData.length > 0) {
                    $scope.SaveType = "Update";
                }
                else {
                    $scope.SaveType = "Save";
                }
                $scope.Insurance = response.data.AssetInsuranceData[0];

               
                
                $('#modalInsurance').modal('show');

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
        });
    }

    $scope.SaveInsurance = function (IData) {

        var EncAssetID = $scope.AssetID;
        $scope.Insurance_Submitted = true;
        //alert($scope.isFormValid);
        if ($scope.isFormValid) {
            $scope.imgLoader_Save = true;

            $http.post('../Asset/SaveInsuranceDetails?EncAssetID=' + EncAssetID, IData)
                .then(function (response) {
                   
                    $scope.imgLoader_Save = false;
                    alertify.alert('Success', response.data.success, function () {
                        window.location.href = '../Home/Insurance';
                    });
                });
        }
        
    }

    $scope.ViewWarranty = function (EncAssetID) {
        $scope.AssetID = EncAssetID;
       // $scope.showAssetWarrantyDetails();

        $http.post('../Asset/GetAssetWarrantyDetails', { EncAssetID: EncAssetID }).then(function (response) {
           
            if (response.data.success) {
                if (response.data.AssetData.length > 0) {
                    $scope.AssetName = response.data.AssetData[0].AssetName;
                    $scope.BarcodeNumber = response.data.AssetData[0].BarcodeNumber;
                }
                if (response.data.AssetWarrantyData.length > 0) {
                    $scope.SaveType = "Update";
                }
                else {
                    $scope.SaveType = "Save";
                }
                $scope.Warranty = response.data.AssetWarrantyData[0];
                $('#modalWarranty').modal('show');

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            
        });
    }

    $scope.SaveWarranty = function (WData) {

        var EncAssetID = $scope.AssetID;
        $scope.Warranty_Submitted = true;
        //alert($scope.isFormValid);
        if ($scope.isFormValid) {
            $scope.imgLoader_Save = true;

            $http.post('../Asset/SaveWarrantyDetails?EncAssetID=' + EncAssetID, WData)
                .then(function (response) {

                    $scope.imgLoader_Save = false;
                    alertify.alert('Success', response.data.success, function () {
                        window.location.href = '../Home/Warranty';
                    });
                });
        }

    }

    $scope.ViewMaintenance = function (EncAssetID) {

        $scope.AssetID = EncAssetID;

        $http.post('../Asset/GetAssetMaintenanceDetails', { EncAssetID: EncAssetID }).then(function (response) {

            if (response.data.success) {

                if (response.data.AssetData.length > 0) {
                    $scope.AssetName = response.data.AssetData[0].AssetName;
                    $scope.BarcodeNumber = response.data.AssetData[0].BarcodeNumber;
                }
                if (response.data.AssetMaintenanceData.length > 0) {
                    $scope.SaveType = "Update";
                }
                else {
                    $scope.SaveType = "Save";
                }

                $scope.Maintenance = response.data.AssetMaintenanceData[0];
                $('#modalMaintenance').modal('show');

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    $scope.SaveMaintenance = function (MData) {

        var EncAssetID = $scope.AssetID;
        $scope.Maintenance_Submitted = true;

        if ($scope.isFormValid) {
            $scope.imgLoader_Save = true;

            $http.post('../Asset/SaveMaintenanceDetails?EncAssetID=' + EncAssetID, MData)
                .then(function (response) {

                    $scope.imgLoader_Save = false;
                    alertify.alert('Success', response.data.success, function () {
                        window.location.href = '../Home/Maintenance';
                    });
                });
        }

    }

    $scope.ViewService = function (EncAssetID) {
        $scope.AssetID = EncAssetID;
       // $scope.showAssetServiceDetails();

        $http.post('../Asset/GetAssetServiceDetails', { EncAssetID: EncAssetID }).then(function (response) {
            if (response.data.AssetData.length > 0) {
                $scope.AssetName = response.data.AssetData[0].AssetName;
                $scope.BarcodeNumber = response.data.AssetData[0].BarcodeNumber;
            }
            
            if (response.data.success) {

                if (response.data.AssetServiceData.length > 0) {
                    $scope.SaveType = "Update";
                }
                else {
                    $scope.SaveType = "Save";
                }
                $scope.Service = response.data.AssetServiceData[0];
                $('#modalService').modal('show');

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
           
        });
    }

    $scope.SaveService = function (SData) {

        var EncAssetID = $scope.AssetID;
        $scope.Service_Submitted = true;
        //alert($scope.isFormValid);
        if ($scope.isFormValid) {
            $scope.imgLoader_Save = true;

            $http.post('../Asset/SaveServiceDetails?EncAssetID=' + EncAssetID, SData)
                .then(function (response) {

                    $scope.imgLoader_Save = false;
                    alertify.alert('Success', response.data.success, function () {
                        window.location.href = '../Home/Service';
                    });
                });
        }

    }

    $scope.ViewSafetyNotes = function (EncAssetID) {
        $scope.AssetID = EncAssetID;

        $http.post('../Asset/GetAssetSafetyNotesDetails', { EncAssetID: EncAssetID }).then(function (response) {

            if (response.data.AssetData.length > 0) {
                $scope.AssetName = response.data.AssetData[0].AssetName;
                $scope.BarcodeNumber = response.data.AssetData[0].BarcodeNumber;
            }

            if (response.data.success) {

                if (response.data.AssetSafetyNotesData.length > 0) {
                    $scope.SaveType = "Update";
                }
                else {
                    $scope.SaveType = "Save";
                }
                $scope.SNotes = response.data.AssetSafetyNotesData[0];
                $('#modalSafetyNotes').modal('show');

            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
        });
    }

    $scope.SaveSafetyNotes = function (SNData) {

        var EncAssetID = $scope.AssetID;
        $scope.SafetyNotes_Submitted = true;

        if ($scope.isFormValid) {
            $scope.imgLoader_Save = true;

            $http.post('../Asset/SaveSafetyNotesDetails?EncAssetID=' + EncAssetID, SNData)
                .then(function (response) {

                    $scope.imgLoader_Save = false;
                    alertify.alert('Success', response.data.success, function () {
                        window.location.href = '../Home/SafetyNotes';
                    });
                });
        }

    }
    column = "AssetID";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        $scope.sorting = val;
        $scope.getViewAssetsList();
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getViewAssetsList();
    }

    //Loading employees list on first time
    $scope.getViewAssetsList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getViewAssetsList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getViewAssetsList();
    };
});