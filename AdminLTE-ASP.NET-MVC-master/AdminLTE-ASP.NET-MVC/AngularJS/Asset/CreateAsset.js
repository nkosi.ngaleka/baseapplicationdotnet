﻿app.controller('CreateAssetCtrl', function ($scope, $filter, $http, $compile) {

    // Configuration
    $scope.CreateAssetData = null;
    $scope.LoadingSuppliers = false;
    $scope.LoadingEmployees = false;
    $scope.ShowLocationTab = true;

    $scope.OthersData = {};
    $scope.OthersData.Insurance = {};
    $scope.OthersData.Warranty = {};
    $scope.OthersData.Maintenance = {};
    $scope.OthersData.Service = {};
    $scope.OthersData.SafetyNotes = {};

    $scope.dropdownIconDefault = 'icon icon-text';

    $scope.dropdownIconSpinner = 'fa fa-refresh fa-spin';

    $scope.setDropdownIcon = function (id) {
        $('#' + id).attr('class', $scope.dropdownIconSpinner);
    }

    $scope.resetDropdownIcon = function (id) {
        $('#' + id).attr('class', $scope.dropdownIconDefault);
    }

    $scope.setDropdownIcon(1, 'default');

    $scope.classDisable = true;
    $scope.categoryDisable = true;
    $scope.subCategoryDisable = true;
    $scope.displayDispose = false;
    $scope.imgLoader_Save = false;
    //$scope.liLast = false;
    $scope.liLast = true;

    $scope.showResidualValueCompareError = false;
    $scope.showTransferAssetPriceError = false;
    $scope.TransferAssetPriceMessage = "";

    // Function,Project,Fund,Region selection
    $scope.FunctionGUID = '';
    $scope.ProjectGUID = '';
    $scope.RegionGUID = '';
    $scope.FundGUID = '';
    $scope.CostingGUID = '';
    $scope.ItemGUID = '';
    //$scope.FunctionLevelID = '';

    $scope.SelectedFunctionLabel = 'Function';
    $scope.SelectedProjectLabel = 'Project';
    $scope.SelectedFundLabel = 'Fund';
    $scope.SelectedRegionLabel = 'Region';
    $scope.SelectedItemLabel = 'Item';
    $scope.SelectedCostingLabel = 'Costing';
    $scope.SelectedSupplierLabel = 'Select';
    $scope.SelectedEmployeeLabel = 'Select';

    $scope.SelectedBuildingLabel = 'Select';
    $scope.SelectedRoomLabel = 'Select';

    //Document Upload
    var scannedDocsFiles = [];
    var formData = new FormData();
    var Uploadedfiles = [];
    var isTrue = false;

    $scope.Tab1 = true;
    $scope.Tab2 = false;
    $scope.Tab3 = false;
    $scope.Tab4 = false;
    $scope.Tab5 = false;
    $scope.Tab6 = false;

    $scope.SaveType = '';
    $scope.HasInfrastructure = false;

    $scope.SetDepreciationMethod = false;// Set Depreciation Method on create

    //@*MCODE *@
    //Set classes of Create Asset MCode
    $scope.InsuranceShow = false;
    $scope.WarrantyShow = false;
    $scope.MaintenanceShow = false;
    $scope.ServiceShow = false;
    $scope.SafetyNotesShow = false;

    $scope.InsuranceCheck = function (insuranceval) {
        $scope.Insurance = insuranceval;
        var x = insuranceval;
        if (x == 'Y') {
            $scope.InsuranceShow = true;
            $scope.Tab7_1 = true;
        }
        else if (x == 'N') {
            $scope.InsuranceShow = false;
            $scope.Tab7_1 = false;
        }

    }

    $scope.WarrantyCheck = function (warrantyval) {
        $scope.Warranty = warrantyval;
        var x = warrantyval;
        if (x == 'Y') {
            $scope.WarrantyShow = true;
            $scope.Tab7_2 = true;
        }
        else if (x == 'N') {
            $scope.WarrantyShow = false;
            $scope.Tab7_2 = false;
        }

    }

    $scope.MaintenanceCheck = function (maintainval) {
        $scope.Maintenance = maintainval;
        var x = maintainval;
        if (x == 'Y') {
            $scope.MaintenanceShow = true;
            $scope.Tab7_3 = true;

        }
        else if (x == 'N') {
            $scope.MaintenanceShow = false;
            $scope.Tab7_3 = false;

        }

    }

    $scope.ServiceCheck = function (serviceval) {
        $scope.Service = serviceval;

        var x = serviceval;
        if (x == 'Y') {
            $scope.ServiceShow = true;
            $scope.Tab7_4 = true;

        }
        else if (x == 'N') {
            $scope.ServiceShow = false;
            $scope.Tab7_4 = false;

        }

    }

    $scope.SafetyNotesCheck = function (notifyval) {
        $scope.SafetyNotes = notifyval;

        var x = notifyval;
        if (x == 'Y') {
            $scope.SafetyNotesShow = true;
            $scope.Tab7_5 = true;

        }
        else if (x == 'N') {
            $scope.SafetyNotesShow = false;
            $scope.Tab7_5 = false;

        }

    }

    //@*MCODE *@

    //Reading query string to decide add/edit
    function getUrlParameter(param, dummyPath) {

        var sPageURL = dummyPath || window.location.search.substring(1),
            sURLVariables = sPageURL.split(/[&||?]/),
            res;

        for (var i = 0; i < sURLVariables.length; i += 1) {
            var paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');

            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }

        return res;
    }

    var editAssetID = getUrlParameter('editAssetID');
    var epid = getUrlParameter('epid');
    var pname = getUrlParameter('pname');
    var etp = getUrlParameter('etp');
    var ParentAssetID = getUrlParameter('ParentAssetID');

    $scope.EPID = '';
    $scope.PNAME = '';
    $scope.FROM_PROJECT = false;

    if (epid !== undefined) {
        $scope.EPID = epid;
        $scope.PNAME = decodeURI(pname);
        $scope.FROM_PROJECT = true;
        $scope.TOTAL_PAID = Number(etp);
        console.log($scope.TOTAL_PAID);
    }

    //$scope.setDropdownIcon('icon-classification');

    // Get Edit Asset form data for Update
    if (editAssetID !== undefined) {

        $scope.SaveType = 'Update';

        $scope.FieldList = [];

        $http.post('../Asset/getCreateAssetFormData', { EncAssetID: editAssetID })
            .then(function (AssetRow) {

                console.log('AssetRow');
                console.log(AssetRow);


                if (AssetRow.data.FromProject === true) {
                    $scope.FROM_PROJECT = true;
                    $scope.PNAME = AssetRow.data.ProjectName;
                    $scope.TOTAL_PAID = Number(AssetRow.data.TotalPaid);
                }

                console.log($scope.TOTAL_PAID);

                if (AssetRow.data.success[0].OwnedByID == -1) {
                    AssetRow.data.success[0].OwnedByID = null;
                    AssetRow.data.success[0].OwnedByName = 'Select';
                }

                if (AssetRow.data.success[0].SupplierID == -1) {
                    AssetRow.data.success[0].SupplierID = null;
                    AssetRow.data.success[0].SupplierName = 'Select';
                }

                if (AssetRow.data.success[0].BuildingID == -1) {

                    if (AssetRow.data.success[0].BuildingName !== ''
                        && AssetRow.data.success[0].BuildingName !== null) {

                        AssetRow.data.success[0].BName = AssetRow.data.success[0].BuildingName;
                        $scope.SelectedBuildingLabel = AssetRow.data.success[0].BName;
                        AssetRow.data.success[0].BuildingID = -1;
                    } else {
                        AssetRow.data.success[0].BuildingID = null;
                        AssetRow.data.success[0].BName = 'Select';
                    }
                }
                else {
                    $scope.SelectedBuildingLabel = AssetRow.data.success[0].BBarcode + ' - ' + AssetRow.data.success[0].BName + ' (' + AssetRow.data.success[0].BLocation + ')';
                }

                if (AssetRow.data.success[0].RoomID == -1) {

                    //if (AssetRow.data.success[0].BuildingRoom !== ''
                    //    && AssetRow.data.success[0].BuildingRoom !== null) {
                    //    AssetRow.data.success[0].RName = AssetRow.data.success[0].BuildingRoom;
                    //    $scope.SelectedRoomLabel = AssetRow.data.success[0].RName;
                    //    AssetRow.data.success[0].RoomID = -1;

                    //    $scope.SelectedRoomLabel = '';
                    //    $scope.SelectedRoomLabel = 'Barcode: ' + AssetRow.data.success[0].RBarcode + ', Room: ' + AssetRow.data.success[0].RName;
                    //    $scope.CreateAssetData.RoomID = Room.RoomID;
                    //    $scope.CreateAssetData.BuildingName = Room.BuildingName;
                    //    $scope.CreateAssetData.BuildingFloor = Room.RoomFloor;
                    //    $scope.CreateAssetData.Location = Room.BuildingLocation;
                    //    $scope.CreateAssetData.Site = Room.BuildingSite;

                    //} else {
                    AssetRow.data.success[0].RoomID = null;
                    AssetRow.data.success[0].RName = 'Select';
                    //}

                }
                else {
                    $scope.SelectedRoomLabel = 'Barcode: ' + AssetRow.data.success[0].RBarcode + ', Room: ' + AssetRow.data.success[0].RName;
                    AssetRow.data.success[0].BuildingName = AssetRow.data.success[0].BName;
                    AssetRow.data.success[0].BuildingFloor = AssetRow.data.success[0].RFloor;
                    AssetRow.data.success[0].Location = AssetRow.data.success[0].BLocation;
                    AssetRow.data.success[0].Site = AssetRow.data.success[0].BSite;
                }

                $scope.ClassificationList = AssetRow.data.ClassificationsList.Data.success;
                $scope.ClassList = AssetRow.data.ClassesList.Data.success;
                $scope.CategoryList = AssetRow.data.CategoriesList.Data.success;
                $scope.SubCategoryList = AssetRow.data.SubCategoriesList.Data.success;

                $scope.BuildingsList = AssetRow.data.BuildingsList.success;
                $scope.RoomsList = AssetRow.data.RoomsList.success;

                $scope.SelectedProjectLabel = AssetRow.data.ProjectLevel;
                $scope.SelectedRegionLabel = AssetRow.data.RegionLevel;
                $scope.SelectedFundLabel = AssetRow.data.FundLevel;
                $scope.SelectedItemLabel = AssetRow.data.ItemLevel;
                $scope.SelectedCostingLabel = AssetRow.data.CostingLevel;
                $scope.SelectedFunctionLabel = AssetRow.data.FunctionLevel;

                $scope.DeprecationMethodList = AssetRow.data.DeprecationMethodsList.Data.success;
                $scope.SuppliersList = AssetRow.data.SuppliersList.Data.success;
                $scope.EmployeesList = AssetRow.data.EmployeesList.Data.success;

                $scope.classDisable = false;
                $scope.categoryDisable = false;
                $scope.subCategoryDisable = false;

                //AssetRow.data.success[0].ContactNumber = parseInt(AssetRow.data.success[0].ContactNumber);

                if (AssetRow.data.success[0].ClassificationRemoved === true) {

                    AssetRow.data.success[0].ClassificationID = "";
                    AssetRow.data.success[0].ClassID = "";
                    AssetRow.data.success[0].CategoryID = "";
                    AssetRow.data.success[0].SubCategoryID = "";

                    $scope.classDisable = true;
                    $scope.categoryDisable = true;
                    $scope.subCategoryDisable = true;

                }

                if (AssetRow.data.success[0].ClassRemoved === true) {

                    AssetRow.data.success[0].ClassID = "";
                    AssetRow.data.success[0].CategoryID = "";
                    AssetRow.data.success[0].SubCategoryID = "";

                    $scope.categoryDisable = true;
                    $scope.subCategoryDisable = true;

                }

                if (AssetRow.data.CategoryRemoved === true) {

                    AssetRow.data.success[0].CategoryID = "";
                    AssetRow.data.success[0].SubCategoryID = "";

                    $scope.subCategoryDisable = true;
                }

                if (AssetRow.data.SubCategoryRemoved === true) {
                    AssetRow.data.success[0].SubCategoryID = "";
                }

                //if (AssetRow.data.success[0].SubCategoryRemoved == 'true') {
                //    AssetRow.data.success[0].SubCategoryID = null;
                //}

                $scope.CreateAssetData = AssetRow.data.success[0];

                //CAData.FunctionLevelName = $scope.FunctionLevelName;
                $scope.FunctionLevelID = AssetRow.data.success[0].FunctionLevelID;
                $scope.ProjectGUID = AssetRow.data.success[0].ProjectGUID;
                $scope.RegionGUID = AssetRow.data.success[0].RegionGUID;
                $scope.FundGUID = AssetRow.data.success[0].FundGUID;
                $scope.ItemGUID = AssetRow.data.success[0].ItemGUID;
                $scope.CostingGUID = AssetRow.data.success[0].CostingGUID;

                $scope.SelectedSupplierLabel = AssetRow.data.success[0].SupplierName;
                $scope.SelectedEmployeeLabel = AssetRow.data.success[0].OwnedByName;

                $scope.ExistingAssetAttachmentList = AssetRow.data.AttachmentsList.Data.success;

                //INFRA DATA WITH VALUES
                var IData1 = AssetRow.data.InfrastructureFieldsWithData;
                $scope.HasInfrastructure = AssetRow.data.HasInfrastructure;

                //$scope.IDataAttachmentRaw = AssetRow.data.InfrastructureDataWithValues_ATT;
                //$scope.IDataAttachment = $scope.ArrayGroupByKey(AssetRow.data.InfrastructureDataWithValues_ATT);

                if ($scope.HasInfrastructure == true) {
                    for (var i = 0; i < IData1.length; i++) {

                        $scope.FieldList.push({
                            ID: IData1[i].ID,
                            UploadID: 'U' + IData1[i].ID,
                            ShowUploadLoader: false,
                            AttachmentList: [],
                            ModelName: IData1[i].FieldType == 'DROPDOWN' ? parseInt(IData1[i].Value) : IData1[i].Value,
                            FieldName: 'F' + IData1[i].ID,
                            LabelName: IData1[i].FieldName,
                            FieldType: IData1[i].FieldType,
                            FieldRequired: IData1[i].Required,
                            DDList: IData1[i].DropdownList,
                            FieldValue: IData1[i].Value,
                            ExistingAttachmentsList: IData1[i].AttachmentData
                        });

                    }

                    $scope.ShowLocationTab = false;
                }

                $scope.$on('ngRepeatFinishedJDP', function (ngRepeatFinishedEvent) {
                    if ($scope.HasInfrastructure) {
                        $scope.FieldList.forEach(function (c) {

                            if (c.FieldType == 'DATE') {
                                //Datepicker Jquery
                                $("#" + c.FieldName).datepicker({
                                    dateFormat: "yy-mm-dd",
                                    onSelect: function (d) {

                                        var key = $scope.findObjectIndexByKey($scope.FieldList, 'ID', c.ID);
                                        $scope.FieldList[key].ModelName = d;

                                    }
                                });
                            }

                        });
                    }
                });

            });

        //$("#InfraItems *").attr("disabled", "disabled")

    }
    else {

        //Add
        $scope.SaveType = 'Add';

        $scope.setDropdownIcon('icon-classification');

        //Gets Employees List
        $http.post('../Asset/getEmployees').then(function (cdata) {

            if (cdata.data.success) {
                $scope.EmployeesList = cdata.data.success;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });

        //Gets Suppliers
        $http.post('../Asset/getSuppliers').then(function (cdata) {

            if (cdata.data.success) {
                $scope.SuppliersList = cdata.data.success;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (cdata.data.error) {

            }

        });

        //Gets Classifications
        $http.post('../Asset/getClassifications').then(function (cdata) {

            if (cdata.data.success) {
                $scope.ClassificationList = cdata.data.success;
                $scope.resetDropdownIcon('icon-classification');
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (cdata.data.error) {
                $scope.resetDropdownIcon('icon-classification');
            }

        });

        //Gets Deprecation Method List
        $http.post('../Asset/getDeprecationMethods').then(function (cdata) {

            if (cdata.data.success) {
                $scope.DeprecationMethodList = cdata.data.success;
                $scope.SetDepreciationMethod = true;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (cdata.data.error) {

            }


        });

        //Gets Rooms
        $http.post('../Asset/GetBuildingWiseRooms').then(function (cdata) {
            if (cdata.data.success) {
                $scope.RoomsList = cdata.data.success;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (cdata.data.error) {

            }
        });

    }

    $scope.ArrayGroupByKey = function (items) {

        var groups = items.reduce(function (obj, item) {
            obj[item.FieldInspectionItem] = obj[item.FieldInspectionItem] || [];
            obj[item.FieldInspectionItem].push(item.AttachmentName);
            return obj;
        }, {});

        var myArray = Object.keys(groups).map(function (key) {
            return { FieldInspectionItem: key, AttachmentName: groups[key] };
        });

        console.log(myArray);

        return myArray;
    }

    //Employee select start

    $scope.SearchEmployee = function () {
        $scope.EmployeesList = null;
        $scope.LoadingEmployees = true;
        $http.post('../Asset/getEmployees', { Keyword: $scope.EmployeeSearchKeyword }).then(function (cdata) {

            if (cdata.data.success) {
                $scope.LoadingEmployees = false;
                $scope.EmployeesList = cdata.data.success;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (cdata.data.error) {
                $scope.LoadingEmployees = false;
            }
        });

    }

    $scope.EmployeeSelected = function (EmployeeID, EmployeeName) {
        //console.log("CREATE ASSET DATA")
        //console.log($scope.CreateAssetData)
        $scope.SelectedEmployeeLabel = '';
        $scope.SelectedEmployeeLabel = EmployeeName;
        $scope.CreateAssetData.OwnedByID = EmployeeID;
        $("#modalEmployees .close").click();
    }

    //Supplier select start

    $scope.SearchSupplier = function () {

        $scope.SuppliersList = null;
        $scope.LoadingSuppliers = true;
        $http.post('../Asset/getSuppliers', { Keyword: $scope.SupplierSearchKeyword })
             .then(function (cdata) {

                 if (cdata.data.success) {
                     $scope.LoadingSuppliers = false;
                     $scope.SuppliersList = cdata.data.success;
                 } else if (cdata.data.expire) {
                     window.location.href = '../Home/Dashboard';
                 }
                 else if (cdata.data.error) {
                     $scope.LoadingSuppliers = false;
                 }

             });

    }

    $scope.SearchRoom = function () {

        $scope.RoomsList = null;
        $scope.LoadingRooms = true;
        $http.post('../Asset/GetBuildingWiseRooms', { Keyword: $scope.RoomSearchKeyword }).then(function (cdata) {

            if (cdata.data.success) {
                $scope.LoadingRooms = false;
                $scope.RoomsList = cdata.data.success;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (cdata.data.error) {
                $scope.LoadingRooms = false;
            }
        });

    }

    $scope.SupplierSelected = function (SupplierID, SupplierName) {
        console.log("CREATE ASSET DATA")
        console.log($scope.CreateAssetData)
        $scope.SelectedSupplierLabel = '';
        $scope.SelectedSupplierLabel = SupplierName;
        $scope.CreateAssetData.SupplierID = SupplierID;
        $("#modalSuppliers .close").click();
    }

    $scope.BuildingSelected = function (Building) {

        console.log("CREATE ASSET DATA");
        console.log($scope.CreateAssetData);
        console.log(Building);

        $scope.SelectedBuildingLabel = '';
        $scope.SelectedBuildingLabel = 'Barcode: ' + Building.BuildingBarcode + ', Building: ' + Building.BuildingName + ', Location: ' + Building.BuildingLocation;
        $scope.CreateAssetData.BuildingID = Building.BuildingID;
        $scope.CreateAssetData.Site = Building.BuildingSite;
        $scope.CreateAssetData.Location = Building.BuildingLocation;

        $("#modalBuildings .close").click();

        $scope.SelectedBuildingID = Building.BuildingID;

        $scope.RoomsList = null;
        $scope.LoadingRooms = true;

        $scope.CreateAssetData.RoomID = undefined;
        $scope.SelectedRoomLabel = 'Select';

        $http.post('../Asset/GetRooms', { BuildingID: $scope.SelectedBuildingID })
             .then(function (cdata) {

                 if (cdata.data.success) {
                     $scope.LoadingRooms = false;
                     $scope.RoomsList = cdata.data.success;
                 }
                 else if (cdata.data.expire) {
                     window.location.href = '../Home/Dashboard';
                 }
                 else if (cdata.data.error) {
                     $scope.LoadingRooms = false;
                 }

             });

    }

    $scope.RoomSelected = function (Room) {
        console.log("CREATE ASSET DATA");
        console.log($scope.CreateAssetData);
        $scope.SelectedRoomLabel = '';
        $scope.SelectedRoomLabel = 'Barcode: ' + Room.RoomBarcode + ', Room: ' + Room.RoomName;
        $scope.CreateAssetData.RoomID = Room.RoomID;
        $scope.CreateAssetData.BuildingName = Room.BuildingName;
        $scope.CreateAssetData.BuildingFloor = Room.RoomFloor;
        $scope.CreateAssetData.Location = Room.BuildingLocation;
        $scope.CreateAssetData.Site = Room.BuildingSite;

        $("#modalRooms .close").click();
    }

    //PROJECT TREE START

    //Gets project levels
    $http.post('../Asset/getProjectLevels').then(function (ProjectData) {
        //console.log('ProjectData')
        //console.log(ProjectData.data.success)
        if (ProjectData.data.success) {
            $scope.p = ProjectData.data.success[0].tbl_mscoa_process;
            $scope.p0 = ProjectData.data.success[0].p0;
            $scope.p1 = ProjectData.data.success[0].p1;
            $scope.p2 = ProjectData.data.success[0].p2;
            $scope.p3 = ProjectData.data.success[0].p3;
            $scope.p4 = ProjectData.data.success[0].p4;
            $scope.p5 = ProjectData.data.success[0].p5;
            $scope.p6 = ProjectData.data.success[0].p6;
            $scope.p7 = ProjectData.data.success[0].p7;
            $scope.p8 = ProjectData.data.success[0].p8;

            $scope.$on('ngRepeatFinishedProject', function (ngRepeatFinishedEvent) {
                $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
                $('.ProjectChildLevel').hide();
            });
        } else if (ProjectData.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
        else if (ProjectData.data.error) {
            $scope.ProjectLevelListError = ProjectData.data.error;
        }
    });

    $scope.ProjectSelected = function (a, b) {
        var id = a + '' + b;

        var children = $('#' + id).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $('#' + id).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');

        } else {
            children.show('fast');
            $('#' + id).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        }

    }

    $scope.SelectedProjectLeaf = function (GUID, l0, l1, l2, l3, l4, l5, l6, l7, l8) {
        $scope.SelectedProjectLabel = '';

        $scope.SelectedProjectLabel = 'Project >> ' + l0 + ' >> ' + l1 + ' >> ' + l2 + ' >> ' + l3 + ' >> ' + l4 + ' >> ' + l5 + ' >> ' + l6 + ' >> ' + l7 + ' >> ' + l8;

        $("#modalProjectLevels .close").click();

        $scope.ProjectGUID = GUID;

        $scope.CreateAssetData.ProjectGUID = GUID;
        //alert(GUID)
    }

    $scope.GetProjectLeafLevel = function (id1, id2, l0, l1, l2, l3, l4, l5, l6, l7) {

        $http.post('../Asset/getProjectLeafLevel',
            {
                l0: l0,
                l1: l1,
                l2: l2,
                l3: l3,
                l4: l4,
                l5: l5,
                l6: l6,
                l7: l7
            })
       .then(
           function (LeafLevelData) {

               if (LeafLevelData.data.success) {
                   $scope.leaf = LeafLevelData.data.success;

                   var HTMLCode = '<ul style="margin-left: 30px;">';

                   for (var i = 0; i < $scope.leaf.length; i++) {

                       if ($scope.leaf[i].PostingLevel == 'Yes') {

                           HTMLCode = HTMLCode + '<li title="' + $scope.leaf[i].Definition + '" ng-show=\'liLast\' class="ProjectChildLevel" ><span style="background: lightblue;cursor: pointer;"' +
                               'ng-click="SelectedProjectLeaf(\'' + $scope.leaf[i].GUID + '\',\'' + $scope.leaf[i].LV0 + '\',\'' + $scope.leaf[i].LV1 + '\',\'' + $scope.leaf[i].LV2 + '\',\'' + $scope.leaf[i].LV3 + '\',\'' + $scope.leaf[i].LV4 + '\',\'' + $scope.leaf[i].LV5 + '\',\'' + $scope.leaf[i].LV6 + '\',\'' + $scope.leaf[i].LV7 + '\',\'' + $scope.leaf[i].LV8 + '\')">' +
                               '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.leaf[i].LV8 +
                           '</span>' +
                           '</li>';
                       } else {

                           HTMLCode = HTMLCode + '<li title="' + $scope.leaf[i].Definition + '" ng-show=\'liLast\' class="ProjectChildLevel" ><span style="background: #ff3333;cursor: not-allowed;">' +
                           '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.leaf[i].LV8 +
                           '</span>' +
                           '</li>';
                       }
                   }

                   HTMLCode = HTMLCode + '</ul>';

                   var temp = $compile(HTMLCode)($scope);
                   $("li[id='" + id1 + "" + id2 + "'] ul").remove();
                   angular.element(document.getElementById(id1 + '' + id2)).append(temp);

               } else if (LeafLevelData.data.expire) {
                   window.location.href = '../Home/Dashboard';
               }
               else if (LeafLevelData.data.error) {
                   $scope.LeafLevelListError = LeafLevelData.data.error;
               }
           }
       );

    }

    //PROJECT TREE END


    //FUND TREE START

    //Gets fund levels
    $http.post('../Asset/getFundLevels').then(function (FundData) {

        if (FundData.data.success) {
            $scope.fund = FundData.data.success[0].tbl_mscoa_process;
            $scope.fund0 = FundData.data.success[0].fund0;
            $scope.fund1 = FundData.data.success[0].fund1;
            $scope.fund2 = FundData.data.success[0].fund2;
            $scope.fund3 = FundData.data.success[0].fund3;
            $scope.fund4 = FundData.data.success[0].fund4;
            $scope.fund5 = FundData.data.success[0].fund5;
            $scope.fund6 = FundData.data.success[0].fund6;
            $scope.fund7 = FundData.data.success[0].fund7;

            $scope.$on('ngRepeatFinishedFund', function (ngRepeatFinishedEvent) {
                $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
                $('.FundChildLevel').hide();
            });
        } else if (FundData.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
        else if (FundData.data.error) {
            $scope.FundLevelListError = FundData.data.error;
        }
    });

    $scope.FundSelected = function (a, b) {
        var id = a + '' + b;

        var children = $('#' + id).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $('#' + id).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');

        } else {
            children.show('fast');
            $('#' + id).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        }


    }

    $scope.SelectedFundLeaf = function (GUID, l0, l1, l2, l3, l4, l5, l6, l7) {
        $scope.SelectedFundLabel = '';

        $scope.SelectedFundLabel = 'Fund >> ' + l0 + ' >> ' + l1 + ' >> ' + l2 + ' >> ' + l3 + ' >> ' + l4 + ' >> ' + l5 + ' >> ' + l6 + ' >> ' + l7;

        $("#modalFundLevels .close").click();

        $scope.FundGUID = GUID;

        $scope.CreateAssetData.FundGUID = GUID;

    }

    $scope.GetFundLeafLevel = function (id1, id2, l0, l1, l2, l3, l4, l5, l6) {

        $http.post('../Asset/getFundLeafLevel',
            {
                l0: l0,
                l1: l1,
                l2: l2,
                l3: l3,
                l4: l4,
                l5: l5,
                l6: l6
            })
       .then(
           function (LeafLevelData) {

               if (LeafLevelData.data.success) {
                   $scope.FundLeaf = LeafLevelData.data.success;

                   var HTMLCode = '<ul style="margin-left: 30px;">';

                   for (var i = 0; i < $scope.FundLeaf.length; i++) {

                       if ($scope.FundLeaf[i].PostingLevel == 'Yes') {

                           HTMLCode = HTMLCode + '<li  title="' + $scope.FundLeaf[i].Definition + '" ng-show=\'liLast\' class="FundChildLevel" ><span style="background: lightblue;cursor: pointer;"' +
                               'ng-click="SelectedFundLeaf(\'' + $scope.FundLeaf[i].GUID + '\',\'' + $scope.FundLeaf[i].LV0 + '\',\'' + $scope.FundLeaf[i].LV1 + '\',\'' + $scope.FundLeaf[i].LV2 + '\',\'' + $scope.FundLeaf[i].LV3 + '\',\'' + $scope.FundLeaf[i].LV4 + '\',\'' + $scope.FundLeaf[i].LV5 + '\',\'' + $scope.FundLeaf[i].LV6 + '\',\'' + $scope.FundLeaf[i].LV7 + '\')">' +
                               '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.FundLeaf[i].LV7 +
                               '</span>' +
                               '</li>';

                       } else {

                           HTMLCode = HTMLCode + '<li  title="' + $scope.FundLeaf[i].Definition + '" ng-show=\'liLast\' class="FundChildLevel" ><span style="background: #ff3333;cursor: not-allowed;">' +
                                '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.FundLeaf[i].LV7 +
                                '</span>' +
                                '</li>';
                       }

                   }

                   HTMLCode = HTMLCode + '</ul>';

                   var temp = $compile(HTMLCode)($scope);
                   $("li[id='" + id1 + "" + id2 + "'] ul").remove();
                   angular.element(document.getElementById(id1 + '' + id2)).append(temp);


               } else if (LeafLevelData.data.expire) {
                   window.location.href = '../Home/Dashboard';
               }
               else if (LeafLevelData.data.error) {
                   $scope.FundLeafLevelListError = LeafLevelData.data.error;
               }
           }
       );

    }

    //FUND TREE END


    //ITEM TREE START

    //Gets item levels
    $http.post('../Asset/getItemLevels').then(function (ItemData) {

        if (ItemData.data.success) {
            $scope.item = ItemData.data.success[0].tbl_mscoa_process;
            $scope.item0 = ItemData.data.success[0].item0;
            $scope.item1 = ItemData.data.success[0].item1;
            $scope.item2 = ItemData.data.success[0].item2;
            $scope.item3 = ItemData.data.success[0].item3;
            $scope.item4 = ItemData.data.success[0].item4;
            $scope.item5 = ItemData.data.success[0].item5;
            $scope.item6 = ItemData.data.success[0].item6;

            $scope.$on('ngRepeatFinishedItem', function (ngRepeatFinishedEvent) {
                $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
                $('.ItemChildLevel').hide();
            });
        } else if (ItemData.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
        else if (ItemData.data.error) {
            $scope.ItemLevelListError = ItemData.data.error;
        }
    });

    $scope.ItemSelected = function (a, b) {
        var id = a + '' + b;

        var children = $('#' + id).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $('#' + id).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');

        } else {
            children.show('fast');
            $('#' + id).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        }


    }

    $scope.SelectedItemLeaf = function (GUID, l0, l1, l2, l3, l4, l5, l6, l7) {
        $scope.SelectedItemLabel = '';

        $scope.SelectedItemLabel = 'Item >> ' + l0 + ' >> ' + l1 + ' >> ' + l2 + ' >> ' + l3 + ' >> ' + l4 + ' >> ' + l5 + ' >> ' + l6 + ' >> ' + l7;

        $("#modalItemLevels .close").click();

        $scope.ItemGUID = GUID;
        $scope.CreateAssetData.ItemGUID = GUID;

    }

    $scope.GetItemLeafLevel = function (id1, id2, l0, l1, l2, l3, l4, l5, l6) {

        $http.post('../Asset/getItemLeafLevel',
            {
                l0: l0,
                l1: l1,
                l2: l2,
                l3: l3,
                l4: l4,
                l5: l5,
                l6: l6
            })
       .then(
           function (ItemLevelData) {

               if (ItemLevelData.data.success) {
                   $scope.ItemLeaf = ItemLevelData.data.success;

                   var HTMLCode = '<ul style="margin-left: 30px;">';

                   for (var i = 0; i < $scope.ItemLeaf.length; i++) {

                       if ($scope.ItemLeaf[i].PostingLevel == 'Yes') {

                           HTMLCode = HTMLCode + '<li  title="' + $scope.ItemLeaf[i].Definition + '" ng-show=\'liLast\' class="ItemChildLevel" ><span style="background: lightblue;cursor: pointer;"' +
                               'ng-click="SelectedItemLeaf(\'' + $scope.ItemLeaf[i].GUID + '\',\'' + $scope.ItemLeaf[i].LV0 + '\',\'' + $scope.ItemLeaf[i].LV1 + '\',\'' + $scope.ItemLeaf[i].LV2 + '\',\'' + $scope.ItemLeaf[i].LV3 + '\',\'' + $scope.ItemLeaf[i].LV4 + '\',\'' + $scope.ItemLeaf[i].LV5 + '\',\'' + $scope.ItemLeaf[i].LV6 + '\',\'' + $scope.ItemLeaf[i].LV7 + '\')">' +
                               '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.ItemLeaf[i].LV7 +
                               '</span>' +
                               '</li>';

                       } else {

                           HTMLCode = HTMLCode + '<li  title="' + $scope.ItemLeaf[i].Definition + '" ng-show=\'liLast\' class="ItemChildLevel" ><span style="background: #ff3333;cursor: not-allowed;">' +
                                '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.ItemLeaf[i].LV7 +
                                '</span>' +
                                '</li>';
                       }

                   }

                   HTMLCode = HTMLCode + '</ul>';

                   var temp = $compile(HTMLCode)($scope);
                   $("li[id='" + id1 + "" + id2 + "'] ul").remove();
                   angular.element(document.getElementById(id1 + '' + id2)).append(temp);

               } else if (ItemLevelData.data.expire) {
                   window.location.href = '../Home/Dashboard';
               }
               else if (ItemLevelData.data.error) {
                   $scope.ItemLeafLevelListError = ItemLevelData.data.error;
               }
           }
       );

    }

    //ITEM TREE END


    //REGION TREE START

    //Gets region levels
    $http.post('../Asset/getRegionLevels').then(function (RegionData) {

        console.log('RegionData')
        console.log(RegionData)

        if (RegionData.data.success) {
            $scope.r = RegionData.data.success[0].tbl_mscoa_process;
            $scope.r0 = RegionData.data.success[0].r0;
            $scope.r1 = RegionData.data.success[0].r1;
            $scope.r2 = RegionData.data.success[0].r2;
            $scope.r3 = RegionData.data.success[0].r3;
            $scope.r4 = RegionData.data.success[0].r4;
            $scope.r5 = RegionData.data.success[0].r5;
            $scope.r6 = RegionData.data.success[0].r6;
            $scope.r7 = RegionData.data.success[0].r7;
            $scope.r8 = RegionData.data.success[0].r8;

            $scope.$on('ngRepeatFinishedRegion', function (ngRepeatFinishedEvent) {
                $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
                $('.RegionChildLevel').hide();
            });
        } else if (RegionData.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
        else if (RegionData.data.error) {
            $scope.RegionLevelListError = RegionData.data.error;
        }
    });

    $scope.RegionSelected = function (a, b) {
        var id = a + '' + b;

        var children = $('#' + id).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $('#' + id).find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');

        } else {
            children.show('fast');
            $('#' + id).find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        }


    }

    $scope.SelectedRegionLeaf = function (GUID, l0, l1, l2, l3, l4, l5, l6, l7, l8, l9) {

        $scope.SelectedRegionLabel = '';

        $scope.SelectedRegionLabel = 'Region >> ' + l0 + ' >> ' + l1 + ' >> ' + l2 + ' >> ' + l3 + ' >> ' + l4 + ' >> ' + l5 + ' >> ' + l6 + ' >> ' + l7 + ' >> ' + l8 + ' >> ' + l9;

        $("#modalRegionLevels .close").click();

        $scope.RegionGUID = GUID;

        $scope.CreateAssetData.RegionGUID = GUID;
    }

    $scope.GetRegionLeafLevel = function (id1, id2, l0, l1, l2, l3, l4, l5, l6, l7, l8) {

        $http.post('../Asset/getRegionLeafLevel',
            {
                l0: l0,
                l1: l1,
                l2: l2,
                l3: l3,
                l4: l4,
                l5: l5,
                l6: l6,
                l7: l7,
                l8: l8
            })
            .then(
               function (RegionLeafLevelData) {

                   console.log('leaflevel')
                   console.log(RegionLeafLevelData)

                   if (RegionLeafLevelData.data.success) {
                       $scope.leafRegion = RegionLeafLevelData.data.success;

                       var HTMLCode = '<ul style="margin-left: 30px;">';

                       for (var i = 0; i < $scope.leafRegion.length; i++) {



                           if ($scope.leafRegion[i].PostingLevel == 'Yes') {
                               HTMLCode = HTMLCode + '<li title="' + $scope.leafRegion[i].Definition + '" ng-show=\'liLast\' class="RegionChildLevel" ><span style="background: lightblue;cursor: pointer;"' +
                                          'ng-click="SelectedRegionLeaf(\'' + $scope.leafRegion[i].GUID + '\',\'' + $scope.leafRegion[i].LV0 + '\',\'' + $scope.leafRegion[i].LV1 + '\',\'' + $scope.leafRegion[i].LV2 + '\',\'' + $scope.leafRegion[i].LV3 + '\',\'' + $scope.leafRegion[i].LV4 + '\',\'' + $scope.leafRegion[i].LV5 + '\',\'' + $scope.leafRegion[i].LV6 + '\',\'' + $scope.leafRegion[i].LV7 + '\',\'' + $scope.leafRegion[i].LV8 + '\',\'' + $scope.leafRegion[i].LV9 + '\')">' +
                                          '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.leafRegion[i].LV9 +
                                          '</span>' +
                                          '</li>';

                           } else {

                               HTMLCode = HTMLCode + '<li title="' + $scope.leafRegion[i].Definition + '" ng-show=\'liLast\' class="RegionChildLevel" ><span style="background: #ff3333;cursor: not-allowed;">' +
                                          '<i style="font-weight: 700;font-size: 14px;" class="fa fa-leaf"></i> ' + $scope.leafRegion[i].LV9 +
                                          '</span>' +
                                          '</li>';
                           }
                       }

                       HTMLCode = HTMLCode + '</ul>';

                       var temp = $compile(HTMLCode)($scope);
                       $("li[id='" + id1 + "" + id2 + "'] ul").remove();
                       angular.element(document.getElementById(id1 + '' + id2)).append(temp);


                   } else if (RegionLeafLevelData.data.expire) {
                       window.location.href = '../Home/Dashboard';
                   }
                   else if (RegionLeafLevelData.data.error) {
                       $scope.RegionLeafLevelListError = RegionLeafLevelData.data.error;
                   }
               }
       );

    }

    //REGION TREE END


    //NEW FUNCTION TREE START

    $http.post('../Asset/getFunctionLevels').then(function (FunctionData) {
        console.log('FunctionData');
        console.log(FunctionData);
        if (FunctionData.data.success) {
            $scope.function = FunctionData.data.success[0].tbl_mscoa_process;
            $scope.function1 = FunctionData.data.success[0].function1;
            $scope.function2 = FunctionData.data.success[0].function2;
            $scope.function3 = FunctionData.data.success[0].function3;
            //$scope.function4 = FunctionData.data.success[0].function4;

            $scope.$on('ngRepeatFinishedFunction', function (ngRepeatFinishedEvent) {
                $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
                $('.FunctionChildLevel').hide();
            });
        } else if (FunctionData.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
        else if (FunctionData.data.error) {
            $scope.FunctionLevelListError = FunctionData.data.error;
        }
    });

    $scope.FunctionSelected = function (a, b) {
        var id = a + '' + b;

        var children = $('#' + id).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $('#' + id).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');

        } else {
            children.show('fast');
            $('#' + id).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        }
    }

    $scope.SelectedFunctionLeaf = function (GUID, l1, l2, l3) {
        $scope.SelectedFunctionLabel = '';

        $scope.SelectedFunctionLabel = 'Function >> ' + l1 + ' >> ' + l2 + ' >> ' + l3;

        $("#myModalDefault .close").click();

        $scope.FunctionGUID = GUID;
        $scope.CreateAssetData.FunctionGUID = GUID;

    }

    //FUNCTION TREE END

    //COSTING TREE START

    //Gets fund levels
    $http.post('../Asset/getCostingLevels').then(function (CostingData) {

        if (CostingData.data.success) {
            $scope.costing = CostingData.data.success[0].tbl_mscoa_process;
            $scope.costing1 = CostingData.data.success[0].costing1;
            $scope.costing2 = CostingData.data.success[0].costing2;
            $scope.costing3 = CostingData.data.success[0].costing3;
            $scope.costing4 = CostingData.data.success[0].costing4;

            $scope.$on('ngRepeatFinishedFund', function (ngRepeatFinishedEvent) {
                $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
                $('.CostingChildLevel').hide();
            });
        } else if (CostingData.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
        else if (CostingData.data.error) {
            $scope.CostingLevelListError = CostingData.data.error;
        }
    });

    $scope.CostingSelected = function (a, b) {
        var id = a + '' + b;

        var children = $('#' + id).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $('#' + id).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');

        } else {
            children.show('fast');
            $('#' + id).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        }


    }

    $scope.SelectedCostingLeaf = function (GUID, l1, l2, l3, l4) {
        $scope.SelectedCostingLabel = '';

        $scope.SelectedCostingLabel = 'Costing >> ' + l1 + ' >> ' + l2 + ' >> ' + l3 + ' >> ' + l4;

        $("#modalCostingLevels .close").click();

        $scope.CostingGUID = GUID;
        $scope.CreateAssetData.CostingGUID = GUID;

    }

    //COSTING TREE END

    //Search Tree
    $scope.SearchTree = function () {

        $('.childLevel').hide();

        var SearchKeyword = $scope.SearchTreeKeyword;
        //alert(SearchKeyword.toLowerCase())

        $('input[class="lastLevel"][value*="' + SearchKeyword + '"]').closest('ul').closest('ul').closest('li').show();

    }

    $scope.classificationChange = function (value) {

        if (value == undefined) {
            $scope.classDisable = true;
            $scope.categoryDisable = true;
            $scope.subCategoryDisable = true;

        } else {

            $scope.setDropdownIcon('icon-class');

            $scope.classDisable = true;
            $scope.categoryDisable = true;
            $scope.subCategoryDisable = true;

            //Gets Class
            $http.post('../Asset/getClasses', { ClassificationID: value }).then(function (cdata) {

                if (cdata.data.success) {
                    $scope.ClassList = cdata.data.success;
                    $scope.classDisable = false;
                    $scope.resetDropdownIcon('icon-class');
                }
                else if (cdata.data.error) {
                    $scope.resetDropdownIcon('icon-class');
                } else if (cdata.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }

            });

        }

    }

    $scope.ClassChange = function (value) {

        if (value == undefined) {
            $scope.categoryDisable = true;
            $scope.subCategoryDisable = true;

        } else {

            $scope.setDropdownIcon('icon-category');

            $scope.categoryDisable = true;
            $scope.subCategoryDisable = true;

            var key = $scope.findObjectIndexByKey($scope.ClassList, 'Id', value);

            if ($scope.ClassList[key].Description == 'Infrastructure') {
                $scope.ShowLocationTab = false;
            } else {
                $scope.ShowLocationTab = true;
            }

            //Gets 
            $http.post('../Asset/getCategories', { ClassID: value }).then(function (cdata) {

                if (cdata.data.success) {
                    $scope.CategoryList = cdata.data.success;
                    $scope.categoryDisable = false;
                    $scope.subCategoryDisable = true;
                    $scope.resetDropdownIcon('icon-category');
                }
                else if (cdata.data.error) {
                    $scope.resetDropdownIcon('icon-category');

                } else if (cdata.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }

            });
        }

    }

    $scope.FieldList = [];

    $scope.CategoryChange = function (value) {
        $scope.FieldList = [];
        if (value == undefined) {
            $scope.subCategoryDisable = true;

        } else {

            $scope.setDropdownIcon('icon-subcategory');

            $scope.subCategoryDisable = true;

            //Gets 
            $http.post('../Asset/getSubCategoriesAndInfrastructureData', { CategoryID: value })
                .then(function (cdata) {

                    if (cdata.data.SubCategories) {

                        $scope.SubCategoryList = cdata.data.SubCategories;

                        var IData = cdata.data.InfrastructureData;

                        for (var i = 0; i < IData.length; i++) {

                            $scope.FieldList.push({
                                ID: IData[i].ID,
                                UploadID: 'U' + IData[i].ID,
                                ShowUploadLoader: false,
                                AttachmentList: [],
                                ModelName: '',
                                FieldName: 'F' + IData[i].ID,
                                LabelName: IData[i].FieldName,
                                FieldType: IData[i].FieldType,
                                FieldRequired: IData[i].Required,
                                HasSubFields: IData[i].HasSubFields,
                                DDList: IData[i].DropdownList
                            });

                        }

                        $scope.$on('ngRepeatFinishedJDP', function (ngRepeatFinishedEvent) {

                            $scope.FieldList.forEach(function (c) {
                                if (c.FieldType == 'DATE') {
                                    //Datepicker Jquery
                                    $("#" + c.FieldName).datepicker({
                                        dateFormat: "yy-mm-dd",
                                        onSelect: function (d) {

                                            var key = $scope.findObjectIndexByKey($scope.FieldList, 'ID', c.ID);
                                            $scope.FieldList[key].ModelName = d;
                                        }
                                    });
                                }
                            });

                        });

                        $scope.subCategoryDisable = false;

                        $scope.resetDropdownIcon('icon-subcategory');


                    } else if (cdata.data.error) {
                        $scope.resetDropdownIcon('icon-subcategory');

                    } else if (cdata.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }

                });
        }
    }

    //sets date in textbox on selecting date from jquery datepicker 
    $scope.SetDate = function (ID) {

        var key = $scope.findObjectIndexByKey($scope.FieldList, 'ID', ID);
        $scope.FieldList[key].ModelName = '';

    }

    //returns index of object in array of objects
    $scope.findObjectIndexByKey = function (array, key, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {

                return i;
            }
        }

        return null;

    }

    //Datepicker Jquery
    $(".jdp").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function (d) {

        }
    });

    //@MCODE@

    //Datepicker Jquery
    $("#I_Date").datepicker({
        dateFormat: "yy-mm-dd",
        //maxDate: 0,
        onSelect: function (d) {
            $scope.OthersData.Insurance.InsuranceDate = d;

            if (this.id === "I_Date") {
                // alert(this.id);
                $("#I_ExpiryDate").datepicker("option", "minDate", d);
            }
        }

    });

    //Datepicker Jquery
    $("#I_ExpiryDate").datepicker({
        dateFormat: "yy-mm-dd",
        //mindate:0,
        onSelect: function (d) {
            $scope.OthersData.Insurance.ExpiryDate = d;
            if (this.id === "I_ExpiryDate") {

                $("#I_Date").datepicker("option", "maxDate", d);

            }


        }

    });

    //Datepicker Jquery
    $("#W_Date").datepicker({
        dateFormat: "yy-mm-dd",
        // maxDate: 0,
        onSelect: function (d) {
            $scope.OthersData.Warranty.WarrantyDate = d;

            if (this.id === "W_Date") {

                $("#W_ExpiryDate").datepicker("option", "minDate", d);
            }
        }

    });

    //Datepicker Jquery
    $("#W_ExpiryDate").datepicker({
        dateFormat: "yy-mm-dd",
        //mindate: 0,
        onSelect: function (d) {
            $scope.OthersData.Warranty.ExpiryDate = d;
            if (this.id === "W_ExpiryDate") {

                $("#W_Date").datepicker("option", "maxDate", d);

            }


        }

    });

    //Datepicker Jquery
    $("#M_Date").datepicker({
        dateFormat: "yy-mm-dd",
        // maxDate: 0,
        onSelect: function (d) {
            $scope.OthersData.Maintenance.Date = d;

            if (this.id === "M_Date") {
                // alert(this.id);
                $("#M_ExpiryDate").datepicker("option", "minDate", d);
            }
        }

    });

    //Datepicker Jquery
    $("#M_ExpiryDate").datepicker({
        dateFormat: "yy-mm-dd",
        mindate: 0,
        onSelect: function (d) {
            $scope.OthersData.Maintenance.ExpiryDate = d;
            if (this.id === "M_ExpiryDate") {

                $("#M_Date").datepicker("option", "maxDate", d);

            }


        }

    });

    //Datepicker Jquery
    $("#S_Date").datepicker({
        dateFormat: "yy-mm-dd",
        // maxDate: 0,
        onSelect: function (d) {
            $scope.OthersData.Service.ServiceDate = d;

            if (this.id === "S_Date") {
                // alert(this.id);
                $("#S_ExpiryDate").datepicker("option", "minDate", d);
            }
        }

    });

    //Datepicker Jquery
    $("#S_ExpiryDate").datepicker({
        dateFormat: "yy-mm-dd",
        mindate: 0,
        onSelect: function (d) {
            $scope.OthersData.Service.ExpiryDate = d;
            if (this.id === "S_ExpiryDate") {

                $("#S_Date").datepicker("option", "maxDate", d);

            }


        }

    });

    //Attachment start
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    //removing an item from attachment list
    $scope.RemoveAttachment = function (index, ID) {

        var key = $scope.findObjectIndexByKey($scope.FieldList, 'ID', ID);
        $scope.FieldList[key].AttachmentList.splice(index, 1);

    }

    //adding items to field list array variable in scope
    $scope.UploadAttachment = function (FieldName, LabelName, ID) {

        var k = $scope.findObjectIndexByKey($scope.FieldList, 'ID', ID);

        $scope.FieldList[k].ShowUploadLoader = true;

        var files = $("#" + FieldName).get(0).files;

        if (files.length < 1) {

            alertify.alert('Error', 'Select files for ' + LabelName + ' to upload.', function () {
            });

        } else {

            for (var i = 0; i < files.length; i++) {

                var key = $scope.findObjectIndexByKey($scope.FieldList, 'ID', ID);
                $scope.FieldList[key].AttachmentList.push(files[i]);

            }

            $scope.FieldList[k].ShowUploadLoader = false;

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $("#" + FieldName).val('').clone(true);
        }
    }

    //Attachments
    $scope.UploadAttachmentLoaderStatic = false;

    $scope.AttachmentFilesStatic = [];

    $scope.RemoveAttachmentStatic = function (index) {

        $scope.AttachmentFilesStatic.splice(index, 1);

    }

    $scope.UploadAttachmentsStatic = function () {

        var files = $("#caAttachment").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select files to upload.', function () {
            });
        } else {

            if ((GetAllFilesSizes(files) + GetAllFilesSizes($scope.AttachmentFilesStatic)) > $scope.maxUploadSize) {
                alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
                return false;
            }

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFilesStatic.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#caAttachment').val('').clone(true);
        }

    }

    //Attachments in update
    $scope.RemoveExistingAttachmentStatic = function (index) {

        $scope.ExistingAssetAttachmentList.splice(index, 1);

        console.log('Asset Attachment List');
        console.log($scope.ExistingAssetAttachmentList);

    }

    //Remove Existing Infra Attachments
    $scope.RemoveExistingAttachmentDynamic = function (a, b, c, d) {

        console.log(a);
        console.log(b);
        console.log(c);
        console.log(d);

        $scope.FieldList[c].ExistingAttachmentsList.splice(d, 1);

        //$scope.ExistingAssetAttachmentList.splice(index, 1);

        //$scope.FieldList.filter(item => item.ID === FieldID).map(item => item.AttachmentData).splice(index,1);

        console.log('INDEX');
        console.log($scope.FieldList[c]);

    }

    //NEXT / PREVIOUS IN TAB START
    $scope.SetFormPosition = function () {
        $(window).scrollTop($('#FormSpace').offset().top);
    }

    $scope.ShowTab1Spinner = false;
    $scope.Tab1Text = 'Next';


    $scope.Tab1Next = function (form) {

        $scope.SetFormPosition();

        if ($scope[form].$valid) {

            $scope.ShowTab1Spinner = true;
            //$scope.Tab1Text = 'Validating.. Please wait.';

            console.log($scope.CreateAssetData.BarcodeNumber);
            console.log($scope.SaveType);
            console.log(editAssetID);

            $http.post('../Asset/CheckBarcodeExists',
                {
                    Barcode: $scope.CreateAssetData.BarcodeNumber,
                    SaveType: $scope.SaveType,
                    EncAssetID: editAssetID
                })
                .then(function (cdata) {

                    console.log('BARCODE VALIDATION');
                    console.log(cdata);

                    if (cdata.data.success) {
                        if (!cdata.data.result) {

                            $scope.ShowTab1Spinner = false;
                            //$scope.Tab1Text = 'Next';

                            if ($scope.SetDepreciationMethod == true) {
                                $scope.DeprecationMethodList.forEach(function (x) {
                                    if (x.MethodName == 'Straight Line') {
                                        $scope.CreateAssetData.DepreciationMethodID = x.Id;
                                    }
                                });
                                $scope.SetDepreciationMethod = false;
                            }

                            $scope.showMsgs = false;
                            $scope.Tab1 = false;
                            $scope.Tab2 = true;

                            $('.nav-tabs > .active').next('li').find('a').trigger('click');

                        } else {
                            alertify.alert('Message', 'Barcode already exists. Please enter a new Barcode.', function () { });
                            $scope.ShowTab1Spinner = false;
                            return false;
                        }
                    } else if (cdata.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                    else {
                        alertify.alert('Message', 'Problem Validating Barcode. Please try after sometime.', function () { }); return false;

                        $scope.ShowTab1Spinner = false;
                        //$scope.Tab1Text = 'Next';

                        if ($scope.SetDepreciationMethod == true) {
                            $scope.DeprecationMethodList.forEach(function (x) {
                                if (x.MethodName == 'Straight Line') {
                                    $scope.CreateAssetData.DepreciationMethodID = x.Id;
                                }
                            });
                            $scope.SetDepreciationMethod = false;
                        }

                        $scope.showMsgs = false;
                        $scope.Tab1 = false;
                        $scope.Tab2 = true;

                        $('.nav-tabs > .active').next('li').find('a').trigger('click');

                    }
                });

        } else {
            $scope.showMsgs = true;
        }

    }

    $scope.Tab2Next = function (form) {

        $scope.SetFormPosition();

        console.log($scope.FROM_PROJECT);
        console.log(Number($scope.CreateAssetData.PurchasePrice));
        console.log($scope.TOTAL_PAID);


        if ($scope.FROM_PROJECT == true && Number($scope.CreateAssetData.PurchasePrice) > $scope.TOTAL_PAID) {
            $scope.showTransferAssetPriceError = true;
            $scope.TransferAssetPriceMessage = "Purchase Price for this Transfer Asset must not exceed " + $scope.TOTAL_PAID + ".";
            return false;
        } else {
            $scope.showTransferAssetPriceError = false;
            $scope.TransferAssetPriceMessage = "";
        }

        if (Number($scope.CreateAssetData.PurchasePrice) <= Number($scope.CreateAssetData.ResidualValue)) {
            $scope.showResidualValueCompareError = true;
            $scope.showMsgs = true;
        } else {
            $scope.showResidualValueCompareError = false;
            if ($scope[form].$valid) {

                $scope.showMsgs = false;
                $scope.Tab2 = false;
                $scope.Tab3 = true;

                $('.nav-tabs > .active').next('li').find('a').trigger('click');

            } else {
                $scope.showMsgs = true;

            }
        }


    }

    $scope.Tab3Next = function (form) {

        $scope.SetFormPosition();
        if ($scope[form].$valid) {

            $scope.showMsgs = false;
            $scope.Tab3 = false;
            $scope.Tab4 = true;

            $('.nav-tabs > .active').next('li').find('a').trigger('click');

        } else {
            $scope.showMsgs = true;
        }
    }

    $scope.Tab4Next = function (form) {
        $scope.SetFormPosition();

        if ($scope[form].$valid) {

            $scope.showMsgs = false;
            $scope.Tab5 = false;
            $scope.Tab4 = true;

            $('.nav-tabs > .active').next('li').find('a').trigger('click');

        } else {
            $scope.showMsgs = true;
        }
    }

    $scope.Tab5Next = function (form) {
        $scope.SetFormPosition();
        //var files = $("#caAttachment").get(0).files;

        //    if (files == undefined) {

        //        $('#caAttachment').addClass("error");

        //        $scope.showMsgs = true;

        //    } else if (files.length < 1) {

        //        $('#caAttachment').addClass("error");

        //        $scope.showMsgs = true;

        //    } else {

        //if(GetAllFilesSizes(files) > $scope.maxUploadSize)
        //{
        //    alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
        //    return false;
        //}

        if ($scope.SaveType == 'Add') {
            if ($scope.AttachmentFilesStatic < 1) {
                alertify.alert('Stop', "Please upload attachments.", function () {
                });
                return;
            }
        }

        //VALID
        //$('#caAttachment').removeClass("error");

        $scope.showMsgs = false;
        $scope.Tab6 = true;
        $scope.Tab5 = false;

        $('.nav-tabs > .active').next('li').find('a').trigger('click');

        //}

    }

    //@MCODE@
    $scope.Tab6Next = function (form) {

        $scope.SetFormPosition();

        if ($scope[form].$valid) {

            $scope.showMsgs = false;
            $scope.Tab6 = false;
            $scope.Tab7_1 = false;
            $scope.Tab7_2 = false;
            $scope.Tab7_3 = false;
            $scope.Tab7_4 = false;
            $scope.Tab7_5 = false;

            $('.nav-tabs > .active').next('li').find('a').trigger('click');

        } else {
            $scope.showMsgs = true;
        }
    }
    //@MCODE@


    $scope.Tab2Previous = function (form) {
        $scope.SetFormPosition();
        $scope.showMsgs = false;
        $scope.Tab2 = false;
        $scope.Tab1 = true;
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    }

    $scope.Tab3Previous = function (form) {
        $scope.SetFormPosition();
        $scope.showMsgs = false;
        $scope.Tab3 = false;
        $scope.Tab2 = true;
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    }

    $scope.Tab4Previous = function (form) {
        $scope.SetFormPosition();
        $scope.showMsgs = false;
        $scope.Tab4 = false;
        $scope.Tab3 = true;
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    }

    $scope.Tab5Previous = function (form) {
        $scope.SetFormPosition();
        //$scope.showMsgs = false;
        //$scope.Tab5 = false;
        //$scope.Tab4 = true;
        //$('.nav-tabs > .active').prev('li').find('a').trigger('click');
        $scope.showMsgs = false;
        $scope.Tab4 = false;
        $scope.Tab3 = true;
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    }

    $scope.Tab6Previous = function (form) {
        $scope.SetFormPosition();
        $scope.showMsgs = false;
        $scope.Tab6 = false;
        $scope.Tab5 = true;
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    }

    //@MCODE@
    $scope.Tab7Previous = function (form) {

        $scope.SetFormPosition();
        $scope.showMsgs = false;
        $scope.Tab7_1 = false;
        $scope.Tab7_2 = false;
        $scope.Tab7_3 = false;
        $scope.Tab7_4 = false;
        $scope.Tab7_5 = false;
        $scope.Tab6 = true;
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    }


    //@MCODE@

    //NEXT / PREVIOUS IN TAB END

    //Validate File inputs in Infrastructure tab
    $scope.ValidateInfraFileUploads = function () {

        var FileUploadValid = true;

        $scope.FieldList.forEach(function (item) {

            if (item.FieldType == 'FILE_UPLOAD' && item.AttachmentList.length < 1 && FileUploadValid == true) {
                alertify.alert('Error', "Please upload attachments for " + item.LabelName, function () { });

                FileUploadValid = false;
                $scope.imgLoader_Save = false;

            }

        });
        return FileUploadValid;
    }

    //Validate File inputs in Infrastructure tab for UPDATE
    $scope.ValidateInfraFileUploadsForUpdate = function () {

        var FileUploadValid = true;

        $scope.FieldList.forEach(function (item) {

            if (item.FieldType == 'FILE_UPLOAD' && item.AttachmentList.length < 1 && item.ExistingAttachmentsList.length < 1 && FileUploadValid == true) {
                alertify.alert('Error', "Please upload attachments for " + item.LabelName + ". You have removed existing Attachments for this field.", function () { });

                FileUploadValid = false;
                $scope.imgLoader_Save = false;

            }

        });
        return FileUploadValid;
    }

    // Save Asset
    $scope.SaveAsset = function (CAData, form) {

        var Ival = $scope.Insurance;
        var Wval = $scope.Warranty;
        var Mval = $scope.Maintenance;
        var Sval = $scope.Service;
        var SNval = $scope.SafetyNotes;


        if (Ival == 'Y') {
            $scope.Tab7_1 = true;
        } else {
            $scope.Tab7_1 = false;
        }

        if (Wval == 'Y') {
            $scope.Tab7_2 = true;
        } else {
            $scope.Tab7_2 = false;
        }

        if (Mval == 'Y') {
            $scope.Tab7_3 = true;
        } else {
            $scope.Tab7_3 = false;
        }

        if (Sval == 'Y') {
            $scope.Tab7_4 = true;
        } else {
            $scope.Tab7_4 = false;
        }

        if (SNval == 'Y') {
            $scope.Tab7_5 = true;
        } else {
            $scope.Tab7_5 = false;
        }
        
        $scope.imgLoader_Save = true;

        $scope.CreateAssetData.Location = $('#CompanyAddress').val();

        CAData.ProjectGUID = $scope.ProjectGUID;
        CAData.RegionGUID = $scope.RegionGUID;
        CAData.FundGUID = $scope.FundGUID;
        CAData.ItemGUID = $scope.ItemGUID;
        CAData.CostingGUID = $scope.CostingGUID;

        var parID = '';
        if (ParentAssetID !== undefined) {
            parID = ParentAssetID;
        }

        if ($scope[form].$valid) {

            console.log('VALID>>>>>');


            $scope.showMsgs = false;

            var files = $scope.AttachmentFilesStatic;

            var OthersCheckList = {
                Insurance: Ival == 'Y' ? true : false
                , Warranty: Wval == 'Y' ? true : false
                , Maintenance: Mval == 'Y' ? true : false
                , Service: Sval == 'Y' ? true : false
                , SafetyNotes: SNval == 'Y' ? true : false
            };

            $http({
                method: 'post',
                url: '../Asset/Save',
                headers: { 'Content-Type': undefined },

                transformRequest: function () {
                    formData.append("jsonDataAsset", angular.toJson(CAData));
                    formData.append("jsonDataInfra", angular.toJson($scope.FieldList));
                    formData.append("epid", $scope.EPID);
                    formData.append("parID", parID);

                    formData.append("jsonDataInsurance", angular.toJson($scope.OthersData.Insurance));
                    formData.append("jsonDataWarranty", angular.toJson($scope.OthersData.Warranty));
                    formData.append("jsonDataMaintenance", angular.toJson($scope.OthersData.Maintenance));
                    formData.append("jsonDataService", angular.toJson($scope.OthersData.Service));
                    formData.append("jsonDataSafetyNotes", angular.toJson($scope.OthersData.Notes));
                    formData.append("jsonDataOthersCheckList", angular.toJson(OthersCheckList));

                    for (var i = 0; i < files.length; i++) {
                        formData.append("AssetAttachment", files[i]);
                    }

                    $scope.FieldList.forEach(function (item) {

                        if (item.FieldType == 'FILE_UPLOAD') {

                            for (var i = 0; i < item.AttachmentList.length; i++) {
                                formData.append('F' + item.ID, item.AttachmentList[i]);
                            }

                        }

                    });

                    return formData;
                }

            }).then(function (upData, status, headers, config) {

                $scope.imgLoader_Save = false;

                if (upData.data.success) {

                    alertify.alert('Success', upData.data.success, function () {
                        window.location.href = '../Home/CreateAsset';
                    });

                }
                else if (upData.data.error) {

                    alertify.alert('Error', upData.data.error, function () {
                    });

                }
                else if (upData.data.expire) {

                    window.location.href = '../Home/Dashboard';

                }
            });
        }
        else {

            $scope.showMsgs = true;
            $scope.imgLoader_Save = false;
            console.log('Invalid>>>>')
        }
    }

    // Update Asset
    $scope.UpdateAsset = function (CAData, form) {

        console.log('ASSET UPDATE');
        console.log(CAData);
        //return false;

        $scope.CreateAssetData.Location = $('#CompanyAddress').val();

        CAData.FunctionLevelID = $scope.FunctionLevelID;
        CAData.ProjectGUID = $scope.ProjectGUID;
        CAData.RegionGUID = $scope.RegionGUID;
        CAData.FundGUID = $scope.FundGUID;
        CAData.ItemGUID = $scope.ItemGUID;
        CAData.CostingGUID = $scope.CostingGUID;

        if ($scope[form].$valid) {

            $scope.imgLoader_Save = true;
            $scope.showMsgs = false;

            $http({
                method: 'post',
                url: '../Asset/Update',
                headers: { 'Content-Type': undefined },

                transformRequest: function () {
                    formData.append("jsonData", angular.toJson(CAData));
                    formData.append("jsonDataInfra", angular.toJson($scope.FieldList));
                    formData.append("jsonDataExistingAttachmentList", angular.toJson($scope.ExistingAssetAttachmentList));

                    for (var i = 0; i < $scope.AttachmentFilesStatic.length; i++) {
                        formData.append("AssetAttachment", $scope.AttachmentFilesStatic[i]);
                    }

                    return formData;
                }

            }).then(function (upData, status, headers, config) {

                $scope.imgLoader_Save = false;

                if (upData.data.success) {

                    alertify.alert('Success', upData.data.success, function () {
                        window.location.href = '../Home/ViewAssets';
                    });

                } else if (upData.data.error) {

                    alertify.alert('Error', upData.data.error, function () {
                    });
                } else if (upData.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            });
        } else {

            $scope.showMsgs = true;

            $scope.imgLoader_Save = false;
        }

    }

});

app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('onFinishRenderProject', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('onFinishRenderRegion', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('onFinishRenderFund', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('onFinishRenderItem', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }


    $('html').scrollTop(0);

});

app.directive('onFinishRenderJDP', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('onFinishRenderSetDepreciationMethod', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});
