﻿app.controller('UploadAssetController', function ($scope, $http, $filter) {

    //Attachments
    $scope.UploadAttachmentLoader = false;
    $scope.imgLoader_Save = false;
    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {
        $scope.AttachmentFiles.splice(index, 1);
    }

    $scope.UploadAttachment = function () {

        $scope.AttachmentFiles = [];

        var files = $("#Attachment").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select file to upload.', function () {
            });
        } else {

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachment').val('').clone(true);
        }
    }

    //Save Asset
    $scope.SaveUploadedAssets = function () {

        if ($scope.AttachmentFiles < 1) {
            alertify.alert('Error', "Please upload Assets List.", function () {
            });
            return;
        }

        var formData = new FormData();

        $scope.showMsgs = false;
        $scope.imgLoader_Save = true;

        var files = $scope.AttachmentFiles;

        $http({
            method: 'post',
            url: '../Asset/CreateBulkAssets',
            headers: { 'Content-Type': undefined },
            transformRequest: function () {

                formData.append("UploadedFile", files[0]);

                return formData;
            }
        }).then(function (res) {

            $scope.imgLoader_Save = false;

            console.log(res);

            if (res.data.success) {
                alertify.alert('Success', res.data.success, function () {

                    $scope.AttachmentFiles = [];
                    $('#Attachment').val('').clone(true);

                    $(".downloadLastUploadedResult").get(0).click();

                    //window.location.href = '../Home/UploadAssets';
                });
            }
            else if (res.data.error) {
                window.location.href = '../Home/UploadAssets';
            }
            else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else {
                alertify.alert('Error', res.data.error, function () {
                });
            }

        });
    }

});
