﻿//VERIFY DISPOSAL
app.controller('DisposeAssetCtrl', function ($scope, $http) {

    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.imgLoader_Save1 = false;
    $scope.imgLoader_Save = false;

    $scope.getApproveAssetList = function () {
        $http.get("../DisposeAsset/getApproveAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                       function (response) {
                           //console.log('Pagination')
                           //console.log(response.data.success)
                           if (response.data.expire) {
                               window.location.href = '../Home/Dashboard';
                           } else {
                               $scope.items = response.data.success.employees;
                               $scope.totalCount = response.data.success.totalCount;
                               $scope.showLoader = false;
                               $scope.showTable = true;
                           }
                       },
                       function (err) {
                           var error = err;
                       });

    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "AssetName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getApproveAssetList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getApproveAssetList();
    }
    //Loading employees list on first time
    $scope.getApproveAssetList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getApproveAssetList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getApproveAssetList();
    };



    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    //Modal popup open event
    //Gets selected disposed asset
    $scope.showPopup = function (ID)
    {
        $scope.AssetID = ID;
        $http.post('../DisposeAsset/getSelectedApproveAsset', { DisposeID: ID }).then(function (assetsdata) {

            if (assetsdata.data.success) {
                $scope.AssetDetail = assetsdata.data.success[0];
                console.log($scope.AssetDetail)
                $scope.LoadDisposedAssetAttachmentsList(ID);

                //$('.AssetDescription').trigger('click');
            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (assetsdata.data.error) {

            }

        });
    }

    //Gets attachments of disposed asset
    $scope.LoadDisposedAssetAttachmentsList = function (DisposeID) {
        $http.post('../DisposeAsset/getAttachments', { DisposeID: DisposeID }).then(function (attachments) {

            if (attachments.data.success) {
                $scope.AttachmentList = attachments.data.success;
                console.log($scope.AttachmentList)


            } else if (attachments.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (attachments.data.error) {

            }

        });
    }


    //Set Approve/Reject to disposed asset
    $scope.ApproveRejectTab = true;


    $scope.SignedCouncilResolutionAttached = 'Yes';
    $scope.SignedAssetDisposalFormAttached = 'Yes';


    $scope.DisposeApproveReject = function (id, status, Commentary, UserPIN) {

        if (Commentary != undefined && UserPIN != undefined) {
    
            if (status == "Approve") {
                $scope.imgLoader_Save = true;
                $scope.imgLoader_Save1 = false;
            }
            else {
                $scope.imgLoader_Save1 = true;
                $scope.imgLoader_Save = false;
            }
            $scope.rejectMsg = false;

            var parameters = {
                AssetID: $scope.AssetID,
                Commentary: Commentary,
                Status: status,
                UserPIN: UserPIN,
                SignedCouncilResolutionAttached: $scope.SignedCouncilResolutionAttached,
                SignedAssetDisposalFormAttached: $scope.SignedAssetDisposalFormAttached,
                SignedCouncilResolutionAttachedCommentary: $scope.SignedCouncilResolutionAttached == 'No' ? $scope.SignedCouncilResolutionAttachedCommentary : null,
                SignedAssetDisposalFormAttachedCommentary: $scope.SignedAssetDisposalFormAttached == 'No' ? $scope.SignedAssetDisposalFormAttachedCommentary : null
            }

            $http.get('../DisposeAsset/ApproveRejectAsset', { params: parameters }).then(function (res) {
                $scope.imgLoader_Save1 = false;
                $scope.imgLoader_Save = false;
                if (res.data.success) {

                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Home/ApproveAssets';
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }
            });

        } else {
            $scope.rejectMsg = true;
        }
    }

});
