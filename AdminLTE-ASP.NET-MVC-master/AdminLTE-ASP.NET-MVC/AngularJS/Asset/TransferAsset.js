﻿app.controller('TransferAssetCtrl', function ($scope, $http) {

    $scope.transferAssetData = {};
    $scope.transferAssetData.CustodianID = null;
    $scope.PanelLocation = false;
    $scope.PanelCustodian = false;

    //loading employees list in modal popup
    $http.post('../Asset/getEmployees').then(function (reasons) {

        if (reasons.data.success) {
            $scope.Employees = reasons.data.success;
        } else if (reasons.data.error) {
            alertify.alert('Error', "Problem loading employees.", function () { });
        } else if (reasons.data.expire) {
            window.location.href = '../Home/Dashboard';
        }

    });

    //Gets Rooms
    $http.post('../Asset/GetBuildingWiseRooms').then(function (cdata) {
        if (cdata.data.success) {
            $scope.RoomsList = cdata.data.success;
        } else if (cdata.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
        else if (cdata.data.error) {

        }
    });

    $scope.RoomSelected = function (Room) {
        console.log("CREATE ASSET DATA");
        console.log($scope.transferAssetData);
        $scope.SelectedRoomLabel = '';
        $scope.SelectedRoomLabel = 'Barcode: ' + Room.RoomBarcode + ', Room: ' + Room.RoomName;
        $scope.transferAssetData.RoomID = Room.RoomID;
        $scope.transferAssetData.BuildingName = Room.BuildingName;
        $scope.transferAssetData.BuildingFloor = Room.RoomFloor;
        $scope.transferAssetData.Location = Room.BuildingLocation;
        $scope.transferAssetData.Site = Room.BuildingSite;

        $("#modalRooms .close").click();
    }

    $scope.SearchRoom = function () {

        $scope.RoomsList = null;
        $scope.LoadingRooms = true;
        $http.post('../Asset/GetBuildingWiseRooms', { Keyword: $scope.RoomSearchKeyword }).then(function (cdata) {

            if (cdata.data.success) {
                $scope.LoadingRooms = false;
                $scope.RoomsList = cdata.data.success;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
            else if (cdata.data.error) {
                $scope.LoadingRooms = false;
            }
        });

    }


    //Employee select start
    $scope.SearchEmployee = function () {
        $scope.EmployeesList = null;
        $scope.LoadingEmployees = true;
        $http.post('../Asset/getEmployees', { Keyword: $scope.EmployeeSearchKeyword }).then(function (cdata) {

            if (cdata.data.success) {
                $scope.LoadingEmployees = false;
                $scope.EmployeesList = cdata.data.success;
            }
            else if (cdata.data.error) {
                $scope.LoadingEmployees = false;
            } else if (cdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }
        });

    }

    $scope.EmployeeSelected = function (EmployeeID, EmployeeName) {
        $scope.SelectedEmployeeLabel = '';
        $scope.SelectedEmployeeLabel = EmployeeName;
        $scope.transferAssetData.CustodianID = EmployeeID;
        $("#modalEmployees .close").click();
    }

    $scope.SetCapturePanel = function () {

        if ($scope.transferAssetData.ChangeTo == "Location") {
            $scope.PanelLocation = true;
            $scope.PanelCustodian = false;
        } else if ($scope.transferAssetData.ChangeTo == "Custodian") {
            $scope.PanelCustodian = true;
            $scope.PanelLocation = false;
        } else {
            $scope.PanelLocation = false;
            $scope.PanelCustodian = false;
        }
    }

    $scope.classDisable = false;
    $scope.dTable = true;
    $scope.dForm = false;
    $scope.dPrint = false;
    $scope.imgLoader_Save = false;

    //Validation purpose
    $scope.capturePanel = true;

    //Document Upload
    var scannedDocsFiles = [];
    var formData = new FormData();
    var Uploadedfiles = [];
    var isTrue = false;


    //Show Transfer Form
    $scope.showTransferForm = function (id) {
        $scope.dForm = true;
        $scope.dTable = false;
        $scope.dPrint = false;


        //Gets Selected Asset
        $scope.transferAssetData.AssetID = id;

        $http.post('../TransferAsset/getSelectedTransferAsset', { assetId: id }).then(function (assetsdata) {

            if (assetsdata.data.success) {
                $scope.AssetDetail = assetsdata.data.success[0];
                //console.log('$scope.AssetDetail');
                //console.log($scope.AssetDetail);
                $('.AssetDescription').trigger('click');
            } else if (assetsdata.data.error) {
                alertify.alert('Error', assetsdata.data.error, function () {
                });
            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    //Show Transfer Table
    $scope.showAssetsTable = function () {
        $scope.dForm = false;
        $scope.dTable = true;
        $scope.dPrint = false;
    }

    //Gets All Asset
    //alert('ok')
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;

    //loading transfer assets
    $scope.getTransferAssetsList = function () {
        $http.get("../TransferAsset/getTransferAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                           function (response) {
                               if (response.data.expire) {
                                   window.location.href = '../Home/Dashboard';
                               } else {
                                   $scope.items = response.data.success.employees;
                                   $scope.totalCount = response.data.success.totalCount;
                                   $scope.showLoader = false;
                                   $scope.showTable = true;
                               }
                           },
                           function (err) {
                               var error = err;
                           });
    }


    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "AssetID";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getTransferAssetsList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getTransferAssetsList();
    }
    //Loading employees list on first time
    $scope.getTransferAssetsList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getTransferAssetsList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getTransferAssetsList();
    };
    


    //Attachments
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {

        $scope.AttachmentFiles.splice(index, 1);

    }

    $scope.UploadAttachments = function () {

        var files = $("#TAAttachments").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select files to upload.', function () {
            });
        } else {

            if ((GetAllFilesSizes(files) + GetAllFilesSizes($scope.AttachmentFiles)) > $scope.maxUploadSize) {
                alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
                return false;
            }

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#TAAttachments').val('').clone(true);
        }

    }



    //Save Transfer Asset
    $scope.saveTransferAsset = function (form) {
        var formData = new FormData();

        //Verify attachments
        var files = $scope.AttachmentFiles;

        if ($scope.AttachmentFiles < 1) {
            alertify.alert('Error', "Upload Attachments to transfer asset.", function () {
            });
            return;
        }
                
        $scope.transferAssetData.Location = $('#NewLocation').val();


        if ($scope[form].$valid) {
            
            $scope.showMsgs = false;
          
                $scope.imgLoader_Save = true;

                $http({
                    method: 'post',
                    url: '../TransferAsset/AssetTransfer',
                    headers: { 'Content-Type': undefined },

                    transformRequest: function () {
                        formData.append("TransferAssetData", angular.toJson($scope.transferAssetData));

                        for (var i = 0; i < files.length; i++) {
                            formData.append("UploadedImage", files[i]);
                        }

                        return formData;
                    }
                }).then(function (upData, status, headers, config) {
                    console.log(upData.data)
                    $scope.imgLoader_Save = false;
                    if (upData.data.success) {

                        alertify.alert('Success', upData.data.success, function () {
                            window.location.href = '../Home/TransferAsset';
                        });
                    } else if (upData.data.error) {
                        alertify.alert('Error', upData.data.error, function () {
                            window.location.href = '../Home/TransferAsset';
                        });
                    } else if (upData.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                });
            }

        
        else {
            $scope.showMsgs = true;
           
        }

    }

    //Print Transfer Asset
    $scope.printPreviewTransferAsset = function (form) {

        if ($scope[form].$valid) {

            $scope.showMsgs = false;

            $scope.LocationFormed = "Site: " + ($scope.transferAssetData.Site == undefined ? "" :$scope.transferAssetData.Site)
                + ", Building: " + ($scope.transferAssetData.BuildingName == undefined ? "" : $scope.transferAssetData.BuildingName)
                + ", Floor: " + ($scope.transferAssetData.BuildingFloor == undefined ? "" :$scope.transferAssetData.BuildingFloor)
                + ", Room: " + ($scope.transferAssetData.BuildingRoom == undefined ? "" : $scope.transferAssetData.BuildingRoom)
                + ", Location: " + $("#NewLocation").val();

            $scope.dForm = false;
            $scope.dTable = false;
            $scope.dPrint = true;

            $scope.date = new Date();

        }
        else {
            $scope.showMsgs = true;
        }

    }

    $scope.goBackTransferAssetForm = function () {

        $scope.dForm = true;
        $scope.dTable = false;
        $scope.dPrint = false;
    }

    $scope.Transferexport = function () {
        //alert('hi')
        html2canvas(document.getElementById('exportthis'), {

            onrendered: function (canvas) {

                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Asset_Transfer_Form.pdf");
            }
        });

    }
    

});