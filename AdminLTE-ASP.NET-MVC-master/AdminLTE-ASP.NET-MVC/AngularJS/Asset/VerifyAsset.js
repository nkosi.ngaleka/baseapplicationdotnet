﻿app.controller('VerifyAssetCtrl', function ($scope, $http) {

    $scope.VerifyAssetData = {};
    //$scope.verifyAssetData.CustodianID = null;

    $scope.classDisable = false;
    $scope.dTable = true;
    $scope.dForm = false;
    
    $scope.imgLoader_Save = false;

    $scope.AssetConditions = [];

    $http.post('../Asset/getConditions').then(function (cdata) {

        if (cdata.data.success) {
            $scope.AssetConditions = cdata.data.success;
        } else if (cdata.data.error) {
            alertify.alert('Error', cdata.data.error, function () {
            });
        } else if (cdata.data.expire) {
            window.location.href = '../Home/Dashboard';
        }

    });

    //Validation purpose
    $scope.capturePanel = true;

    //Document Upload
    var scannedDocsFiles = [];
    var formData = new FormData();
    var Uploadedfiles = [];
    var isTrue = false;

    $scope.FieldList = [];

    $scope.showPopup = function (ID, BarcodeNumber) {
        $scope.AssetID = ID;
        $scope.BarcodeNumber = BarcodeNumber;

        $scope.FieldList = [];

        $http.post('../Asset/getSelectedVerifyAsset', { assetId: ID }).then(function (assetsdata) {
            console.log('INF');
            console.log(assetsdata);
            
            if (assetsdata.data.success) {

                $scope.AssetDetail = assetsdata.data.AssetData[0];
                $scope.LastReviewDetails = assetsdata.data.LastReviewDetails;

                if (assetsdata.data.InfraData.success) {

                    var a = [], b = [], c = [];

                    a = assetsdata.data.InfraData.TextPlusDate;
                    b = assetsdata.data.InfraData.Dropdown;
                    c = assetsdata.data.InfraData.FileUpload;

                    $scope.InfraDataTextPlusDate = assetsdata.data.InfraData.TextPlusDate;
                    $scope.InfraDataDropdown = assetsdata.data.InfraData.Dropdown;
                    $scope.InfraDataFileUpload = $scope.ArrayGroupByKey(assetsdata.data.InfraData.FileUpload);
                    //assetsdata.data.InfraData.FileUpload;
                    //$scope.InfraDataFileUpload1 = $scope.ArrayGroupByKey(assetsdata.data.InfraData.FileUpload);
                    console.log('FUPLOAD');
                    console.log($scope.InfraDataFileUpload);

                    if (a.length == 0 && a.length == 0 && a.length == 0) {
                        $scope.NoInfraData = true;
                    } else {
                        $scope.NoInfraData = false;
                    }

                } else {
                    $scope.InfraDataTextPlusDate = [];
                    $scope.InfraDataDropdown = [];
                    $scope.InfraDataFileUpload = [];

                    $scope.InfraError = true;
                    $scope.InfraErrorMessage = "Problem showing infrastructure data.";
                }


                // Generating infra form
                var IData1 = assetsdata.data.InfrastructureFieldsWithData;
                $scope.HasInfrastructure = assetsdata.data.HasInfrastructure;

                if ($scope.HasInfrastructure == true) {

                    for (var i = 0; i < IData1.length; i++) {

                        $scope.FieldList.push({
                            ID: IData1[i].ID,
                            UploadID: 'U' + IData1[i].ID,
                            ShowUploadLoader: false,
                            AttachmentList: [],
                            ModelName: IData1[i].FieldType == 'DROPDOWN' ? parseInt(IData1[i].Value) : IData1[i].Value,
                            FieldName: 'F' + IData1[i].ID,
                            LabelName: IData1[i].FieldName,
                            FieldType: IData1[i].FieldType,
                            FieldRequired: IData1[i].Required,
                            DDList: IData1[i].DropdownList,
                            FieldValue: IData1[i].Value,
                            ExistingAttachmentsList: IData1[i].AttachmentData,
                            EditableField: IData1[i].EditableField
                        });

                    }

                }

                console.log('$scope.FieldList')
                console.log($scope.FieldList)
             

            } else if (assetsdata.data.error) {
                alertify.alert('Error', assetsdata.data.error, function () {
                });
            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    $scope.ArrayGroupByKey = function (items) {

        var groups = items.reduce(function (obj, item) {
            obj[item.Key] = obj[item.Key] || [];
            obj[item.Key].push(item.Value);
            return obj;
        }, {});
        var myArray = Object.keys(groups).map(function (key) {
            return { Key: key, Value: groups[key] };
        });
        console.log(myArray);
        return myArray;
    }

    //Gets attachments of disposed asset
    //$scope.LoadDisposedAssetAttachmentsList = function (AssetID) {
    //    $http.post('../Asset/getAttachments', { AssetID: AssetID }).then(function (attachments) {

    //        if (attachments.data.success) {
    //            $scope.AttachmentList = attachments.data.success;
    //            console.log($scope.AttachmentList)


    //        }
    //        else if (attachments.data.error) {

    //        }

    //    });
    //}
  

    //Gets All Asset
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;

    //loading transfer assets
    $scope.getVerifyAssetsList = function () {
        $http.get("../Asset/getVerifyAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                           function (response) {
                               if (response.data.expire) {
                                   window.location.href = '../Home/Dashboard';
                               } else {
                                   $scope.items = response.data.success.employees;
                                   $scope.totalCount = response.data.success.totalCount;
                                   $scope.showLoader = false;
                                   $scope.showTable = true;
                               }
                           },
                           function (err) {
                               var error = err;
                           });
    }

    //Sorting 
    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "AssetName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getVerifyAssetsList();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getVerifyAssetsList();
    }
    //Loading employees list on first time
    $scope.getVerifyAssetsList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getVerifyAssetsList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getVerifyAssetsList();
    };
    
    //Attachments
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {

        $scope.AttachmentFiles.splice(index, 1);

    }

    $scope.UploadAttachments = function () {

        var files = $("#Attachments").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select files to upload.', function () {
            });
        } else {

            if ((GetAllFilesSizes(files) + GetAllFilesSizes($scope.AttachmentFiles)) > $scope.maxUploadSize) {
                alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
                return false;
            }

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachments').val('').clone(true);
        }

    }


    //Save Verify Asset
    $scope.SaveVerifyAsset = function (form) {
        var formData = new FormData();

        //Verify attachments
        var files = $scope.AttachmentFiles;

        if ($scope.AttachmentFiles < 1) {
            alertify.alert('Error', "Upload Attachments to transfer asset.", function () {
            });
            return;
        }

        if ($scope[form].$valid) {
            
            console.log('on save infra data');
            console.log($scope.FieldList);
            //return false;

            $scope.showMsgs = false;
          
                $scope.imgLoader_Save = true;

                $http({
                    method: 'post',
                    url: '../Asset/AssetVerify',
                    headers: { 'Content-Type': undefined },

                    transformRequest: function () {
                        formData.append("VerifyAssetData", angular.toJson($scope.VerifyAssetData));
                        formData.append("UserPIN", angular.toJson($scope.UserPIN));
                        formData.append("AssetID", angular.toJson($scope.AssetID));
                        formData.append("BarcodeNumber", angular.toJson($scope.BarcodeNumber));
                        formData.append("jsonDataInfra", angular.toJson($scope.FieldList));

                        for (var i = 0; i < files.length; i++) {
                            formData.append("UploadedImage", files[i]);
                        }

                        return formData;
                    }
                }).then(function (upData, status, headers, config) {
                    console.log(upData.data)
                    $scope.imgLoader_Save = false;
                    if (upData.data.success) {

                        alertify.alert('Success', upData.data.success, function () {
                            window.location.href = '../Home/AssetReview';
                        });
                    } else if (upData.data.error) {
                        alertify.alert('Error', upData.data.error, function () {
                        });
                    } else if (upData.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                });
            }

        else {
            $scope.showMsgs = true;
        }

    }

    $scope.goBackVerifyAssetForm = function () {

        $scope.dForm = true;
        $scope.dTable = false;
        
    }

});
