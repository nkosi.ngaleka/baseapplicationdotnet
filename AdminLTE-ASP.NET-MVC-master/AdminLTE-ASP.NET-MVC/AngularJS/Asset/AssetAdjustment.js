﻿app.controller('AssetAdjustmentCtrl', function ($scope, $http) {

    $scope.AssetAdjustmentData = {};
    //$scope.AssetAdjustmentData.CustodianID = null;

    $scope.classDisable = false;
    $scope.dTable = true;
    $scope.dForm = false;
    
    $scope.imgLoader_Save = false;

    //Validation purpose
    $scope.capturePanel = true;

    //Document Upload
    var scannedDocsFiles = [];
    var formData = new FormData();
    var Uploadedfiles = [];
    var isTrue = false;

    $scope.showPopup = function (ID, BarcodeNumber) {
        $scope.AssetID = ID;
        $scope.BarcodeNumber = BarcodeNumber;
        $http.post('../Asset/getSelectedAssetAdjustment', { assetId: ID }).then(function (assetsdata) {

            if (assetsdata.data.success) {
                $scope.AssetDetail = assetsdata.data.AssetData[0];
                //$scope.LastReviewDetails = assetsdata.data.LastReviewDetails;
                //$('.AssetDescription').trigger('click');
                console.log(assetsdata);
                //console.log($scope.LastReviewDetails);
            } else if (assetsdata.data.error) {
                alertify.alert('Error', assetsdata.data.error, function () {
                });
            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    //Gets All Asset
    //alert('ok')
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;

    //loading assets
    $scope.getAssetAdjustmentList = function () {
        $http.get("../Asset/getAdjustmentAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                           function (response) {
                               if (response.data.expire) {
                                   window.location.href = '../Home/Dashboard';
                               } else {
                                   $scope.items = response.data.success.employees;
                                   $scope.totalCount = response.data.success.totalCount;
                                   $scope.showLoader = false;
                                   $scope.showTable = true;
                               }
                           },
                           function (err) {
                               var error = err;
                           });
    }

    //Sorting 
    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "AssetName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getAssetAdjustmentList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        $scope.search = val;
        $scope.getAssetAdjustmentList();
    }
    //Loading employees list on first time
    $scope.getAssetAdjustmentList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getAssetAdjustmentList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getAssetAdjustmentList();
    };
    


    //Attachments
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {

        $scope.AttachmentFiles.splice(index, 1);

    }

    $scope.UploadAttachments = function () {

        var files = $("#Attachments").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select files to upload.', function () {
            });
        } else {


            if ((GetAllFilesSizes(files) + GetAllFilesSizes($scope.AttachmentFiles)) > $scope.maxUploadSize) {
                alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
                return false;
            }

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachments').val('').clone(true);
        }

    }


    //Save Verify Asset
    $scope.SaveAssetAdjustment = function (form) {
        var formData = new FormData();

        //Verify attachments
        var files = $scope.AttachmentFiles;

        if ($scope.AttachmentFiles < 1) {
            alertify.alert('Error', "Upload Attachments to adjust asset.", function () {
            });
            return false;
        }

        if ($scope[form].$valid) {
            
            $scope.showMsgs = false;
          
                $scope.imgLoader_Save = true;

                $http({
                    method: 'post',
                    url: '../Asset/AssetAdjustment',
                    headers: { 'Content-Type': undefined },

                    transformRequest: function () {
                        formData.append("AssetAdjustmentData", angular.toJson($scope.AssetAdjustmentData));
                        formData.append("UserPIN", angular.toJson($scope.UserPIN));
                        formData.append("AssetID", angular.toJson($scope.AssetID));
                        formData.append("BarcodeNumber", angular.toJson($scope.BarcodeNumber));

                        for (var i = 0; i < files.length; i++) {
                            formData.append("UploadedImage", files[i]);
                        }

                        return formData;
                    }
                }).then(function (upData, status, headers, config) {
                    console.log(upData.data)
                    $scope.imgLoader_Save = false;
                    if (upData.data.success) {

                        alertify.alert('Success', upData.data.success, function () {
                            window.location.href = '../Home/AssetAdjustment';
                        });
                    } else if (upData.data.error) {
                        alertify.alert('Error', upData.data.error, function () {
                        });
                    } else if (upData.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                });
            }

        
        else {
            $scope.showMsgs = true;
        }

    }

    $scope.goBackVerifyAssetForm = function () {

        $scope.dForm = true;
        $scope.dTable = false;
        
    }

});