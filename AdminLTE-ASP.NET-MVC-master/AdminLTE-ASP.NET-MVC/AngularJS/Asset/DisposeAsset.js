﻿app.controller('DisposeAssetCtrl', function ($scope, $filter, $http) {

    $scope.classDisable = false;
    $scope.dTable = true;
    $scope.dForm = false;
    $scope.dPrint = false;
    $scope.imgLoader_Save = false;
    $scope.DivOtherReason = false;
    //Validation purpose
    $scope.capturePanel = true;
    
    //Document Upload
    var scannedDocsFiles = [];
    var formData = new FormData();
    var Uploadedfiles = [];
    var isTrue = false;

    //Datepicker Jquery
    $(".jdp").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function (d) {
            $scope.AssetDetail.DisposalDate = d;
        }
     
    });

    //Gets Reason for disposal
    $http.post('../DisposeAsset/getDisposeReasons').then(function (reasons) {
        if (reasons.data.success) {
            $scope.DisposeReasons = reasons.data.success;

            //console.log($scope.DisposalReasons);
        } else if (reasons.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
    });

    //Gets Means for disposal
    $http.post('../DisposeAsset/getDisposeMeans').then(function (reasons) {
        if (reasons.data.success) {
            $scope.DisposeMeans = reasons.data.success;

            //console.log($scope.DisposalMeans);
        } else if (reasons.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
    });

    //Show Dispose Form
    $scope.showDisposeForm = function (id) {
        $scope.dForm = true;
        $scope.dTable = false;
        $scope.dPrint = false;

        //Gets Selected Asset
        $http.post('../DisposeAsset/getSelectedAsset', { assetId: id }).then(function (assetsdata) {

            if (assetsdata.data.success) {
                $scope.AssetDetail = assetsdata.data.success[0];
               
                console.log($scope.AssetDetail);
                $('.AssetDescription').trigger('click');
            } else if (assetsdata.data.error) {
               
                alertify.alert('Error', assetsdata.data.error, function () {
                });
            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }


        });
    }

    //Show Dispose Table
    $scope.showAssetsTable = function () {
        $scope.dForm = false;
        $scope.dTable = true;
        $scope.dPrint = false;
    }
    
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.getDiposeAssetList = function () {
        $http.get("../DisposeAsset/getAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                      function (response) {
                          if (response.data.expire) {
                              window.location.href = '../Home/Dashboard';
                          } else {
                              $scope.items = response.data.success.employees;
                              $scope.totalCount = response.data.success.totalCount;
                              $scope.showLoader = false;
                              $scope.showTable = true;
                          }
                      },
                      function (err) {
                          var error = err;
                      });
    }


   //Sorting 

   // sort ordering (Ascending or Descending). Set true for desending
   $scope.reverse = false;
   $scope.orderType = "ASC";
   $scope.column = "AssetName";
   $scope.sort = function (val) {
       $scope.column = val;
       if ($scope.reverse) {
           $scope.reverse = false;
           $scope.reverseclass = 'arrow-up';
           $scope.orderType = "DESC";
       } else {
           $scope.reverse = true;
           $scope.reverseclass = 'arrow-down';
           $scope.orderType = "ASC";
       }
       //alert(val)
       $scope.sorting = val;
       //alert($scope.sorting)
       $scope.getDiposeAssetList();
   }

   // remove and change class
   $scope.sortClass = function (col) {
       if ($scope.column == col) {
           if ($scope.reverse) {
               return 'arrow-down';
           } else {
               return 'arrow-up';
           }
       } else {
           return '';
       }
   }

   //Search Method
   $scope.SerachMethod = function (val) {
       $scope.search = val;
       $scope.getDiposeAssetList();
   }
   //Loading employees list on first time
   $scope.getDiposeAssetList();

   //This method is calling from pagination number
   $scope.pageChanged = function () {
       $scope.getDiposeAssetList();
   };

   //This method is calling from dropDown
   $scope.changePageSize = function () {
       $scope.pageIndex = 1;
       $scope.getDiposeAssetList();
   };
    //PDF
    $scope.export = function () {
        html2canvas(document.getElementById('exportthis'), {

            onrendered: function (canvas) {

                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Asset_Disposal_Form.pdf");               
            }
        });

    }

    //$http.post('../DisposeAsset/getAssets').then(function (assetsdata) {
    //    //alert('hi')

    //    //console.log(assetsdata.data.success);
    //    if (assetsdata.data.success) {
    //        $scope.AssetsList = assetsdata.data.success;
    //        $scope.AssetsList.forEach(function (a) {
    //            AssetsListArray.push({
    //                AssetName: a.AssetName,
    //                BarcodeNumber: a.BarcodeNumber,
    //                Location: a.Location,
    //                PurchaseDate: a.PurchaseDate,
    //                AssetID: a.AssetID,
    //                DateCaptured: a.DateCaptured
    //            })
    //            $scope.showLoader = false;
    //            $scope.showTable = true;
    //        });
    //        $scope.AssetsListData = AssetsListArray;
    //        $scope.countAssets = AssetsListArray.length;

    //       // alert(AssetsListArray.length)
    //        $scope.items = AssetsListArray;

    //        var searchMatch = function (haystack, needle) {
    //            if (!needle) {
    //                return true;
    //            }
    //            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    //        };

    //        // init the filtered items
    //        $scope.search = function () {
                
    //            $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                    
                    
    //                for (var attr in item) {
                        
    //                    if (searchMatch(item[attr], $scope.query))
    //                        return true;
    //                }
    //                return false;
    //            });
    //            // take care of the sorting order
    //            if ($scope.sort.sortingOrder !== '') {
                    
    //                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sort.sortingOrder, $scope.sort.reverse);
                    
    //            }
    //            $scope.currentPage = 0;
    //            // now group by pages
    //            $scope.groupToPages();
    //        };


    //        //$scope.search = function (item) {
    //        //    //alert(item.AssetName)
    //        //    if (!$scope.query || (item.AssetName.toLowerCase().indexOf($scope.query) != -1) || (item.BarcodeNumber.toLowerCase().indexOf($scope.query.toLowerCase()) != -1) || (item.Location.toLowerCase().indexOf($scope.query) != -1) || (item.AssetID.toLowerCase().indexOf($scope.query) != -1) || (item.TEAM_MEMBER_FIRST_NAME.toLowerCase().indexOf($scope.query) != -1)) {
    //        //        return true;
    //        //    }
    //        //    return false;
    //        //};


    //        // calculate page in place
    //        $scope.groupToPages = function () {
    //            $scope.pagedItems = [];

    //            for (var i = 0; i < $scope.filteredItems.length; i++) {
    //                if (i % $scope.itemsPerPage === 0) {
    //                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
    //                } else {
    //                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
    //                }
    //            }
    //        };

    //        $scope.range = function (size, start, end) {
    //            var ret = [];
    //            console.log(size, start, end);

    //            if (size < end) {
    //                end = size;
    //                start = size - $scope.gap;
    //            }
    //            for (var i = start; i < end; i++) {
    //                ret.push(i);
    //            }
    //            console.log(ret);
    //            return ret;
    //        };

    //        $scope.prevPage = function () {
    //            if ($scope.currentPage > 0) {
    //                $scope.currentPage--;
    //            }
    //        };

    //        $scope.nextPage = function () {
    //            if ($scope.currentPage < $scope.pagedItems.length - 1) {
    //                $scope.currentPage++;
    //            }
    //        };
    //        //$scope.First = function () {
    //        //    if ($scope.currentPage > 0) {
    //        //        $scope.currentPage = 0;
    //        //    }
    //        //};
    //        //$scope.Last = function () {
    //        //    if ($scope.currentPage < $scope.totalpages()) {
    //        //        $scope.currentPage = $scope.totalpages()-1;
    //        //    }
    //        //};

    //        $scope.setPage = function () {
    //            $scope.currentPage = this.n;
    //        };

    //        // functions have been describe process the data for display
    //        $scope.search();
            
    //    }
    //    else if (assetsdata.data.error) {

    //    }
    //});

    //Gets Classifications
    $http.post('../DisposeAsset/getClassifications').then(function (cdata) {
       
        if (cdata.data.success)
        {
            $scope.ClassificationList = cdata.data.success;
        } else if (cdata.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
       
    });

    //$scope.disposeAssetData = { Id: 2, Description: 'A' };

    //On Change of Classification
    $scope.classificationChange = function (value) {

        //if (value == undefined) {
        //    $scope.classDisable = true;
            
        //} else {
        //    $scope.classDisable = false;

        //}

    }

    //Print Dispose Asset
    $scope.printPreviewDisposeAsset = function (form) {
        $('#Attachment').removeClass("error");
        if ($scope[form].$valid) {
         
            $scope.showMsgs = false;

            $scope.dForm = false;
            $scope.dTable = false;
            $scope.dPrint = true;

            $scope.date = new Date();
            $scope.DisposeReasons.forEach(function (d) {
                if (d.ID == $scope.AssetDetail.ReasonForDisposal) {
                    $scope.pdfREASON = d.Reason;
                }
                if ($('#ReasonForDisposal option:selected').html() == 'Other') {
                    $scope.pdfREASON = $scope.AssetDetail.OtherReason;
                }
            });


            //$scope.AssetDetail.ReasonForDisposal
            $scope.DisposeMeans.forEach(function (d) {
                if (d.ID == $scope.AssetDetail.MeansOfDisposal) {
                    $scope.pdfMeans = d.Mean;
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }

    }
    


    Uploadedfiles = [];

    $scope.DisposeReasonChanged = function () {

        //if ($scope.AssetDetail.ReasonForDisposal == '6') {
        //    $scope.DivOtherReason = true;
        //} else {
        //    $scope.DivOtherReason = false;
        //}

        if ($('#ReasonForDisposal option:selected').html() == 'Other') {
            $scope.DivOtherReason = true;
        } else {
            $scope.DivOtherReason = false;
        }

    }


    //Attachments
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {

        $scope.AttachmentFiles.splice(index, 1);

    }

    $scope.UploadAttachments = function () {

        var files = $("#Attachment").get(0).files;

        if (files.length < 1) {

            alertify.alert('Error', 'Select files to upload.', function () {
            });

        } else {

            if ((GetAllFilesSizes(files) + GetAllFilesSizes($scope.AttachmentFiles)) > $scope.maxUploadSize) {
                alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
                return false;
            }

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachment').val('').clone(true);
        }

    }



    //Save Dispose Asset
    $scope.saveDisposeAsset = function (form) {

        //var files = $("#Attachment").get(0).files;


        //Verify attachments
        var files = $scope.AttachmentFiles;

        if ($scope.AttachmentFiles < 1) {
            alertify.alert('Error', "Upload Attachments to dispose asset.", function () {
            });
            return false;
        }

        if ($scope[form].$valid) {
            
            $scope.showMsgs = false;

            //if (files.length < 1) {
            //    $('#Attachment').addClass("error");
            //} else if ($('#ReasonForDisposal option:selected').html() == 'Other' && $scope.AssetDetail.OtherReason == '') {
            //    $('#OtherReason').addClass("error");
            //} else {
            //    $('#Attachment').removeClass("error");
                $scope.imgLoader_Save = true;
               
                var ArrayDisposeAsset = {
                    AssetID: $scope.AssetDetail.AssetID,
                    ReasonForDisposal: $scope.AssetDetail.ReasonForDisposal,
                    MeansOfDisposal: $scope.AssetDetail.MeansOfDisposal,
                    DisposalDate: $scope.AssetDetail.DisposalDate,
                    Commentary: $scope.AssetDetail.Commentary
                };

                if ($('#ReasonForDisposal option:selected').html() == 'Other') {
                    ArrayDisposeAsset.OtherReason = $scope.AssetDetail.OtherReason;
                }

                $http({
                    method: 'post',
                    url: '../DisposeAsset/AssetDispose',
                    headers: { 'Content-Type': undefined },

                    transformRequest: function () {
                        formData.append("jsonData", angular.toJson(ArrayDisposeAsset));

                        for (var i = 0; i < files.length; i++) {
                            formData.append("UploadedImage", files[i]);
                        }

                        //formData.append("UploadedImage", files[0]);

                        return formData;
                    },
                    data: { jsonData: ArrayDisposeAsset }
                }).then(function (upData, status, headers, config) {
                    //console.log(upData.data)
                    $scope.imgLoader_Save = false;
                    if (upData.data.success) {

                        alertify.alert('Success', upData.data.success, function () {
                            window.location.href = '../Home/DisposeAssets';
                        });
                    } else if (upData.data.error) {
                        alertify.alert('Error', upData.data.error, function () {
                            window.location.href = '../Home/DisposeAssets';
                        });
                    } else if (upData.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                });

            //}
        }
        else {
            $scope.showMsgs = true;

            //if (files.length < 1) {
            //    $('#Attachment').addClass("error");
            //} else {
            //    $('#Attachment').removeClass("error");
            //}
            //if ($('#ReasonForDisposal option:selected').html() == 'Other' && $scope.AssetDetail.OtherReason == '') {
            //    $('#OtherReason').addClass("error");
            //} else {
            //    $('#OtherReason').removeClass("error");
            //}
        }

    }

    $scope.goBackDisposeAssetForm = function () {

        $scope.dForm = true;
        $scope.dTable = false;
        $scope.dPrint = false;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



