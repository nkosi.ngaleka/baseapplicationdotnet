﻿app.controller('TransferAssetCtrl', function ($scope, $http) {
    $scope.ApproveorRject = true;
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "AssetName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.imgLoader_Save = false;
    $scope.imgLoader_Save1 = false;
    $scope.getVerifyTransferAssetList = function () {
        $http.get("../TransferAsset/getApproveTransferAssets?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                      function (response) {
                          if (response.data.expire) {
                              window.location.href = '../Home/Dashboard';
                          } else {
                              $scope.items = response.data.success.employees;
                              $scope.totalCount = response.data.success.totalCount;
                              $scope.showLoader = false;
                              $scope.showTable = true;
                          }
                      },
                      function (err) {
                          var error = err;
                      });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "AssetName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getVerifyTransferAssetList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SerachMethod = function (val) {
        //alert(val)
        $scope.search = val;
        $scope.getVerifyTransferAssetList();
    }
    //Loading employees list on first time
    $scope.getVerifyTransferAssetList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getVerifyTransferAssetList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getVerifyTransferAssetList();
    };

    //Modal popup open event
    //Gets selected Transfer asset
    $scope.showTransferPopup = function (ID) {
        //alert(ID)
        $scope.AssetID = ID;
        $http.post('../TransferAsset/getSelectedApproveTransferAsset', { TransferID: ID }).then(function (assetsdata) {

            if (assetsdata.data.success) {
                $scope.AssetDetail = assetsdata.data.success[0];
                //console.log($scope.AssetDetail)
                $scope.LoadTransferedAssetAttachmentsList(ID);

                //$('.AssetDescription').trigger('click');
            } else if (assetsdata.data.error) {

            } else if (assetsdata.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    $scope.LoadTransferedAssetAttachmentsList = function (TransferID) {
        $http.post('../TransferAsset/getTransferAttachments', { TransferID: TransferID }).then(function (attachments) {

            if (attachments.data.success) {
                $scope.AttachmentList = attachments.data.success;
                console.log("Attachments")
                console.log($scope.AttachmentList)


            }
            else if (attachments.data.error) {

            } else if (attachments.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }


    //Set Approve/Reject to New asset
    $scope.ApproveRejectTab = true;

    $scope.SignedTransferFormAttached = 'Yes';


    //Set Approve/Reject to Transfer asset
    $scope.TransferApproveReject = function (id, status, Commentary, UserPIN) {
        if (Commentary != undefined && UserPIN != undefined) {
            if (status == "Approve") {
                $scope.imgLoader_Save = true;
                $scope.imgLoader_Save1 = false;
            }
            else {
                $scope.imgLoader_Save1 = true;
                $scope.imgLoader_Save = false;
            }
            $scope.rejectMsg = false;
             var parameters = {
                AssetID: $scope.AssetID,
                Commentary: Commentary,
                Status: status,
                UserPIN: UserPIN,
                SignedTransferFormAttached: $scope.SignedTransferFormAttached,
                SignedTransferFormAttachedCommentary: $scope.SignedTransferFormAttached == 'No' ? $scope.SignedTransferFormAttachedCommentary : null

            }
             $http.get('../TransferAsset/TransferApproveReject', { params: parameters }).then(function (res) {
                 $scope.imgLoader_Save = false;
                 $scope.imgLoader_Save1 = false;
                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Home/VerifyTransferAsset';
                    });
                }
                else if (res.data.error){
                    alertify.alert('Error', res.data.error, function () {
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            });
        }
        else {
            $scope.rejectMsg = true;
        }
    }

});