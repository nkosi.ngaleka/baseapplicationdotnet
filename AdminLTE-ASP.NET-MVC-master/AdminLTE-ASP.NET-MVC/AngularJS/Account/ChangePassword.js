﻿app.controller('ChangePasswordController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;
    $scope.SpinnerSearchWip = false;
    $scope.imgLoader_Save = false;

    $scope.ChangePassword = function () {

        if ($scope["ChangePasswordForm"].$valid) {

            $scope.imgLoader_Save = true;
            $scope.showMsgs = false;

            $http.post('../Home/ChangePassword', { ChangePasswordData: $scope.ChangePasswordData })
                .then(function (res) {

                    $scope.imgLoader_Save = false;

                    if (res.data.success) {
                        alertify.alert('Success', res.data.success, function () {
                            window.location.href = '../Home/Logout';
                        });
                    }
                    else if (res.data.error) {
                        alertify.alert('Error', res.data.error, function () {
                        });
                    } else if (res.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                });

        }
        else {
            $scope.showMsgs = true;
        }
    }

   
});

