﻿app.controller('CompanyController', function ($scope, $http, $localStorage) {


    //Loads companies
    //$scope.LoadCompanies = function () {

    //    console.log('loaded...')

    //    var username = $('#Username').val() != '' ? $('#Username').val() : $localStorage.Username;
        
    //    console.log(username)

    //    if (username != '') {

    //        if (username != $localStorage.Username) {
    //            $scope.remember = "N";
    //        } else if (username == $localStorage.Username) {
    //            $scope.remember = "Y";
    //        }

    //        $('#spanCompany').attr('class', 'fa fa-refresh fa-spin');

    //        $http.post('../Account/GetCompanies', { Username: username }).then(function (cdata) {

    //            $('#spanCompany').attr('class', 'fa fa-building-o');

    //            if (cdata.data.Success == true) {
    //                $scope.CompaniesList = cdata.data.Result;

    //                if ($localStorage.chkbx && $localStorage.chkbx != '') {

    //                    $scope.CompanyID = $localStorage.CompanyID;

    //                }
    //            }
    //        });
    //    } 
    //}


    if ($localStorage.chkbx && $localStorage.chkbx != '') {

        $scope.remember = "Y";
        $scope.Username = $localStorage.Username;
        $scope.Password = $localStorage.Password;
        //$scope.CompanyID = $localStorage.CompanyID;
        $scope.LoadCompanies();

    } else {
        $scope.remember = "N";
        delete $scope.Username;
        delete $scope.Password;
        delete $scope.CompanyID;
    }

    $scope.rememberclick = function () {
        if ($('#remember').is(':checked')) {
            // save username and password
            $localStorage.Username = $scope.Username;
            $localStorage.Password = $scope.Password;
            $localStorage.CompanyID = $scope.CompanyID;
            $localStorage.chkbx = $scope.remember;
        } else {
            $localStorage.Username = '';
            $localStorage.Password = '';
            $localStorage.chkbx = '';
        }
    };
    
});

