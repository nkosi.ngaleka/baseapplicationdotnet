﻿app.controller('RegisterController', function ($scope, $filter, $http, $compile) {
    
    var formData = new FormData();
    var Uploadedfiles = [];

    // Configuration
    $scope.CompanyRegisterData = null;
    $scope.AttachmentFiles = [];

    $scope.SpinnerSave = false;
    $scope.SpinnerUploadAttachment = false;
    $scope.showMsgs = false;
    $scope.capturePanel = true;

    $scope.SaveType = 'Add';

    //Attachments
    $scope.RemoveAttachment = function (index) {
        $scope.AttachmentFiles.splice(index, 1);
    }

    $scope.UploadAttachment = function () {

        var files = $("#Attachment").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select files to upload.', function () {
            });
        } else {

            if ((GetAllFilesSizes(files) + GetAllFilesSizes($scope.AttachmentFiles)) > $scope.maxUploadSize) {
                alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
                return false;
            }

            $scope.AttachmentFiles = [];

            $scope.AttachmentFiles.push(files[0]);
   

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachment').val('').clone(true);
        }

    }

    //Attachments in update
    $scope.RemoveExistingAttachment = function (index) {
        $scope.ExistingAssetAttachmentList.splice(index, 1);
    }

    //NEXT / PREVIOUS IN TAB START
    $scope.SetFormPosition = function () {
        $(window).scrollTop($('#FormSpace').offset().top);
    }

    // Save 
    $scope.SaveCompany = function (form) {

        console.log($scope.CompanyRegisterData);
        
        $scope.SpinnerSave = true;

        if ($scope[form].$valid) {

            $scope.showMsgs = false;

            var files = $scope.AttachmentFiles;
      
            $http({
                method: 'post',
                url: '../Account/CreateCompany',
                headers: { 'Content-Type': undefined },

                transformRequest: function () {
                    formData.append("CompanyData", angular.toJson($scope.CompanyRegisterData));
                    formData.append("CompanyAttachment", files[0]);

                    return formData;
                }

            }).then(function (upData, status, headers, config) {

                $scope.SpinnerSave = false;

                if (upData.data.Success) {

                    alertify.alert('Success', upData.data.Success, function () {
                        window.location.href = '../Account/Register';
                    });

                }
                else if (upData.data.Info) {

                    alertify.alert('Message', upData.data.Info, function () {
                        window.location.href = '../Account/Register';
                    });

                }
                // else if (upData.data.Error) {

                //    alertify.alert('Message', "Problem registering Company. Please try after sometime.", function () {
                //        window.location.href = '../Account/Register';
                //    });

                //}

            });

        }
        else {

            $scope.showMsgs = true;

            $scope.SpinnerSave = false;
        }

    }

});

