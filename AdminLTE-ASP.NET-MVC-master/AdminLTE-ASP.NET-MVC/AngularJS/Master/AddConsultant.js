﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.SaveType = "Add";

    $scope.EditConsultantID = null;

    $scope.CreateNewConsultantData = {};
    $scope.CreateNewConsultantData.Status = "Active";

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "Name";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getConsultantList = function () {
        $http.get("../Master/getConsultants?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search)
            .then(
                          function (response) {
                              if (response.data.success) {
                                  $scope.Consultants = response.data.success.Consultants;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "Name";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getConsultantList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getConsultantList();
    }
    //Loading employees list on first time
    $scope.getConsultantList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getConsultantList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getConsultantList();
    };

    $scope.showAddConsultantForm = function () {

        $scope.SaveType = 'Add';

        $scope.rTable = false;
        $scope.rForm = true;

        $scope.CreateNewConsultantData.Status = "Active";
        $scope.EditConsultantID = null;

    }

    $scope.showConsultantsTable = function () {

        $scope.rTable = true;
        $scope.rForm = false;

        $scope.CreateNewConsultantData = {};

    }

    //Save User
    $scope.SaveNewConsultant = function () {

        if ($scope['CreateNewConsultantForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            
            $scope.CreateNewConsultantData.Address = $('#Address').val();


            $http.post('../Master/CreateNewConsultant',
                {
                    ConsultantFormData: $scope.CreateNewConsultantData,
                    SaveType: $scope.EditConsultantID == null ? "Add" : "Update"
                })
                .then(function (res) {

                $scope.imgLoader_Save = false;

                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Master/AddNewConsultant';
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }
    }


    //Show Edit Form
    $scope.EditConsultant = function (ConsultantID) {

        $http.post('../Master/getConsultantDetails', { ConsultantID: ConsultantID }).then(function (res) {

            if (res.data.success) {

                $scope.EditConsultantID = ConsultantID;

                $scope.CreateNewConsultantData = res.data.ConsultantDetails[0];
                //console.log('EDIT DATA');
                //console.log(res.data.MeanDetails[0]);

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Consultant form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }
});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



