﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;
    $scope.EditUserID = null;
    $scope.uTable = true;
    $scope.uForm = false;
    $scope.SaveType = "";
    $scope.CreateNewUserData = {};
    $scope.CreateNewUserData.Status = "Active";

    
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "FirstName";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.search = '';
    $scope.getFieldWorkers = function () {

        var url = "../Master/getFieldWorkers?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search;

        $http.get(url)
            .then(function (response) {
                console.log(response)
                if (response.data.success) {
                    console.log(response.data.success);
                    $scope.fieldWorkers = response.data.items;
                    console.log('Field Workers');
                    console.log($scope.fieldWorkers)
                    $scope.totalCount = response.data.totalCount;
                    $scope.showLoader = false;
                    $scope.showTable = true;
                } else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            }, function (err) {
                    var error = err;
                    $scope.showLoader = false;
                    $scope.showTable = true;
            });
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "FirstName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.getFieldWorkers();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getFieldWorkers();
    }

    //Loading employees list on first time
    $scope.getFieldWorkers();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getFieldWorkers();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getFieldWorkers();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

    $scope.showAddUserForm = function () {

        $scope.SaveType = 'Add';

        $scope.uTable = false;
        $scope.uForm = true;

        $scope.EditUserID = null;
        $scope.CreateNewUserData

    }

    $scope.showUsersTable = function () {

        $scope.uTable = true;
        $scope.uForm = false;

        $scope.CreateNewUserData = null;

    }

    //Attachments
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {
        $scope.AttachmentFiles.splice(index, 1);
    }

    $scope.UploadAttachment = function () {
        $scope.AttachmentFiles = [];
        var files = $("#Attachment").get(0).files;

        if (files.length < 1) {
            alertify.alert('Message', 'Select files to upload.', function () {
            });
        } else {

            if ((GetAllFilesSizes(files) + GetAllFilesSizes($scope.AttachmentFiles)) > $scope.maxUploadSize) {
                alertify.alert('Stop', "Files size exceeded. Max files size allowed is " + $scope.maxUploadSizeString + ".", function () { });
                return false;
            }

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachment').val('').clone(true);
        }
    }

    //Save User
    $scope.SaveFieldWorker = function () {
       // alert()
        var filecount = $("#Attachment").get(0).files;

        if (filecount.length > 0) {
            alertify.alert('Message', "You have selected Profile Picture but not uploaded. Please upload.", function () {
            });
            return;
        }

        var formData = new FormData();

        if ($scope['CreateNewUserForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            if ($scope.AttachmentFiles < 1) {
                $http.post('../Master/CreateNewUser',
                    {
                        UserFormData: $scope.CreateNewUserData,
                        SaveType: $scope.EditUserID == null ? "Add" : "Update"
                    }).then(function (res) {

                        console.log('res');
                        console.log(res);

                        $scope.imgLoader_Save = false;
                        if (res.data.success) {
                            alertify.alert('Success', res.data.success, function () {
                                window.location.href = '../Master/FieldWorkers';
                            });
                        } else if (res.data.info) {
                            alertify.alert('Message', res.data.info, function () {
                            });
                        } else {
                            alertify.alert('Message', "Problem creating user.", function () {
                                window.location.href = '../Master/FieldWorkers';
                            });
                        }
                    });
            } else {
                var files = $scope.AttachmentFiles;
                $http({
                    method: 'post',
                    url: '../Master/CreateNewUserWithAttachment',
                    headers: { 'Content-Type': undefined },

                    transformRequest: function () {
                        formData.append("UserFormData", angular.toJson($scope.CreateNewUserData));
                        formData.append("SaveType", angular.toJson($scope.EditUserID == null ? "Add" : "Update"));

                        for (var i = 0; i < files.length; i++) {
                            formData.append("UploadedImage", files[i]);
                        }

                        return formData;
                    }
                }).then(function (res) {

                    console.log('res');
                    console.log(res);

                    $scope.imgLoader_Save = false;

                    if (res.data.success) {
                        alertify.alert('Success', res.data.success, function () {
                            window.location.href = '../Master/FieldWorkers';
                        });
                    } else if(res.data.info) {
                        alertify.alert('Message', res.data.info, function () {
                        });
                    } else {
                        alertify.alert('Message', "Problem creating user.", function () {
                        });
                    }

                });
            }
        }
        else {
            $scope.showMsgs = true;
        }
    }

    //Show Edit User Form
    $scope.EditUser = function (item) {
        $scope.EditUserID = item.ID;

        $scope.CreateNewUserData = item;
        $scope.SaveType = 'Update';
        $scope.uTable = false;
        $scope.uForm = true;
        //$http.post('../Master/getUserDetails',
        //    {
        //        UserID: UserID
        //    }).then(function (res) {

        //    if (res.data.success) {

        //        $scope.EditUserID = UserID;
               
        //        $scope.CreateNewUserData = res.data.UserDetails[0];
        //        console.log('EDIT DATA');
        //        console.log(res);

        //        //$scope.EditProjectNumber = res.data.UserDetails[0].ID;

        //        //display form
        //        $scope.SaveType = 'Update';
        //        $scope.uTable = false;
        //        $scope.uForm = true;

        //    } else if (res.data.error) {
        //        alertify.alert('Message', "Problem loading Update User form.", function () {
        //        });
        //    } else if (res.data.expire) {
        //        window.location.href = '../Home/Dashboard';
        //    }

        //});
    }

});


app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});


