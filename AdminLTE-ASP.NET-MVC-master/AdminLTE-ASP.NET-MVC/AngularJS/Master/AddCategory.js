﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.SaveType = "Add";

    $scope.TempClassificationID = null;
    $scope.TempClassID = null;
    $scope.EditCategoryID = null;

    $scope.CreateNewCategoryData = {};
    $scope.CreateNewCategoryData.Status = "Active";

    $scope.ClassificationChanged = function () {

        if ($scope.CreateNewCategoryData.ClassificationID !== undefined) {
            $http.post("../Asset/getClasses",
                {
                    ClassificationID: $scope.CreateNewCategoryData.ClassificationID
                })
            .then(function (response) {
                if (response.data.success) {
                    $scope.Classes = response.data.success;
                    console.log('CLASSES')
                    console.log($scope.Classes)
                } else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            });
        }

    }

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "CategoryName";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getCategoryList = function () {
        $http.get("../Master/getCategories?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                          function (response) {
                              if (response.data.success) {
                                  //console.log('load')
                                  console.log(response)
                                  $scope.Categories = response.data.Categories.Categories;
                                  //$scope.Classes = response.data.Classes;
                                  $scope.Classifications = response.data.Classifications;
                                  $scope.totalCount = response.data.Categories.totalCount;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                              $scope.showLoader = false;
                              $scope.showTable = true;

                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "CategoryName";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getCategoryList();
    }
    // remove and change class
    $scope.sortCategory = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getCategoryList();
    }
    //Loading employees list on first time
    $scope.getCategoryList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getCategoryList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getCategoryList();
    };

    $scope.showAddCategoryForm = function () {

        $scope.SaveType = 'Add';

        $scope.CreateNewCategoryData.Status = "Active";
        $scope.TempClassificationID = null;
        $scope.TempClassID = null;
        $scope.EditCategoryID = null;

        $scope.Classes = [];

        $scope.rTable = false;
        $scope.rForm = true;

    }

    $scope.showCategoriesTable = function () {

        $scope.rTable = true;
        $scope.rForm = false;

        $scope.CreateNewCategoryData = {};

    }

    //Save
    $scope.SaveNewCategory = function () {

        if ($scope['CreateNewCategoryForm'].$valid) {
            if ($scope.SaveType == "Update") {
                $scope.CreateNewCategoryData.ClassificationID = $scope.TempClassificationID;
                $scope.CreateNewCategoryData.ClassID = $scope.TempClassID;
            }
            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewCategory',
                {
                    CategoryFormData: $scope.CreateNewCategoryData,
                    SaveType: $scope.EditCategoryID == null ? "Add" : "Update"
                }).then(function (res) {

                    $scope.imgLoader_Save = false;

                    if (res.data.success) {
                        alertify.alert('Success', res.data.success, function () {
                            window.location.href = '../Master/AddCategory';
                        });
                    }
                    else if (res.data.error) {
                        alertify.alert('Error', res.data.error, function () {
                        });
                    } else if (res.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }

                });

        }
        else {
            $scope.showMsgs = true;
        }
    }


    //Show Edit Form
    $scope.EditCategory = function (CategoryID) {

        $http.post('../Master/getCategoryDetails', { CategoryID: CategoryID }).then(function (res) {

            if (res.data.success) {

                //$scope.Classes = [
                //    {
                //        Id: res.data.CategoryDetails[0].ClassId,
                //        Description: res.data.CategoryDetails[0].ClassDescription
                //    }
                //];

                $scope.Classes = res.data.ClassData;

                $scope.EditCategoryID = CategoryID;

                $scope.CreateNewCategoryData = res.data.CategoryDetails[0];
                //$scope.CreateNewCategoryData['ClassId'] = res.data.CategoryDetails[0].ClassId;
                //console.log(res.data.CategoryDetails[0].ClassId);
                //console.log($scope.CreateNewCategoryData['ClassId']);

                console.log('EDIT DATA');
                console.log(res.data.CategoryDetails[0]);
                $scope.TempClassificationID = $scope.CreateNewCategoryData["ClassificationID"];
                $scope.TempClassID = $scope.CreateNewCategoryData["ClassID"];

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Class form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



