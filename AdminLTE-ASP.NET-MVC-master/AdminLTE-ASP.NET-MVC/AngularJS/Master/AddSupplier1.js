﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.EditSupplierID = null;


    $scope.uTable = true;
    $scope.uForm = false;
    $scope.SaveType = "";

    $scope.CreateNewSupplierData = {};
    $scope.CreateNewSupplierData.Status = "Active";
    
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "FirstName";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getEmployeeList = function () {
        $http.get("../Master/getSuppliers?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&search=" + $scope.search).then(
                          function (response) {
                              if (response.data.success) {
                                  $scope.suppliers = response.data.success.Suppliers;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "ID";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getEmployeeList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getEmployeeList();
    }
    //Loading employees list on first time
    $scope.getEmployeeList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getEmployeeList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getEmployeeList();
    };

    $scope.showAddSupplierForm = function () {

        $scope.SaveType = 'Add';

        $scope.uTable = false;
        $scope.uForm = true;

        $scope.EditSupplierID = null;
        $scope.CreateNewSupplierData

    }

    $scope.showSuppliersTable = function () {

        $scope.uTable = true;
        $scope.uForm = false;

        $scope.CreateNewSupplierData = null;

    }

    //Attachments
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {
        $scope.AttachmentFiles.splice(index, 1);
    }

    $scope.UploadAttachment = function () {

        var files = $("#Attachment").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select files to upload.', function () {
            });
        } else {

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachment').val('').clone(true);
        }
    }

    //Save Supplier
    $scope.SaveUploadedSuppliers = function () {

        var filecount = $("#Attachment").get(0).files;

        if (filecount.length > 0) {
            alertify.alert('Error', "You have selected Profile Picture but not uploaded. Please upload.", function () {
            });
            return;
        }

        if ($scope.AttachmentFiles < 1) {

            alertify.alert('Error', "Please upload Suppliers List.", function () {
            });
            return;
        }

        var formData = new FormData();

        if ($scope['CreateNewSupplierForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

                var files = $scope.AttachmentFiles;
                $http({
                    method: 'post',
                    url: '../Master/UploadSuppliers',
                    headers: { 'Content-Type': undefined },

                    transformRequest: function () {
                        //formData.append("SaveType", angular.toJson($scope.EditSupplierID == null ? "Add" : "Update"));

                        for (var i = 0; i < files.length; i++) {
                            formData.append("UploadedFile", files[i]);
                        }

                        return formData;
                    }
                }).then(function (res) {

                    $scope.imgLoader_Save = false;

                    if (res.data.success) {
                        alertify.alert('Success', res.data.success, function () {
                            window.location.href = '../Master/UploadSupplier';
                        });
                    } else if (res.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                    else {
                        alertify.alert('Error', res.data.error, function () {
                        });
                    }

                });
            }

        else {
            $scope.showMsgs = true;
        }
    }

    //Show Edit Supplier Form
    $scope.EditSupplier = function (SupplierID) {
        
        $http.post('../Master/getSupplierDetails',
            {
                SupplierID: SupplierID
            }).then(function (res) {

            if (res.data.success) {

                $scope.EditSupplierID = SupplierID;
               
                $scope.CreateNewSupplierData = res.data.SupplierDetails[0];
                //console.log('EDIT DATA');
                //console.log(res);

                //$scope.EditProjectNumber = res.data.SupplierDetails[0].ID;

                //display form
                $scope.SaveType = 'Update';
                $scope.uTable = false;
                $scope.uForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Supplier form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }


});


app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



