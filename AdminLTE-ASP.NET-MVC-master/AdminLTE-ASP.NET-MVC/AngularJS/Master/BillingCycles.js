﻿app.controller('MasterController', function ($scope, $http, $filter, $compile) {

    //Table config
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "METER_NO"; // Default sorting column
    $scope.showLoader = false;
    $scope.showTable = false;
    $scope.frmMap = true;
    $scope.CycleList = [];
    $scope.CCC = "";
    $http.post('../Master/GetCCC').then(function (reasons) {
        console.log(reasons)
        if (reasons.data.success) {
            $scope.CCCList = reasons.data.CCC;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });

    $scope.GetBillingCycles = function () {
        $scope.showLoader = true;
        $scope.loaderGet = true;
        $scope.frmMap = true;
        $scope.showTable = false;
        $http.post('../Master/GetBillingCycles', { CCC: $scope.CCC}).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.showLoader = false;
                $scope.showTable = true;
                $scope.loaderGet = false;
                $scope.CycleList = reasons.data.items;
            } else if (reasons.data.expire) {
               
                window.location.href = "../Account/Login";
            }
        });
    }
    $scope.showBillingsTable = function () {
        //alert()
        $scope.frmMap = true;
        $scope.uForm = false;
    }
    $scope.EditBillingCycle = function (item) {
        $scope.EditCycleID = item.ID;
        $scope.EditBilling = item;
        $scope.CreateNewUserData = {};
        $scope.imgLoader_Save = false;

        $scope.frmMap = false;
        $scope.uForm = true;
       
    }
    $scope.UpdateBillingCycle = function () {

        if ($scope.CreateNewUserData.StartDate && $scope.CreateNewUserData.EndDate) {
            $scope.imgLoader_Save = true;
            $http.post('../Master/UpdateBillingCycle', { ID: $scope.EditCycleID, StartDate: $scope.CreateNewUserData.StartDate, EndDate: $scope.CreateNewUserData.EndDate }).then(function (reasons) {
                console.log(reasons)
                if (reasons.data.success) {
                    alertify.alert("Success", reasons.data.BillingCycle, function () {
                        $scope.uForm = false;
                        $scope.frmMap = true;
                        $scope.imgLoader_Save = false;
                        $scope.GetBillingCycles();
                    });

                } else if (reasons.data.expire) {

                    window.location.href = "../Account/Login";
                } else {
                    alertify.alert("Error", "Billing cycle update failed", function () { });
                }
            });
            
        } else {
            alertify.alert("Error", "Billing start date and end date required", function () { });

        }
       
    }
    // Search fields
    $scope.search = '';

    $scope.MapData = [];

    $scope.frmSearch = true;
    //$scope.frmMap = false;
    $scope.loaderGet = false;
    $scope.loaderSave = false;
    $scope.isFormValid = false;
    $scope.showError = false;
    $scope.CategoryList = ['Water', 'Electricity', 'All'];

    $scope.$watch('SearchCriteriaForm.$valid', function (newValue) {
        $scope.isFormValid = newValue;
    });

    $http.post('../Master/GetFormConfig').then(function (reasons) {
        console.log(reasons)
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;
            $scope.SuburbList = reasons.data.suburbs;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });

    $("#chkParent").click(function () {
        $(".chkChild").prop("checked", this.checked);
        $scope.chkChildClicked();
    });

    $scope.chkChildClicked = function () {

        $('#tableBody').find('input[type="checkbox"]:checked').each(function () {
            if ($('.chkChild:checked').length == $('.chkChild').length) {
                $('#chkParent').prop('checked', true);
            } else {
                $('#chkParent').prop('checked', false);
            }
        });

        $('#tableBody').find('input[type="checkbox"]').each(function () {

            if ($(this).prop("checked") === true) {
                if ($scope.selectedMeters.indexOf(this.id) === -1) {
                    $scope.selectedMeters.push(this.id);
                }
            } else {
                if ($scope.selectedMeters.indexOf(this.id) !== -1) {
                    $scope.selectedMeters.splice($scope.selectedMeters.indexOf(this.id), 1);
                }
            }

        });

        console.log($scope.selectedMeters);
    }

    //$('.atleastone :input').change(function () {
    //    $('.atleastone :input').each(function () {
    //        if ($(this).val() == "") {
    //            $scope.errShow = true;
    //        } else {
    //            $scope.errShow = false;
    //            return false;
    //        }
    //    });
    //});

    $scope.GetFormUser = function () {
        $scope.selectedMeters = [];
        $scope.GetForm();
    }

    $scope.GetForm = function () {

        $('#chkParent').prop('checked', false);

        $scope.showLoader = true;
        $scope.showTable = false;

        $scope.errShow = false;
        $scope.SearchCriteriaFormSubmitted = true;


        if ($scope.isFormValid && !$scope.errShow) {

            $scope.loaderGet = true;
            $scope.showError = false;

            var url = '../Master/GetAllocatedMeters'
            var params = {
                UserID: $scope.UserID,
                Unreviewed: true,
                pageIndex: $scope.pageIndex,
                pageSize: $scope.pageSizeSelected,
                sorting: $scope.sorting,
                orderType: $scope.orderType,
                search: $scope.search
            };

            console.log(params);

            $http.post(url, params)
                .then(function (reasons) {
                    $scope.frmMap = false;
                    if (reasons.data.success === true) {
                        console.log(reasons)
                        if (reasons.data.items.length > 0) {
                            $scope.Screens = reasons.data.items;
                            $scope.totalCount = reasons.data.totalCount;

                            $('#tableBody').find('input[type="checkbox"]').each(function () {

                                // if exists
                                if ($scope.selectedMeters.indexOf(this.id) !== -1) {
                                    //$(this).prop("checked", true);
                                    $scope['cb' + this.id] = true;
                                }

                            });
                            $scope.loaderGet = true;
                            $scope.frmMap = true;
                        } else {
                            alertify.alert("Success", "No Meters allocated for the selected Field worker", function () { });

                        }

                        $scope.loaderGet = false;

                    } else if (reasons.data.error) {
                        $scope.loaderGet = false;
                        alertify.alert("Error", reasons.data.error, function () { });
                    } else if (reasons.data.expire) {
                        window.location.href = "../Account/Login";
                    } else { window.location.href = "../Account/Login"; }

                    $scope.showLoader = false;
                    $scope.showTable = true;
                });
        }
    }

    $scope.isExist = function (id) {
        return $scope.MapData.map(function (type) { return type.USID; }).indexOf(id);
    }

    $scope.SaveUserScreenMap = function () {

        if ($scope.UserID == undefined) { return; }

        $scope.loaderSave = true;

        var checkList = $scope.selectedMeters;

        $http.post('../Master/SaveUnallocations',
            {
                CheckList: checkList,
                UserID: $scope.UserID
            })
            .then(function (response) {
                if (response.data.success) {
                    $scope.loaderSave = false;
                    alertify.alert("Success", response.data.success, function () {
                        window.location.href = '../Master/Unallocate';
                    });
                } else if (response.data.error) {
                    $scope.loaderSave = false;
                    alertify.alert("Error", response.data.error, function () { });
                } else {
                    window.location.href = "../Account/Login";
                }
            });
    }

    // Table section
    // Sorting
    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "METER_NO";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.GetForm();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    // Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetForm();
    }

    // This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetForm();
    };

    // This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetForm();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});