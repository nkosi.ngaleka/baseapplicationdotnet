﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.SaveType = "Add";

    $scope.TempClassificationID = null;
    $scope.TempClassID = null;
    $scope.TempCategoryID = null;
    $scope.EditSubCategoryID = null;

    $scope.CreateNewSubCategoryData = {};
    $scope.CreateNewSubCategoryData.Status = "Active";

    $scope.ClassificationChanged = function () {

        if ($scope.CreateNewSubCategoryData.ClassificationID !== undefined) {
            $http.post("../Asset/getClasses", { ClassificationID: $scope.CreateNewSubCategoryData.ClassificationID })
            .then(function (response) {
                if (response.data.success) {
                    $scope.Classes = response.data.success;
                }else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else if (response.data.error) {
                    alertify.alert('Error', response.data.error, function () {
                    });
                }
            });
        }

    }


    $scope.ClassChanged = function () {

        if ($scope.CreateNewSubCategoryData.ClassID !== undefined) {
            $http.post("../Asset/getCategories", { ClassID: $scope.CreateNewSubCategoryData.ClassID })
            .then(function (response) {
                if (response.data.success) {
                    $scope.Categories = response.data.success;
                } else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else if (response.data.error) {
                    alertify.alert('Error', response.data.error, function () {
                    });
                }
            });
        }

    }


    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "ClassificationName";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getSubCategoryList = function () {
        $http.get("../Master/getSubCategories?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                          function (response) {
                              if (response.data.success) {
                                  console.log('load')
                                  console.log(response)
                                  $scope.SubCategories = response.data.SubCategories.SubCategories;
                                  
                                  $scope.Classifications = response.data.Classifications;
                                  $scope.totalCount = response.data.SubCategories.totalCount;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              } else { }
                              $scope.showLoader = false;
                              $scope.showTable = true;

                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "ClassificationName";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getSubCategoryList();
    }
    // remove and change class
    $scope.sortSubCategory = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getSubCategoryList();
    }
    //Loading employees list on first time
    $scope.getSubCategoryList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getSubCategoryList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getSubCategoryList();
    };

    $scope.showAddSubCategoryForm = function () {

        $scope.SaveType = 'Add';


        $scope.CreateNewSubCategoryData.Status = "Active";
        $scope.TempClassificationID = null;
        $scope.TempClassID = null;
        $scope.TempCategoryID = null;
        $scope.EditSubCategoryID = null;

        $scope.Classes = [];
        $scope.Categories = [];


        $scope.rTable = false;
        $scope.rForm = true;

    }

    $scope.showSubCategoriesTable = function () {

        $scope.rTable = true;
        $scope.rForm = false;

        $scope.CreateNewSubCategoryData = {};

    }

    //Save
    $scope.SaveNewSubCategory = function () {

        if ($scope['CreateNewSubCategoryForm'].$valid) {
            if ($scope.SaveType == "Update") {
                $scope.CreateNewSubCategoryData.ClassificationID = $scope.TempClassificationID;
                $scope.CreateNewSubCategoryData.ClassID = $scope.TempClassID;
                $scope.CreateNewSubCategoryData.CategoryID = $scope.TempCategoryID;
            }
            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewSubCategory',
                {
                    SubCategoryFormData: $scope.CreateNewSubCategoryData,
                    SaveType: $scope.EditSubCategoryID == null ? "Add" : "Update"
                }).then(function (res) {

                $scope.imgLoader_Save = false;

                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Master/AddSubCategory';
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                } else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }
    }


    //Show Edit Form
    $scope.EditSubCategory = function (SubCategoryID) {

        $http.post('../Master/getSubCategoryDetails', { SubCategoryID: SubCategoryID }).then(function (res) {

            if (res.data.success) {

                $scope.Classes = res.data.ClassData;
                $scope.Categories = res.data.CategoryData;

                $scope.EditSubCategoryID = SubCategoryID;

                $scope.CreateNewSubCategoryData = res.data.SubCategoryDetails[0];

                $scope.TempClassificationID = $scope.CreateNewSubCategoryData["ClassificationID"];
                $scope.TempClassID = $scope.CreateNewSubCategoryData["ClassID"];
                $scope.TempCategoryID = $scope.CreateNewSubCategoryData["CategoryID"];

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Class form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }


});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



