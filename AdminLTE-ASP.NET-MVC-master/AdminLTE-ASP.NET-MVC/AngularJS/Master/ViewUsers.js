﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.showTable = false;
    $scope.showLoader = true;

    $http.get("../Master/GetCompanyUsers")
        .then(function (response) {
            if (response.data.success) {

                if (response.data.info) {
                    alertify.alert('Message', "No data found", function () { });
                } else {
                    $scope.users = response.data.result.Users;
                }

                $scope.showLoader = false;
                $scope.showTable = true;
            } 
        },
        function (err) {
            var error = err;
            $scope.showLoader = false;
            $scope.showTable = true;
        });

});
