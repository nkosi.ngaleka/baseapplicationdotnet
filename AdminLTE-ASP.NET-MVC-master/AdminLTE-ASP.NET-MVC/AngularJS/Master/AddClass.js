﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.SaveType = "Add";

    $scope.TempClassificationID = null;
    $scope.EditClassID = null;

    $scope.CreateNewClassData = {};
    $scope.CreateNewClassData.Status = "Active";

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "Description";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getClassList = function () {
        $http.get("../Master/getClasses?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                          function (response) {

                              console.log(response.data);

                              if (response.data.success) {
                                  $scope.Classes = response.data.success.Classes;
                                  $scope.Classifications = response.data.Classifications;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "Description";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getClassList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getClassList();
    }
    //Loading employees list on first time
    $scope.getClassList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getClassList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getClassList();
    };

    $scope.showAddClassForm = function () {

        $scope.SaveType = 'Add';

        $scope.CreateNewClassData.Status = "Active";
        $scope.TempClassificationID = null;
        $scope.EditClassID = null;

        $scope.rTable = false;
        $scope.rForm = true;

    }

    $scope.showClassesTable = function () {

        $scope.rTable = true;
        $scope.rForm = false;

        $scope.CreateNewClassData = {};

    }

    //Save
    $scope.SaveNewClass = function () {

        if ($scope['CreateNewClassForm'].$valid) {
            if ($scope.SaveType == "Update") {
                $scope.CreateNewClassData.ClassificationID = $scope.TempClassificationID;
            }
            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewClass',
                {
                    ClassFormData: $scope.CreateNewClassData,
                    SaveType: $scope.EditClassID == null ? "Add" : "Update"
                }).then(function (res) {

                    $scope.imgLoader_Save = false;

                    if (res.data.success) {
                        alertify.alert('Success', res.data.success, function () {
                            window.location.href = '../Master/AddClass';
                        });
                    } else if (res.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                    else {
                        alertify.alert('Error', res.data.error, function () {
                        });
                    }

                });

        }
        else {
            $scope.showMsgs = true;
        }
    }

    //Show Edit Form
    $scope.EditClass = function (ClassID) {

        $http.post('../Master/getClassDetails', { ClassID: ClassID }).then(function (res) {
            console.log(res.data);
            if (res.data.success) {

                $scope.EditClassID = ClassID;

                $scope.CreateNewClassData = res.data.ClassDetails[0];
                console.log('EDIT DATA');
                console.log(res.data.ClassDetails[0]);
                $scope.TempClassificationID = $scope.CreateNewClassData["ClassificationID"];

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Class form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



