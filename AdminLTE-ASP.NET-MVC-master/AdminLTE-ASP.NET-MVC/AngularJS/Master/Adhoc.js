﻿app.controller('AdhocMasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.SaveType = "Add";

    $scope.EditLinkID = null;

    $scope.reportLinksData = {};
    $scope.reportLinksData.Status = "Active";
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "ReportName";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getReportLinksList = function () {
        $http.get("../Master/GetAdhocLinks?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&search=" + $scope.search)
            .then(function (response) {
                if (response.data.success) {
                    $scope.reportLinks = response.data.success.ReportLinks;
                    $scope.totalCount = response.data.success.totalCount;
                    $scope.showLoader = false;
                    $scope.showTable = true;
                } else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            }, function (err) {
                var error = err;
            });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "Id";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getReportLinksList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getReportLinksList();
    }
    //Loading employees list on first time
    $scope.getReportLinksList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getReportLinksList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getReportLinksList();
    };

    $scope.showAddLinkForm = function () {

        $scope.SaveType = 'Add';

        $scope.rTable = false;
        $scope.rForm = true;
        $scope.reportLinksData.Status = "Active";
        $scope.EditLinkID = null;

    }

    $scope.showLinksTable = function () {

        $scope.rTable = true;
        $scope.rForm = false;
        $scope.reportLinksData = {};

    }

    //Save User
    $scope.SaveNewLink = function () {

        if ($scope['reportLinksForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewAdhocLink',
                {
                    ReportLinksFormData: $scope.reportLinksData,
                    SaveType: $scope.EditLinkID == null ? "Add" : "Update"
                })
                .then(function (res) {

                $scope.imgLoader_Save = false;

                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Master/Adhoc';
                    });
                }
                else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }
    }

    //Show Edit Form
    $scope.EditReportLink = function (ID) {

        $http.post('../Master/GetReportLinkDetails', { ID: ID }).then(function (res) {

            if (res.data.success) {

                $scope.EditLinkID = ID;

                $scope.reportLinksData = res.data.ReportLinkDetails[0];

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Report Link form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

    //Reading query string to decide add/edit
    function getUrlParameter(param, dummyPath) {

        var sPageURL = dummyPath || window.location.search.substring(1),
            sURLVariables = sPageURL.split(/[&||?]/),
            res;

        for (var i = 0; i < sURLVariables.length; i += 1) {
            var paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');

            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }

        return res;
    }

    var fromExternal = getUrlParameter('fromext');
    if (fromExternal !== undefined) {
        $scope.showAddLinkForm();
    }
});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});