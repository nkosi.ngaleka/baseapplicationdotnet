﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.dTable = true;
    $scope.dForm = false;

    $scope.EditMeanID = null;

    $scope.CreateNewMeanData = {};
    $scope.CreateNewMeanData.Status = "Active";
    $scope.SaveType = "";

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "Mean";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getAssetDisposeMeans = function () {
        $http.get("../Master/getAssetDisposeMeans?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                          function (response) {
                              if (response.data.success) {
                                  $scope.Means = response.data.success.Means;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "Mean";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getAssetDisposeMeans();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getAssetDisposeMeans();
    }
    //Loading employees list on first time
    $scope.getAssetDisposeMeans();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getAssetDisposeMeans();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getAssetDisposeMeans();
    };

    $scope.showAddMeanForm = function () {

        $scope.SaveType = 'Add';

        $scope.dTable = false;
        $scope.dForm = true;

        $scope.CreateNewMeanData.Status = "Active";
        $scope.EditMeanID = null;

    }

    $scope.showMeansTable = function () {

        $scope.dTable = true;
        $scope.dForm = false;

        $scope.CreateNewMeanData = {};

    }

    //Save User
    $scope.SaveNewMean = function () {

        if ($scope['CreateNewMeanForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewAssetDisposeMean', {
                MeanFormData: $scope.CreateNewMeanData,
                SaveType: $scope.EditMeanID == null ? "Add" : "Update"
            }).then(function (res) {

                $scope.imgLoader_Save = false;

                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Master/AddNewAssetDisposeMean';
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }
    }

    //Show Edit Form
    $scope.EditMean = function (MeanID) {

        $http.post('../Master/getMeanDetails', { MeanID: MeanID }).then(function (res) {

            if (res.data.success) {

                $scope.EditMeanID = MeanID;

                $scope.CreateNewMeanData = res.data.MeanDetails[0];
                console.log('EDIT DATA');
                console.log(res.data.MeanDetails[0]);

                //display form
                $scope.SaveType = 'Update';
                $scope.dTable = false;
                $scope.dForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Mean form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



