﻿app.controller('ViewStagingController', function ($scope, $http, $filter, $compile) {

    //Table config
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "MODIFIED_DATE"; // Default sorting column
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.searchForm = true;
    $scope.imageView = false;
    $scope.isSaving = false;
    $scope.selectedMeters = [];
    $scope.Stage = 'YES';
    $scope.UserID = "0";
    $scope.EditView = false;
    // Search fields
    $scope.search = '';
    $scope.MapData = [];

    $scope.showError = false;
    $scope.frmSearch = true;
    $scope.frmMap = false;
    $scope.loaderGet = false;
    $scope.loaderSave = false;
    $scope.isFormValid = false;
    var date = new Date();
    $scope.fromdate = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
    $scope.todate = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');

    $scope.$watch('SearchCriteriaForm.$valid', function (newValue) {
        $scope.isFormValid = newValue;
    });
    $http.post('../Master/GetReadCode').then(function (reasons) {
        if (reasons.data.success) {
            $scope.ReadCodeList = reasons.data.ReadCode;

        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $http.post('../Master/GetCCC').then(function (reasons) {
        console.log(reasons)
        if (reasons.data.success) {
            $scope.CCCList = reasons.data.CCC;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetCycle = function (ccc) {
        $scope.CycleList = [];
        $scope.routeList = [];
        $http.post('../Master/GetCycle?CCC=' + ccc).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.CycleList = reasons.data.Cycles;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }

    $scope.getRouteCode = function (CCC, Cycle) {
        //  alert(CCC)
        //var url = '../Master/GetUnAllocatedList'
        $scope.routeList = [];
        var params = {
            Cycle: $scope.Cycle,
            CCC: $scope.CCC
        };
        $http.post('../Master/GetRouteCodeCycle', params).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.RouteList = reasons.data.Routes;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }
    

    $("#chkParent").click(function () {
        $(".chkChild").prop("checked", this.checked);
        $scope.chkChildClicked();
    });

    $scope.chkChildClicked = function () {

        $('#tableBody').find('input[type="checkbox"]:checked').each(function () {
            if ($('.chkChild:checked').length == $('.chkChild').length) {
                $('#chkParent').prop('checked', true);
            } else {
                $('#chkParent').prop('checked', false);
            }
        });

        $('#tableBody').find('input[type="checkbox"]').each(function () {

            if ($(this).prop("checked") === true) {
                if ($scope.selectedMeters.indexOf(this.id) === -1) {
                    $scope.selectedMeters.push(this.id);
                }
            } else {
                if ($scope.selectedMeters.indexOf(this.id) !== -1) {
                    $scope.selectedMeters.splice($scope.selectedMeters.indexOf(this.id), 1);
                }
            }

        });

        console.log($scope.selectedMeters);
    }


    $scope.GetFormUser = function () {
        $scope.selectedMeters = [];
        $scope.GetForm();
    }


    $scope.GetForm = function () {

        $('#chkParent').prop('checked', false);

        $scope.showLoader = true;
        $scope.showTable = false;

        $scope.errShow = false;
        $scope.SearchCriteriaFormSubmitted = true;
        if ($scope.isFormValid && !$scope.errShow) {
            $scope.MeterItems = [];
            $scope.SpinnerSearchWip = true;

            $scope.loaderGet = true;
            $scope.showError = false;
            debugger;
            var url = '../Master/GetBulkStagingMeters'
            var params = {
                CCC: $scope.CCC,
                Cycle: $scope.Cycle,
                Route: $scope.Route,
                ReadCode: $scope.ReadCode,
                from: $scope.fromdate,
                to: $scope.todate,
                Unreviewed: true,
                pageIndex: $scope.pageIndex,
                pageSize: $scope.pageSizeSelected,
                sorting: $scope.sorting,
                orderType: $scope.orderType,
                search: $scope.search
            };

            console.log(params);

            $http.post(url, params)
                .then(function (reasons) {
                    $scope.frmMap = false;
                    if (reasons.data.success === true) {
                        console.log(reasons)
                        if (reasons.data.items.length > 0) {
                            $scope.MeterItems = reasons.data.items;
                            $scope.totalCount = reasons.data.totalCount;

                            //$('#tableBody').find('input[type="checkbox"]').each(function () {

                            //    // if exists
                            //    if ($scope.selectedMeters.indexOf(this.id) !== -1) {
                            //        //$(this).prop("checked", true);
                            //        $scope['cb' + this.id] = true;
                            //    }

                            //});

                        } else {
                            alertify.alert("SUCCESS", "Read Meters Not Available", function () { });

                        }


                        $scope.frmMap = true;
                        $scope.loaderGet = false;
                        $scope.SpinnerSearchWip = false;

                    } else if (reasons.data.error) {
                        $scope.loaderGet = false;
                        $scope.SpinnerSearchWip = false;
                        alertify.alert("Error", reasons.data.error, function () { });
                    } else if (reasons.data.expire) {
                        window.location.href = "../Account/Login";
                    } else { window.location.href = "../Account/Login"; }

                    $scope.showLoader = false;
                    $scope.SpinnerSearchWip = false;
                    $scope.showTable = true;
                });
        }
    }

    // Table section
    // Sorting
    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "MODIFIED_DATE";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.GetForm();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    // Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetForm();
    }

    // This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetForm();
    };

    // This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetForm();
    };


    $scope.isExist = function (id) {
        return $scope.MapData.map(function (type) { return type.USID; }).indexOf(id);
    }
    $scope.checkAll = function () {

        if ($scope.selectedAll) {
            //alert()
            $scope.selectedAll = true;
        } else {
            // alert("hu")
            $scope.selectedAll = false;
        }
        angular.forEach($scope.MeterItems, function (MeterItem) {
            MeterItem.selected = $scope.selectedAll;
        });
    };

    $scope.submitSolar = function () {

        
        $scope.loaderSave = true;
        var newDataList = [];
        angular.forEach($scope.MeterItems, function (selected) {
            if (selected.selected) {
                newDataList.push(selected);
            }
        });
        $scope.SelectedMeterDetails = newDataList;
        console.log($scope.SelectedMeterDetails)
        //var checkList = $scope.selectedMeters;
        debugger
        if ($scope.SelectedMeterDetails.length > 0) {
            $http.post('../Master/SubmitBulkSolar',
                {
                    CheckList: $scope.SelectedMeterDetails
                    
                })
                .then(function (response) {
                    debugger;
                    if (response.data.success) {
                        $scope.loaderSave = false;
                        alertify.alert("Success", response.data.success, function () {
                            $scope.GetForm();
                        });
                    } else if (response.data.error) {
                        $scope.loaderSave = false;
                        alertify.alert("Error", response.data.error, function () { });
                    } else {
                        window.location.href = "../Account/Login";
                    }
                });
        } else {
            alertify.alert("SUCCESS", "Please select atleast one meter", function () { });

        }

    }

    

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

    $scope.EditReading = function (meter) {
        console.log(meter)
        //alert(item.mid)
        $scope.ViewImages(meter.MID);
        $scope.selectedMeter = meter;
        $scope.EditView = true;
        $scope.showGallery = false;
        $scope.frmMap = false;
        $scope.imageView = false;
        $scope.searchForm = false;
    }
    $scope.displayTableView = function () {
        $scope.EditView = false;
        $scope.frmMap = true;
        $scope.showGallery = false;
        $scope.searchForm = true;
        $scope.imageView = false;

        $scope.GetForm();
    }
    $scope.backToView = function () {
        $scope.EditView = false;
        $scope.frmMap = true;
        $scope.showGallery = false;
        $scope.searchForm = true;
        $scope.imageView = false;
    }
    $scope.showGalleryForm = function (meter) {
        $scope.ViewAllImages(meter.MID)
        $scope.selectedMeter = meter;
        $scope.EditView = false;
        $scope.frmMap = false;
        $scope.searchForm = false;
        $scope.showGallery = true;

        $scope.imageView = true;
    }
    $scope.saveReading = function () {
        // alert($scope.selectedMeter.CURRENT_READING)

        $scope.isSaving = true;
        $http.post('../Master/updateMeterReading',
            {
                MID: $scope.selectedMeter.MID,
                CURRENT_READING: $scope.selectedMeter.CURRENT_READING,
                READ_CODE_ID: $scope.selectedMeter.READ_CODE_ID,
                REVIEW_COMMENTS: $scope.selectedMeter.REVIEW_COMMENTS
            })
            .then(function (response) {
                console.log("Meter save")
                console.log(response)
                if (response.data.success) {
                    $scope.loaderSave = false;
                    $scope.isSaving = false;
                    $scope.SpinnerSearchWip = false;
                    var msg = "";
                    
                    alertify.alert("Success", response.data.success, function () {
                        //  window.location.href = '../Master/ViewAudited';
                        
                        $scope.displayTableView();
                    });
                } else if (response.data.error) {
                    $scope.SpinnerSearchWip = false;
                    $scope.loaderSave = false;
                    alertify.alert("Error", response.data.error, function () {
                        
                    });
                } else {
                    /// window.location.href = "../Account/Login";
                    $scope.SpinnerSearchWip = false;
                    $scope.loaderSave = false;
                    alertify.alert("Error", "Update Failed.", function () { });
                }
                $scope.isSaving = false;

            });
    }
    //$scope.changeUser = function (UserID) {
    //    $scope.RouteList = [];
    //    $http.get('../Master/GetUserCCC?UserID=' + UserID).then(function (reasons) {
    //        if (reasons.data.success) {
    //            $scope.CCCList = reasons.data.CCC;
    //        } else if (reasons.data.expire) {
    //            window.location.href = "../Account/Login";
    //        }
    //    });
    //}
    

    $scope.fileTitle = '';

    $scope.showImagePopup = function (a) {
        // alert(ID)
        debugger;
        $scope.fileTitle = a.AttachmentFileName;
        $scope.ImageDetails = a;
        // $('#popupImage').attr('src', 'data:image/jpeg;base64, a.Attachment');
        //var data = {
        //    fileName: a.GeneratedAttachmentName,
        //    file = a.Attachment

        //}

        // $('#downloadImage').attr('href', '../Report/DownloadDBAttachment/');
    }
    // Gallery
    $scope.ViewImages = function (item) {

        // $scope.showLoaderOnly();
        $scope.imagesList = [];
        $scope.isimagesloading = true;
        $http.post('../Report/GetAuditedImages', { ID: item }).then(function (response) {
            console.log('respose');
            console.log(response);
            $scope.isimagesloading = false;
            // $scope.selectedMeter = item;
            if (response.data.success) {

                $scope.imagesList = response.data.attachments;

                //   $scope.showGalleryForm();

            } else if (response.data.error) {
                alertify.alert('Message', "images loading failed due to slow network.", function () {

                });

            } else if (response.data.expire) {
                window.location.href = '../Home/Home';
            }
        });
    }
    $scope.ViewAllImages = function (item) {

        // $scope.showLoaderOnly();
        $scope.allImagesList = [];
        $scope.isimagesloading = true;
        $http.post('../Report/GetAllAuditedImages', { ID: item }).then(function (response) {
            console.log('respose');
            console.log(response);
            $scope.isimagesloading = false;
            // $scope.selectedimgMeter = item;
            if (response.data.success) {

                $scope.allImagesList = response.data.attachments;

                //   $scope.showGalleryForm();

            } else if (response.data.error) {
                alertify.alert('Message', response.data.error, function () {

                });

            } else if (response.data.expire) {
                window.location.href = '../Home/Home';
            }
        });
    }
});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});