﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.rSuppliersErrorList = false;
    $scope.SaveType = "Add";

    $scope.EditSupplierID = null;

    $scope.CreateNewSupplierData = {};
    $scope.CreateNewSupplierData.Status = "Active";
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "SupplierName";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getSupplierList = function () {
        $http.get("../Master/getSuppliers?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search)
            .then(
                          function (response) {
                              if (response.data.success) {
                                  $scope.Suppliers = response.data.success.Suppliers;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "SupplierName";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getSupplierList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getSupplierList();
    }
    //Loading employees list on first time
    $scope.getSupplierList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getSupplierList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getSupplierList();
    };

    $scope.showAddSupplierForm = function () {

        $scope.SaveType = 'Add';

        $scope.rTable = false;
        $scope.rBulkForm = false;
        $scope.rForm = true;

        $scope.CreateNewSupplierData.Status = "Active";
        $scope.EditSupplierID = null;

    }

    $scope.showAddBulkSuppliersForm = function () {

        $scope.SaveType = 'Add Bulk';

        $scope.rTable = false;
        $scope.rForm = false;
        $scope.rSuppliersErrorList = false;
        $scope.rBulkForm = true;

        //$scope.CreateNewSupplierData.Status = "Active";
        //$scope.EditSupplierID = null;
    }

    $scope.showSuppliersTable = function () {

        $scope.rBulkForm = false;
        $scope.rForm = false;
        $scope.rSuppliersErrorList = false;
        $scope.rTable = true;


        $scope.CreateNewSupplierData = {};

    }

    $scope.reloadSuppliers = function () { window.location.href = '../Master/AddSupplier'; }

    //Save User
    $scope.SaveNewSupplier = function () {

        if ($scope['CreateNewSupplierForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewSupplier',
                {
                    SupplierFormData: $scope.CreateNewSupplierData,
                    SaveType: $scope.EditSupplierID == null ? "Add" : "Update"
                })
                .then(function (res) {

                    $scope.imgLoader_Save = false;

                    if (res.data.success) {
                        alertify.alert('Success', res.data.success, function () {
                            window.location.href = '../Master/AddSupplier';
                        });
                    }
                    else if (res.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                    else {
                        alertify.alert('Error', res.data.error, function () {
                        });
                    }

                });

        }
        else {
            $scope.showMsgs = true;
        }
    }

    //Save Bulk

    //Attachments
    $scope.UploadAttachmentLoader = false;

    $scope.AttachmentFiles = [];

    $scope.RemoveAttachment = function (index) {
        $scope.AttachmentFiles.splice(index, 1);
    }

    $scope.UploadAttachment = function () {

        var files = $("#Attachment").get(0).files;

        if (files.length < 1) {
            alertify.alert('Error', 'Select files to upload.', function () {
            });
        } else {

            for (var i = 0; i < files.length; i++) {
                $scope.AttachmentFiles.push(files[i]);
            }

            alertify.alert('Success', 'Uploaded successfully.', function () {
            });

            $('#Attachment').val('').clone(true);
        }
    }

    $scope.SaveBulkSuppliers = function () {

        var filecount = $("#Attachment").get(0).files;

        if (filecount.length > 0) {
            alertify.alert('Error', "You have selected Profile Picture but not uploaded. Please upload.", function () {
            });
            return;
        }

        if ($scope.AttachmentFiles < 1) {
            alertify.alert('Error', "Please Upload Templete file to submit.", function () {
            });
            return;
        }

        var files = $scope.AttachmentFiles;

        $http({
            method: 'post',
            url: '../Master/CreateBulkSuppliers',
            headers: { 'Content-Type': undefined },

            transformRequest: function () {
                var formData = new FormData();
                //formData.append("jsonData", angular.toJson(stockdata));

                for (var i = 0; i < files.length; i++) {
                    formData.append(files[i].name, files[i]);
                }
                return formData;
            },
            data: { files: $scope.files }
        }).then(function (result) {
            if (result.data.success) {
                $scope.SuppliersErrorList = result.data.SuppliersErrorList;
                $scope.rTable = false;
                $scope.rForm = false;
                $scope.rBulkForm = false;
                $scope.rSuppliersErrorList = true;
            }

        });

    }

    //Show Edit Form
    $scope.EditSupplier = function (SupplierID) {

        $http.post('../Master/getSupplierDetails', { SupplierID: SupplierID }).then(function (res) {

            if (res.data.success) {

                $scope.EditSupplierID = SupplierID;

                $scope.CreateNewSupplierData = res.data.SupplierDetails[0];

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Supplier form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



