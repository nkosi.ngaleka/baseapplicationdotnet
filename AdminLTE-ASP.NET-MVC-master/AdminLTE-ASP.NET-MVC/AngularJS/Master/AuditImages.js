﻿app.controller('AuditImageCtrl', function ($scope, $http, $filter) {
    //  alert("ok");
   
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.tableView = true;
    $scope.imageView = false;
    $scope.orderType = "ASC";
    $scope.search = "";
    $scope.imageType = "";
    $scope.isSearch = false;

   
    $scope.GetMeterReadingReports = function (accountno) {
        //alert(accountno);
        debugger;
        $scope.showLoader = true;
        $scope.showTable = false;

        if (accountno != undefined && accountno != "") {
            $scope.isSearch = true;
            var url = "../Report/GetMeterReadingReports?accountNo=" + accountno

            $http.get(url)
                .then(function (response) {
                    $scope.isSearch = false;
                    console.log(response);
                    if (response.data.success === true) {
                        if (response.data.items.length > 0) {
                            $scope.items = response.data.items;
                            $scope.totalCount = response.data.totalCount;

                        } else {
                            alertify.alert("SUCCESS", "Meters Not Available", function () { });

                        }

                        $scope.showLoader = false;
                        $scope.showTable = true;
                    } else if (response.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                }, function (err) {
                    var error = err;
                    alertify.alert("Error", "Data loading problem");
                    console.log(error);
                    $scope.isSearch = false;
                });
        }
        else {
            alertify.alert("MESSAGE", "Account Number is required");
        }
    }

    $scope.GetMeterReadingHistory = function (meterno) {

        $scope.showGalleryForm();

        var url = "../Report/GetMeterReadingHistory?meterno=" + meterno + "&pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search + "&from=" + $scope.fromdate + "&to=" + $scope.todate;

        $http.get(url)
            .then(function (response) {
                console.log(response);
                if (response.data.success === true) {
                    if (response.data.items.length > 0) {
                        $scope.itemHistory = response.data.items;
                        $scope.totalCount = response.data.totalCount;
                    } else {
                        alertify.alert("SUCCESS", "Audited Meters Not Available", function () { });

                    }

                    $scope.showLoader = false;
                    $scope.showTable = true;
                } else if (response.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
            }, function (err) {
                var error = err;
            });
    }


    $scope.showLoaderOnly = function () {
        $scope.showTable = false;
        $scope.showDetails = false;
        $scope.showGallery = false;
        $scope.showLoader = true;

        $scope.tableView = false;
        $scope.imageView = false;
    }

    $scope.EditReading = function (meter) {
        console.log(meter)
        //alert(item.mid)
        $scope.ViewImages(meter.ID)
        $scope.selectedMeter = meter;
        $scope.EditView = false;
        $scope.frmMap = false;
        $scope.searchForm = false;
        $scope.showGallery = true;
        $scope.tableView = false;
        $scope.imageView = true;
    }
    $scope.displayTableView = function () {
        $scope.EditView = false;
        $scope.frmMap = true;
        $scope.showGallery = false;
        $scope.searchForm = true;
        $scope.imageView = false;
        $scope.tableView = true;

    }
    $scope.showGalleryForm = function (meter) {
        $scope.allImagesList = [];
        $scope.ViewAllImages(meter.ID)
        $scope.selectedMeter = meter;
        $scope.EditView = false;
        $scope.frmMap = false;
        $scope.searchForm = false;
        $scope.showGallery = true;
        $scope.tableView = false;
        $scope.imageView = true;
    }

    $scope.fileTitle = '';

    $scope.showImagePopup = function (a) {
        // alert(ID)
       // debugger;
        $scope.fileTitle = a.AttachmentFileName;
        $scope.ImageDetails = a;
      
    }
    // Gallery
    $scope.ViewImages = function (item) {

        // $scope.showLoaderOnly();
        $scope.allImagesList = [];
        $scope.isimagesloading = true;
        $http.post('../Report/GetPreviousAuditedImages', { ID: item }).then(function (response) {
            console.log('respose');
            console.log(response);
            $scope.isimagesloading = false;
            // $scope.selectedMeter = item;
            if (response.data.success) {

                $scope.allImagesList = response.data.attachments;

                //   $scope.showGalleryForm();

            } else if (response.data.error) {
                alertify.alert('Message', "images loading failed due to slow network.", function () {

                });

            } else if (response.data.expire) {
                window.location.href = '../Home/Home';
            }
        });
    }
    $scope.ViewAllImages = function (item) {

        // $scope.showLoaderOnly();
        $scope.allImagesList = [];
        $scope.isimagesloading = true;
        $http.post('../Report/GetAllAuditedImages', { ID: item }).then(function (response) {
            console.log('respose');
            console.log(response);
            $scope.isimagesloading = false;
            // $scope.selectedimgMeter = item;
            if (response.data.success) {

                $scope.allImagesList = response.data.attachments;

                //   $scope.showGalleryForm();

            } else if (response.data.error) {
                alertify.alert('Message', response.data.error, function () {

                });

            } else if (response.data.expire) {
                window.location.href = '../Home/Home';
            }
        });
    }

   

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
            ' <a ng-click="sort_by(order)" style="color: #555555;">' +
            '    <span ng-transclude></span>' +
            '    <i ng-class="selectedCls(order)"></i>' +
            '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});