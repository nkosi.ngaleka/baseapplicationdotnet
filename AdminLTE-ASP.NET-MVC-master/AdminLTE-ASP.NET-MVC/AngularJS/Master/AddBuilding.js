﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.SaveType = "Add";

    $scope.EditBuildingID = null;

    $scope.CreateNewBuildingData = {};
    $scope.CreateNewBuildingData.Status = "Active";
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "Name";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getBuildingList = function () {
        $http.get("../Master/getBuildings?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&search=" + $scope.search)
            .then(
                          function (response) {
                              if (response.data.success) {
                                  $scope.Buildings = response.data.success.Buildings;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "Id";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getBuildingList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getBuildingList();
    }
    //Loading employees list on first time
    $scope.getBuildingList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getBuildingList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getBuildingList();
    };

    $scope.showAddBuildingForm = function () {

        $scope.SaveType = 'Add';

        $scope.rTable = false;
        $scope.rForm = true;

        $scope.CreateNewBuildingData.Status = "Active";
        $scope.EditBuildingID = null;

    }

    $scope.showBuildingsTable = function () {

        $scope.rTable = true;
        $scope.rForm = false;

        $scope.CreateNewBuildingData = {};

    }

    //Save User
    $scope.SaveNewBuilding = function () {

        if ($scope['CreateNewBuildingForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;
            $scope.CreateNewBuildingData.BuildingLocation = $('#BuildingLocation').val();
            $http.post('../Master/CreateNewBuilding',
                {
                    BuildingFormData: $scope.CreateNewBuildingData,
                    SaveType: $scope.EditBuildingID == null ? "Add" : "Update"
                })
                .then(function (res) {

                $scope.imgLoader_Save = false;

                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Master/AddNewBuilding';
                    });
                }
                else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }
    }

    //Show Edit Form
    $scope.EditBuilding = function (BuildingID) {

        $http.post('../Master/getBuildingDetails', { BuildingID: BuildingID }).then(function (res) {

            if (res.data.success) {

                $scope.EditBuildingID = BuildingID;

                $scope.CreateNewBuildingData = res.data.BuildingDetails[0];

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Building form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



