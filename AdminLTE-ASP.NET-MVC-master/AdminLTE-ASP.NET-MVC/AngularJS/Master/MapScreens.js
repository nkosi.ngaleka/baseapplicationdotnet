﻿app.controller('MasterController', function ($scope, $http, $filter,$compile) {
    $scope.MapData = [];
    //Gets 
    $http.post('../Master/getMapScreenData').then(function (reasons) {
        if (reasons.data.success) {

            $scope.MapData = reasons.data.MapData;

            $scope.Roles = reasons.data.Roles;
            $scope.Screens = reasons.data.Screens;
  
        } else if (reasons.data.expire) {
            window.location.href = '../Home/Dashboard';
        }
    });

    $scope.isExist = function (id) {
        return $scope.MapData.map(function (type) { return type.RSID; }).indexOf(id);
    }

    $scope.SaveRoleScreenMap = function () {
    var checkList = [];

        $('#tableBody').find('input[type="checkbox"]:checked').each(function () {
            checkList.push(this.id);
        });

        $http.post('../Master/SaveRoleScreenMap', { CheckList: checkList }).then(function (response) {
            if (response.data.success) {
                alertify.alert("Success", response.data.success, function () {
                    window.location.href = '../Master/MapScreens';
                });
            } else if (response.data.expire) {
                window.location.href = '../Home/Dashboard';
            } else {
                alertify.alert("Error", response.data.error, function () { });
            }
        });
    }

});