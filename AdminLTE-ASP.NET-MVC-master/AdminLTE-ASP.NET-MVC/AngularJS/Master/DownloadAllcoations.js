﻿app.controller('ERFController', function ($scope, $http, $filter) {

    $scope.searchResult = false;

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "ALLOCATED_DATE";
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.search = "";
    $scope.isFormValid = false;
    $scope.$watch('SearchCriteriaForm.$valid', function (newValue) {
        $scope.isFormValid = newValue;
    });
  
    $http.post('../Master/GetUsers').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });

    $scope.GetAllocatedList = function () {
        if ($scope.search == undefined) {
            $scope.search = "";
        }
        $scope.errShow = false;
        $scope.SearchCriteriaFormSubmitted = true;

        if ($scope.isFormValid) {
            var url = "../Master/GetAllocatedERF?pageIndex=" + $scope.pageIndex
                + "&pageSize=" + $scope.pageSizeSelected
                + "&sorting=" + $scope.sorting
                + "&orderType=" + $scope.orderType
                + "&search=" + $scope.search
                + "&userID=" + $scope.UserID;

            $http.get(url)
                .then(function (response) {
                    console.log("response")
                    console.log(response)
                    if (response.data.success) {
                        console.log(response.data.success);
                        if (response.data.items.length > 0) {
                            $scope.items = response.data.items;
                            $scope.totalCount = response.data.totalCount;

                        } else {
                            alertify.alert("SUCCESS", "Allocated Meters Not Available", function () { });

                        }
                        $scope.showLoader = false;
                        $scope.showTable = true;
                        $scope.searchResult = true;
                    } else if (response.data.expire) {
                        window.location.href = '../Home/Dashboard';
                    }
                }, function (err) {
                    var error = err;
                });
        }
    }

    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "ALLOCATED_DATE";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;
        
        $scope.GetAllocatedList();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetAllocatedList();
    }

    //Loading employees list on first time
    //$scope.GetAllocatedList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetAllocatedList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetAllocatedList();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});