﻿app.controller('MasterController', function ($scope, $http, $filter, $compile) {

    //Table config
    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 100; // Maximum number of items per page.
    $scope.sorting = "POD_CODE"; // Default sorting column
    $scope.showLoader = true;
    $scope.showTable = false;
    $scope.selectedMeters = [];
    $scope.readDayList = [];
    // Search fields
    $scope.AreaCode = '';
    $scope.Suburb = '';
    $scope.search = '';
    $scope.Category = "";
    $scope.MapData = [];

    $scope.showError = false;
    $scope.frmSearch = true;
    $scope.frmMap = false;
    $scope.loaderGet = false;
    $scope.loaderSave = false;
    $scope.CategoryList = ['Water', 'Electricity', 'All'];
    $scope.isFormValid = false;

    $scope.$watch('SearchCriteriaForm.$valid', function (newValue) {
        $scope.isFormValid = newValue;
    });
  
    $http.post('../Master/GetFormConfig').then(function (reasons) {
        if (reasons.data.success) {
            $scope.UserList = reasons.data.users;
            
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $http.post('../Master/GetCCC').then(function (reasons) {
        console.log(reasons)
        if (reasons.data.success) {
            $scope.CCCList = reasons.data.CCC;
        } else if (reasons.data.expire) {
            window.location.href = "../Account/Login";
        }
    });
    $scope.GetCycle = function (ccc) {
        $scope.CycleList = [];
        $scope.routeList = [];
        $http.post('../Master/GetCycle?CCC=' + ccc).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.CycleList = reasons.data.Cycles;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }

    $scope.getRouteCode = function (CCC,Cycle) {
      //  alert(CCC)
        //var url = '../Master/GetUnAllocatedList'
        $scope.routeList = [];
        var params = {
            Cycle: $scope.Cycle,
            CCC: $scope.CCC
        };
        $http.post('../Master/GetRouteCodeCycle', params).then(function (reasons) {
            console.log(reasons)
            if (reasons.data.success) {
                $scope.routeList = reasons.data.Routes;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }
    
    

    $("#chkParent").click(function () {
        $(".chkChild").prop("checked", this.checked);
        $scope.chkChildClicked();
    });

    $scope.chkChildClicked = function () {

        $('#tableBody').find('input[type="checkbox"]:checked').each(function () {
            if ($('.chkChild:checked').length == $('.chkChild').length) {
                $('#chkParent').prop('checked', true);
            } else {
                $('#chkParent').prop('checked', false);
            }
        });

        $('#tableBody').find('input[type="checkbox"]').each(function () {

            if ($(this).prop("checked") === true) {
                if ($scope.selectedMeters.indexOf(this.id) === -1) {
                    $scope.selectedMeters.push(this.id);
                }
            } else {
                if ($scope.selectedMeters.indexOf(this.id) !== -1) {
                    $scope.selectedMeters.splice($scope.selectedMeters.indexOf(this.id), 1);
                }
            }
            
        });

        console.log($scope.selectedMeters);
    }

    $scope.changeReadDay = function (Suburb) {
        //alert(Suburb)
        $scope.readDayList = [];
        $http.get('../Master/GetReadDay?Route=' + Suburb).then(function (reasons) {
            if (reasons.data.success) {
                $scope.readDayList = reasons.data.readDay;
            } else if (reasons.data.expire) {
                window.location.href = "../Account/Login";
            }
        });
    }

    //$('.atleastone :input').change(function () {
    //    $('.atleastone :input').each(function () {
    //        if ($(this).val() == "") {
    //            $scope.errShow = true;
    //        } else {
    //            $scope.errShow = false;
    //            return false;
    //        }
    //    });
    //});

    $scope.GetFormUser = function () {
        $scope.selectedMeters = [];
        $scope.GetForm();
    }

    $scope.GetForm = function () {
       // alert($scope.Category)
        $('#chkParent').prop('checked', false);

       

        $scope.errShow = false;
        $scope.SearchCriteriaFormSubmitted = true;

        //$('.atleastone :input').each(function () {
        //    if ($(this).val() == "") {
        //        $scope.errShow = true;
        //    } else {    
        //        $scope.errShow = false;
        //        return false;
        //    }
        //});

        if ($scope.isFormValid && !$scope.errShow) {
            $scope.showLoader = true;
            $scope.showTable = false;
            $scope.loaderGet = true;
            $scope.showError = false;

            var url = '../Master/GetUnAllocatedList'
            var params = {
                CCC: $scope.CCC,
                Cycle: $scope.Cycle,
                Route: $scope.Route,
                AreaCode: $scope.AreaCode,
                Category: $scope.Category,
                pageIndex: $scope.pageIndex,
                pageSize: $scope.pageSizeSelected,
                sorting: $scope.sorting,
                orderType: $scope.orderType,
                search: $scope.search
            };

            console.log(params);
            
            $http.post(url, params)
                .then(function (reasons) {
                    console.log(reasons)
                    $scope.frmMap = false;
                    if (reasons.data.success === true) {
                        console.log(reasons)
                        if (reasons.data.items.length > 0) {
                            $scope.Screens = reasons.data.items;
                            $scope.totalCount = reasons.data.totalCount;

                            $('#tableBody').find('input[type="checkbox"]').each(function () {

                                // if exists
                                if ($scope.selectedMeters.indexOf(this.id) !== -1) {
                                    //$(this).prop("checked", true);
                                    $scope['cb' + this.id] = true;
                                }

                            });
                            $scope.frmMap = true;

                        } else {
                            alertify.alert("SUCCESS", "Meters Not Available", function () { });

                        }
                        $scope.loaderGet = false;

                    } else if (reasons.data.error) {
                        $scope.loaderGet = false;
                        alertify.alert("Error", reasons.data.error, function () { });
                    } else if (reasons.data.expire) {
                        window.location.href = "../Account/Login";
                    } else { window.location.href = "../Account/Login"; }

                    $scope.showLoader = false;
                    $scope.showTable = true;
                });
        }
    }

    $scope.isExist = function (id) {
        return $scope.MapData.map(function (type) { return type.USID; }).indexOf(id);
    }

    $scope.SaveUserScreenMap = function () {

        if ($scope.UserID == undefined) { return; }

        $scope.loaderSave = true;

        var checkList = $scope.selectedMeters;

        //$('#tableBody').find('input[type="checkbox"]:checked').each(function () {
        //    checkList.push(this.id);
        //});
        
        $http.post('../Master/SaveAllocations',
            {
                CheckList: checkList,
                UserID: $scope.UserID,
                process: "Normal"
            })
            .then(function (response) {
                if (response.data.success) {
                    $scope.loaderSave = false;
                    alertify.alert("Success", response.data.success, function () {
                        $scope.GetFormUser();
                    });
                } else if (response.data.error) {
                    $scope.loaderSave = false;
                    alertify.alert("Error", response.data.error, function () { });
                } else {
                    window.location.href = "../Account/Login";
                }
            });
    }

    // Table section
    // Sorting
    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.orderType = "ASC";
    $scope.column = "MeterNumber";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }

        $scope.sorting = val;

        $scope.GetForm();
    }

    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    // Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.GetForm();
    }
    
    // This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.GetForm();
    };

    // This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.GetForm();
    };

    $scope.showUsersTable = function () {
        $scope.uTable = true;
    }
    
});