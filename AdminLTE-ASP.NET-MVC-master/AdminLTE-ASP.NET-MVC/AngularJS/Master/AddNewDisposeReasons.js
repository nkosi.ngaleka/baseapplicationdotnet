﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.dTable = true;
    $scope.dForm = false;

    $scope.EditReasonID = null;

    $scope.CreateNewReasonData = {};
    $scope.CreateNewReasonData.Status = "Active";
    $scope.SaveType = "";

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "Reason";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getAssetDisposeReasons = function () {
        $http.get("../Master/getAssetDisposeReasons?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&orderType=" + $scope.orderType + "&search=" + $scope.search).then(
                          function (response) {
                              if (response.data.success) {
                                  $scope.Reasons = response.data.success.Reasons;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "Reason";
    $scope.orderType = "ASC";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
            $scope.orderType = "DESC";
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
            $scope.orderType = "ASC";
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getAssetDisposeReasons();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getAssetDisposeReasons();
    }
    //Loading employees list on first time
    $scope.getAssetDisposeReasons();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getAssetDisposeReasons();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getAssetDisposeReasons();
    };

    $scope.showAddReasonForm = function () {

        $scope.SaveType = 'Add';

        $scope.dTable = false;
        $scope.dForm = true;

        $scope.CreateNewReasonData.Status = "Active";
        $scope.EditReasonID = null;
    }

    $scope.showReasonsTable = function () {

        $scope.dTable = true;
        $scope.dForm = false;

        $scope.CreateNewReasonData = {};

    }

    //Save User
    $scope.SaveNewReason = function () {

        if ($scope['CreateNewReasonForm'].$valid) {

            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewAssetDisposeReason',
            {
                ReasonFormData: $scope.CreateNewReasonData,
                SaveType: $scope.EditReasonID == null ? "Add" : "Update"
            }).then(function (res) {

                $scope.imgLoader_Save = false;

                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Master/AddNewAssetDisposeReason';
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }
    }


    //Show Edit Form
    $scope.EditReason = function (ReasonID) {
        //alert(ReasonID);
        $http.post('../Master/getReasonDetails', { ReasonID: ReasonID }).then(function (res) {

            if (res.data.success) {

                $scope.EditReasonID = ReasonID;

                $scope.CreateNewReasonData = res.data.ReasonDetails[0];
                console.log('EDIT DATA');
                console.log(res.data.ReasonDetails[0]);

                //display form
                $scope.SaveType = 'Update';
                $scope.dTable = false;
                $scope.dForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Reason form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



