﻿app.controller('MasterController', function ($scope, $http, $filter) {

    $scope.capturePanel = true;

    $scope.rTable = true;
    $scope.rForm = false;
    $scope.SaveType = "Add";

    $scope.TempBuildingID = null;
    $scope.EditRoomID = null;

    $scope.CreateNewRoomData = {};
    $scope.CreateNewRoomData.Status = "Active";

    $scope.maxSize = 5;     // Limit number for pagination display number.
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->
    $scope.pageSizeSelected = 10; // Maximum number of items per page.
    $scope.sorting = "BuildingName";
    $scope.showLoader = true;
    $scope.showTable = false;

    $scope.getRoomList = function () {
        $http.get("../Master/getRooms?pageIndex=" + $scope.pageIndex + "&pageSize=" + $scope.pageSizeSelected + "&sorting=" + $scope.sorting + "&search=" + $scope.search).then(
                          function (response) {

                              console.log(response.data);

                              if (response.data.success) {
                                  $scope.Rooms = response.data.success.Rooms;
                                  $scope.Buildings = response.data.Buildings;
                                  $scope.totalCount = response.data.success.totalCount;
                                  $scope.showLoader = false;
                                  $scope.showTable = true;
                              } else if (response.data.expire) {
                                  window.location.href = '../Home/Dashboard';
                              }
                          },
                          function (err) {
                              var error = err;
                          });
    }
    //Sorting 

    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;
    $scope.column = "BuildingName";
    $scope.sort = function (val) {
        $scope.column = val;
        if ($scope.reverse) {
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        } else {
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
        //alert(val)
        $scope.sorting = val;
        //alert($scope.sorting)
        $scope.getRoomList();
    }
    // remove and change class
    $scope.sortClass = function (col) {
        if ($scope.column == col) {
            if ($scope.reverse) {
                return 'arrow-down';
            } else {
                return 'arrow-up';
            }
        } else {
            return '';
        }
    }

    //Search Method
    $scope.SearchMethod = function (val) {
        $scope.search = val;
        $scope.getRoomList();
    }
    //Loading employees list on first time
    $scope.getRoomList();

    //This method is calling from pagination number
    $scope.pageChanged = function () {
        $scope.getRoomList();
    };

    //This method is calling from dropDown
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getRoomList();
    };

    $scope.showAddRoomForm = function () {

        $scope.SaveType = 'Add';

        $scope.CreateNewRoomData.Status = "Active";
        $scope.TempBuildingID = null;
        $scope.EditRoomID = null;

        $scope.rTable = false;
        $scope.rForm = true;

    }

    $scope.showRoomsTable = function () {

        $scope.rTable = true;
        $scope.rForm = false;

        $scope.CreateNewRoomData = {};

    }

    //Save
    $scope.SaveNewRoom = function () {

        if ($scope['CreateNewRoomForm'].$valid) {
            if ($scope.SaveType == "Update") {
                $scope.CreateNewRoomData.BuildingID = $scope.TempBuildingID;
            }
            $scope.showMsgs = false;
            $scope.imgLoader_Save = true;

            $http.post('../Master/CreateNewRoom',
                {
                    RoomFormData: $scope.CreateNewRoomData,
                    SaveType: $scope.EditRoomID == null ? "Add" : "Update"
                }).then(function (res) {

                $scope.imgLoader_Save = false;

                if (res.data.success) {
                    alertify.alert('Success', res.data.success, function () {
                        window.location.href = '../Master/AddRoom';
                    });
                } else if (res.data.expire) {
                    window.location.href = '../Home/Dashboard';
                }
                else {
                    alertify.alert('Error', res.data.error, function () {
                    });
                }

            });

        }
        else {
            $scope.showMsgs = true;
        }
    }

    //Show Edit Form
    $scope.EditRoom = function (RoomID) {

        $http.post('../Master/getRoomDetails', { RoomID: RoomID }).then(function (res) {
            console.log(res.data);
            if (res.data.success) {

                $scope.EditRoomID = RoomID;

                $scope.CreateNewRoomData = res.data.RoomDetails[0];
                console.log('EDIT DATA');
                console.log(res.data.RoomDetails[0]);
                $scope.TempBuildingID = $scope.CreateNewRoomData["BuildingID"];

                //display form
                $scope.SaveType = 'Update';
                $scope.rTable = false;
                $scope.rForm = true;

            } else if (res.data.error) {
                alertify.alert('Error', "Problem loading Update Room form.", function () {
                });
            } else if (res.data.expire) {
                window.location.href = '../Home/Dashboard';
            }

        });
    }

});

app.$inject = ['$scope', '$filter'];

app.directive("customSort", function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            order: '=',
            sort: '='
        },
        template:
          ' <a ng-click="sort_by(order)" style="color: #555555;">' +
          '    <span ng-transclude></span>' +
          '    <i ng-class="selectedCls(order)"></i>' +
          '</a>',
        link: function (scope) {

            // change sorting order
            scope.sort_by = function (newSortingOrder) {
                var sort = scope.sort;

                if (sort.sortingOrder == newSortingOrder) {
                    sort.reverse = !sort.reverse;
                }

                sort.sortingOrder = newSortingOrder;
            };


            scope.selectedCls = function (column) {
                if (column == scope.sort.sortingOrder) {
                    return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                }
                else {
                    return 'icon-sort'
                }
            };
        }// end link
    }
});



