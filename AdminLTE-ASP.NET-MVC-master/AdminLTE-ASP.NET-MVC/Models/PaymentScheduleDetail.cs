//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentScheduleDetail
    {
        public int Id { get; set; }
        public string ApplicationID { get; set; }
        public Nullable<System.DateTime> PaymentDate1 { get; set; }
        public Nullable<System.DateTime> PaymentDate2 { get; set; }
        public Nullable<System.DateTime> PaymentDate3 { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}
